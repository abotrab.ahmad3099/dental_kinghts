import 'package:flutter/material.dart';
import 'src/my_app.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}
