import 'package:dental_kinghts/src/authentication/check_connection_internet.dart';
import 'package:dental_kinghts/src/constants/color_theme_app.dart';
import 'package:dental_kinghts/src/presentation/pages/splach_screen.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  bool _isConnected = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed && !_isConnected) {
      _isConnected = await CheckConnectionInternet.isInternetConnected();
      if (!_isConnected) {
        // Show an error message to the user
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('No internet connection'),
              content: Text('Please connect to the internet to use the app.'),
              actions: [
                ElevatedButton(
                  child: Text('OK'),
                  onPressed: () => Navigator.pop(context),
                ),
              ],
            );
          },
        );
      }
    }
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Dental Kinghts',
        theme: ThemeData(
          fontFamily: "COCON MODIFIED",
          primarySwatch: MyColor.kToDark,
          focusColor: Colors.black,
        ),
        home: const SplachScreen(),
      );
    });
  }
}
