import 'package:bloc/bloc.dart';
import 'package:dental_kinghts/src/constants/storage.dart';

class AuthenticationBloc {
  static Future<void> getuserdata() async {
    Storage().token = await _getToken();
    Storage().name = await _getName();
    Storage().mobile = await _getMobile();
    Storage().busLine = await _getBusLine();
    Storage().firebase_token = await _getFireBaseToken();
    //Storage().image = await _getiamge();
    Storage().userId = await _getUserId();
    Storage().isMovingStarted = await _getIsMoving();
  }

  static Future<void> login(token, name, mobile, image, email, userId) async {
    Storage().token = token;
    Storage().name = name;
    Storage().mobile = mobile;
    Storage().email = email;
    Storage().userId = userId;
    await _saveToken(token);
    await _saveName(name);
    await _saveMobile(mobile);
    await _saveEmail(email);
    await saveUserId(userId);
    await _saveiamge(image ?? '');
  }

  static Future<void> logout() async {
    Storage().token = '';
    Storage().name = '';
    Storage().mobile = '';
    Storage().email = '';
    await _deleteName();
    await _deleteEmail();
    await _deleteToken();
    await _deleteMobile();
    //await _saveiamge(iamge);
  }

  static Future<void> save_firebase_token(id) async {
    Storage().firebase_token = id;
    await Storage().secureStorage.write(key: 'firebase_token', value: id);
  }

  static Future<void> deleteAll() async {
    try {
      Storage().token = '';
      await Storage().secureStorage.delete(key: 'access_token');
      await Storage().secureStorage.delete(key: 'name');
      await Storage().secureStorage.delete(key: 'mobile');
      await Storage().secureStorage.delete(key: 'iamge');
      await Storage().secureStorage.delete(key: 'email');
    } catch (e) {
      print(e);
    }
  }

  /// delete from keystore/keychain
  static Future<void> _deleteToken() async {
    await Storage().secureStorage.delete(key: 'access_token');
  }

  static Future<void> _deleteName() async {
    await Storage().secureStorage.delete(key: 'name');
  }

  static Future<void> _deleteMobile() async {
    await Storage().secureStorage.delete(key: 'mobile');
  }

  static Future<void> _deleteEmail() async {
    await Storage().secureStorage.delete(key: 'email');
  }

  /// write to keystore/keychain
  static Future<void> _saveToken(String token) async {
    await Storage().secureStorage.write(key: 'access_token', value: token);
  }

  static Future<void> _saveName(String name) async {
    await Storage().secureStorage.write(key: 'name', value: name);
  }

  static Future<void> _saveMobile(String mobile) async {
    await Storage().secureStorage.write(key: 'mobile', value: mobile);
  }

  static Future<void> _saveiamge(String balance) async {
    await Storage().secureStorage.write(key: 'iamge', value: balance);
  }

  static Future<void> _saveEmail(String email) async {
    await Storage().secureStorage.write(key: 'email', value: email);
  }

  static Future<void> saveUserId(userId) async {
    await Storage().secureStorage.write(key: 'userId', value: userId);
  }

  static Future<void> saveiamge(String image) async {
    Storage().image = image;
    await Storage().secureStorage.write(key: 'iamge', value: image);
  }

  static Future<void> saveisMovingStarted(String isMovingStarted) async {
    Storage().isMovingStarted = isMovingStarted;
    await Storage()
        .secureStorage
        .write(key: 'isMovingStarted', value: isMovingStarted);
  }

  static Future<void> saveBusLine(String busLine) async {
    Storage().busLine = busLine;
    await Storage().secureStorage.write(key: 'busLine', value: busLine);
  }

  /// read to keystore/keychain
  static Future<String> _getIsMoving() async {
    return await Storage().secureStorage.read(key: 'isMovingStarted') ??
        'false';
  }

  static Future<String> _getBusLine() async {
    return await Storage().secureStorage.read(key: 'busLine') ?? '1';
  }

  static Future<String> _getToken() async {
    return await Storage().secureStorage.read(key: 'access_token') ?? '';
  }

  static Future<String> _getUserId() async {
    return await Storage().secureStorage.read(key: 'userId') ?? '';
  }

  static Future<String> _getFireBaseToken() async {
    return await Storage().secureStorage.read(key: 'firebase_token') ?? '';
  }

  static Future<String> _getName() async {
    return await Storage().secureStorage.read(key: 'name') ?? '';
  }

  static Future<String> _getEmail() async {
    return await Storage().secureStorage.read(key: 'email') ?? '';
  }

  static Future<String> _getiamge() async {
    return await Storage().secureStorage.read(key: 'iamge') ?? '';
  }

  static Future<String> _getMobile() async {
    return await Storage().secureStorage.read(key: 'mobile') ?? '';
  }
}
