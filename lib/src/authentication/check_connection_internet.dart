import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class CheckConnectionInternet {
  static Future<bool> isInternetConnected() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      try {
        final result = await InternetAddress.lookup('google.com');
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          // Fluttertoast.showToast(
          //     msg: "internet connection", backgroundColor: Colors.green);
          return true;
        }
      } on SocketException catch (_) {
        // Fluttertoast.showToast(
        //     msg: "No internet connection", backgroundColor: Colors.red[300]);
        return false;
      }
    }
    // Fluttertoast.showToast(
    //     msg: "No internet connection", backgroundColor: Colors.red[300]);
    return false;
  }
}
