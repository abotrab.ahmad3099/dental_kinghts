import 'package:flutter/material.dart';

class AppColor {
  final Color primaryColor = const Color.fromRGBO(97, 18, 20, 1);
  final Color primaryColorBold = const Color.fromRGBO(78, 24, 25, 1);

  final Color secondaryColor = const Color.fromRGBO(230, 145, 45, 1);
  final Color secondaryColorBlod = const Color.fromRGBO(165, 110, 43, 1);
}
