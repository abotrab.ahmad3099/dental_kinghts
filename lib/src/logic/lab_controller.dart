import 'dart:io';

import 'package:get/get.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class LabController extends GetxController {
  List<File> jobsInFactoryImage = [];
  List<File> imageForFactory = [];
  List<Asset> imagesAsset = [];
  List<Asset> jobsInImageAsset = [];
  bool addedNewPhotoJob = true;
  bool addedNewPhotoForLab = true;
  @override
  void onInit() {
    jobsInFactoryImage = [];
    imageForFactory = [];
    super.onInit();
  }

  addNewImageToJobFactory(file) {
    jobsInFactoryImage = file;
    addedNewPhotoJob = true;

    update();
  }

  addNewImageforFactory(file) {
    if (imageForFactory.length <= 4) {
      imageForFactory.add(file);
      addedNewPhotoJob = true;
    } else {
      addedNewPhotoJob = false;
    }
    update();
  }

  addImagesforFactory(List<File> file) {
    imageForFactory.addAll(file);
    addedNewPhotoJob = true;

    update();
  }

  deleteImage(index) {
    jobsInFactoryImage.removeAt(index);
    update();
  }

  deleteImageFromFactory(index) {
    imageForFactory.removeAt(index);
    update();
  }
}
