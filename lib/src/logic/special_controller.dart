import 'package:dental_kinghts/src/data/models/spec.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../constants/color_app.dart';

class SpecController extends GetxController {
  var selectedSpec = <String>{};
  Color backgroundColor = Color.fromARGB(255, 236, 188, 188);
  late ScrollController scrollController = ScrollController();
  @override
  void onInit() {
    selectedSpec = {'عامة'};
    scrollController = ScrollController();
    super.onInit();
  }

  addNewSelectedSpec(value) {
    selectedSpec.clear();
    selectedSpec.add(value);
    scrollController = ScrollController();
    update();
  }

  getWidget() {
    List<Widget> widgets = [];
    for (var element in selectedSpec) {
      widgets.add(Row(
        children: [
          Container(
            // width: 20.w,
            height: 4.5.h,
            padding: EdgeInsets.symmetric(horizontal: 2.w),
            decoration: BoxDecoration(
                color: backgroundColor, borderRadius: BorderRadius.circular(6)),
            child: Center(
                child: Text(
              element,
              style: TextStyle(color: ColorApp.primaryColor),
            )),
          ),
          SizedBox(width: 1.w),
        ],
      ));
    }

    List x = widgets.reversed.toList();
    return x;
    // update();
  }
}
