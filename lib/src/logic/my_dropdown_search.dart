import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

class MyDropdownSearch extends StatefulWidget {
  final List<String> items;
  final Function(String) onChanged;
  MyDropdownSearch({required this.items, required this.onChanged});

  @override
  _MyDropdownSearchState createState() => _MyDropdownSearchState();
}

class _MyDropdownSearchState extends State<MyDropdownSearch> {
  String? _selectedItem;
  @override
  initState() {
    // _selectedItem = widget.items[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        TypeAheadField(
          textFieldConfiguration: const TextFieldConfiguration(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'اختيار ملف المريض ..',
            ),
          ),
          suggestionsCallback: (pattern) async {
            return widget.items
                .where((item) =>
                    item.toLowerCase().contains(pattern.toLowerCase()))
                .toList();
          },
          itemBuilder: (context, suggestion) {
            return ListTile(
              title: Text(suggestion.toString()),
            );
          },
          onSuggestionSelected: (suggestion) {
            setState(() {
              _selectedItem = suggestion.toString();
            });
            widget.onChanged(suggestion.toString());
          },
        ),
        Text(_selectedItem ?? ''),
      ],
    );
  }
}
