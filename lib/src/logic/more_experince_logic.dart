import 'package:flutter/cupertino.dart';

import '/src/constants/static_data.dart';
import 'package:get/get.dart';

class MoreExperinceLogic extends GetxController {
  List<int> empQualifiqation = [];
  List<int> lang = [];
  List<int> skills = [];
  List<int> hobbies = [];
  List<int> courses = [];
  List<int> educations = [];
  TextEditingController introduction = TextEditingController();

  int counterEmp = 1;
  int counterSkills = 1;
  int counterLang = 1;
  int counterHobbies = 1;
  int counterCourses = 1;
  int counterEducation = 1;

  @override
  void onInit() {
    introduction = TextEditingController();
    for (var i = 0;
        i < StaticData.experiences!.empolyementQualifications!.length;
        i++) {
      empQualifiqation.add(i + 1);
    }
    for (var i = 0; i < StaticData.experiences!.languages!.length; i++) {
      lang.add(i + 1);
    }
    for (var i = 0; i < StaticData.experiences!.skills!.length; i++) {
      skills.add(i + 1);
    }
    for (var i = 0; i < StaticData.experiences!.hobbits!.length; i++) {
      hobbies.add(i + 1);
    }
    for (var i = 0; i < StaticData.experiences!.courses!.length; i++) {
      courses.add(i + 1);
    }
    for (var i = 0; i < StaticData.experiences!.educations!.length; i++) {
      educations.add(i + 1);
    }
    super.onInit();
  }

  updateEmployeeQualifi() {
    empQualifiqation.add(empQualifiqation.length + 1);
    update();
  }

  updateLang() {
    lang.add(lang.length + 1);
    update();
  }

  updateSkills() {
    skills.add(skills.length + 1);
    update();
  }

  updateHobbies() {
    hobbies.add(hobbies.length + 1);
    update();
  }

  updateCourse() {
    courses.add(courses.length + 1);
    update();
  }

  updateEducation() {
    educations.add(educations.length + 1);
    update();
  }

  updateValueEmpQualification(value) {
    counterEmp = value;
    update();
  }

  updateValueCourse(value) {
    counterCourses = value;
    update();
  }

  updateValueSkills(value) {
    counterSkills = value;
    update();
  }

  updateValueLang(value) {
    counterLang = value;
    update();
  }

  updateValueHobbies(value) {
    counterHobbies = value;
    update();
  }

  updateValueEducation(value) {
    counterEducation = value;
    update();
  }
}
