import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ControllerEditing extends GetxController {
  List<TextEditingController> employeeQualif = [];
  List<TextEditingController> education = [];
  List<TextEditingController> lang = [];
  List<TextEditingController> skill = [];
  List<TextEditingController> hobbies = [];
  List<TextEditingController> course = [];
  DateTime dateTimeCourse = DateTime.now();
  DateTime dateTimeStartLearn = DateTime.now();
  DateTime dateTimeEndLearn = DateTime.now();
  DateTime dateTimeStartEmpQualification = DateTime.now();
  DateTime dateTimeEndEmpQualification = DateTime.now();

  @override
  void onInit() {
    for (var i = 0; i < 4; i++) {
      employeeQualif.add(TextEditingController());
    }
    for (var i = 0; i < 3; i++) {
      education.add(TextEditingController());
    }
    for (var i = 0; i < 2; i++) {
      lang.add(TextEditingController());
      skill.add(TextEditingController());
    }
    for (var i = 0; i < 1; i++) {
      hobbies.add(TextEditingController());
      course.add(TextEditingController());
    }
    dateTimeCourse = DateTime.now();
    dateTimeStartLearn = DateTime.now();
    dateTimeEndLearn = DateTime.now();
    dateTimeStartEmpQualification = DateTime.now();
    dateTimeEndEmpQualification = DateTime.now();
    super.onInit();
  }

  updateCourseData(name, date) {
    course[0].text = name;
    dateTimeCourse = DateTime.parse(date);
    update();
  }

  updateHobbit(name) {
    hobbies[0].text = name;
    update();
  }

  updateSkillsData(nameSkills, level) {
    skill[1].text = nameSkills;
    skill[0].text = level;
    update();
  }

  updateLangData(nameLang, level) {
    lang[1].text = nameLang;
    lang[0].text = level;
    update();
  }

  updateEducation(
      specialization, nameOfUniversity, detials, startDate, endDate) {
    education[0].text = specialization;
    education[1].text = nameOfUniversity;
    education[2].text = detials;
    dateTimeStartLearn = startDate;
    dateTimeEndLearn = endDate;
    update();
  }

  updateEmployeeQualification(text1, text2, text3, text4, startDate, endDate) {
    employeeQualif[0].text = text1;
    employeeQualif[1].text = text2;
    employeeQualif[2].text = text3;
    employeeQualif[3].text = text4;
    dateTimeStartEmpQualification = startDate;
    dateTimeEndEmpQualification = endDate;
    update();
  }

  updateTimeCourse(value) {
    dateTimeCourse = value;
    update();
  }

  updateStartDateLean(value) {
    dateTimeStartLearn = value;
    update();
  }

  updateEndDateLean(value) {
    dateTimeEndLearn = value;
    update();
  }

    updateStartDateEmployeeQualificatio(value) {
    dateTimeStartEmpQualification = value;
    update();
  }

  updateEndDateEmployeeQualificatio(value) {
    dateTimeEndEmpQualification = value;
    update();
  }
}
