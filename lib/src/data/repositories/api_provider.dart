import 'dart:async';

import 'package:dental_kinghts/src/constants/server_addresses.dart';
import 'package:dental_kinghts/src/constants/storage.dart';
import 'package:dental_kinghts/src/data/models/book.dart';
import 'package:dental_kinghts/src/data/models/comment.dart';
import 'package:dental_kinghts/src/data/models/cv.dart';
import 'package:dental_kinghts/src/data/models/doctor_model.dart';
import 'package:dental_kinghts/src/data/models/factory.dart';
import 'package:dental_kinghts/src/data/models/patient.dart';
import 'package:dental_kinghts/src/data/models/patient_model.dart';
import 'package:dental_kinghts/src/data/models/post.dart';
import 'package:dental_kinghts/src/data/models/question.dart';
import 'package:dental_kinghts/src/data/models/t_image.dart';
import 'package:dental_kinghts/src/data/models/t_video.dart';
import 'package:http/http.dart' show Client;

import 'dart:convert';

class ApiProvider {
  Future<List<Doctor>> patient_doctors(String countryId, String cityId) async {
    Client client = Client();
    var url =
        "${ServerAddresses.serverAddress}patient_doctors?country_id=$countryId&city_id=$cityId";
    print(url);
    final response = await client.get(Uri.parse(url));
    List<Doctor> doctors = [];
    print(response.body.toString());
    if (response.statusCode == 200) {
      var res = utf8.decode(response.bodyBytes);
      var res2 = jsonDecode(response.body);
      for (var item in res2) {
        doctors.add(Doctor.fromJson(item));
      }

      client.close();
      // If the call to the server was successful, parse the JSON
      return doctors;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<Doctor> my_profile() async {
    Client client = Client();
    var url = "${ServerAddresses.serverAddress}my_profile";
    print(url);
    final response = await client.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'token': Storage().token,
    });
    Doctor doctor = Doctor();
    print(response.body.toString());
    if (response.statusCode == 200) {
      var res = utf8.decode(response.bodyBytes);
      var res2 = jsonDecode(response.body);
      doctor = Doctor.fromJson(res2);

      client.close();
      // If the call to the server was successful, parse the JSON
      return doctor;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<List<Images>> my_images() async {
    Client client = Client();
    var url = ServerAddresses.serverAddress + "my_images";
    print(url);
    final response = await client.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'token': Storage().token,
    });
    List<Images> images = [];
    print(response.body.toString());
    if (response.statusCode == 200) {
      var res = utf8.decode(response.bodyBytes);
      var res2 = jsonDecode(response.body);
      for (var item in res2) {
        images.add(Images.fromJson(item));
      }

      client.close();
      // If the call to the server was successful, parse the JSON
      return images;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<List<T_Factory>> factories(String countryId, String cityId) async {
    Client client = Client();
    var url = ServerAddresses.serverAddress +
        "factories?country_id=" +
        countryId +
        "&city_id=" +
        cityId;
    print(url);
    final response = await client.get(Uri.parse(url));
    List<T_Factory> factories = [];
    print(response.body.toString());
    if (response.statusCode == 200) {
      var res = utf8.decode(response.bodyBytes);
      var res2 = jsonDecode(response.body);
      for (var item in res2) {
        factories.add(T_Factory.fromJson(item));
      }

      client.close();
      // If the call to the server was successful, parse the JSON
      return factories;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<List<Doctor>> doctors(String specId) async {
    Client client = Client();
    var url =
        ServerAddresses.serverAddress + "doctors?specialization_id=" + specId;
    print(url);
    final response = await client.get(Uri.parse(url));
    List<Doctor> doctors = [];
    print(response.body.toString());
    if (response.statusCode == 200) {
      var res = utf8.decode(response.bodyBytes);
      var res2 = jsonDecode(response.body);
      for (var item in res2) {
        doctors.add(Doctor.fromJson(item));
      }

      client.close();
      // If the call to the server was successful, parse the JSON
      return doctors;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<List<Doctor>> cvs() async {
    Client client = Client();
    var url = ServerAddresses.serverAddress + "cvs";
    print(url);
    final response = await client.get(Uri.parse(url));
    List<Doctor> doctors = [];
    print(response.body.toString());
    if (response.statusCode == 200) {
      var res = utf8.decode(response.bodyBytes);
      var res2 = jsonDecode(response.body);
      for (var item in res2) {
        doctors.add(Doctor.fromJson(item));
      }

      client.close();
      // If the call to the server was successful, parse the JSON
      return doctors;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<CV> getCv(String? id) async {
    Client client = Client();
    var url = ServerAddresses.serverAddress + "cv?id=$id";
    print(url);
    final response = await client.get(Uri.parse(url));
    CV cv;
    print(response.body.toString());
    if (response.statusCode == 200) {
      var res2 = jsonDecode(response.body);

      cv = CV.fromJson(res2);

      client.close();
      // If the call to the server was successful, parse the JSON
      return cv;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<List<Post>> get_posts(String specId, int page) async {
    Client client = Client();
    var url = ServerAddresses.serverAddress +
        "posts?specialization_id=" +
        specId +
        "&page=${page}";
    print(url);
    final response = await client.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'token': Storage().token,
    });
    List<Post> posts = [];
    print(response.body.toString());
    if (response.statusCode == 200) {
      var res = utf8.decode(response.bodyBytes);
      var res2 = jsonDecode(response.body);
      for (var item in res2) {
        posts.add(Post.fromJson(item));
      }

      client.close();
      // If the call to the server was successful, parse the JSON
      return posts;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<List<PatientModel>> get_patients() async {
    Client client = Client();
    var url = ServerAddresses.serverAddress + "patients";

    final response = await client.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'token': Storage().token,
    });
    List<PatientModel> patients = [];
    print(response.body.toString());
    if (response.statusCode == 200) {
      var res = utf8.decode(response.bodyBytes);
      var res2 = jsonDecode(response.body);
      for (var item in res2) {
        patients.add(PatientModel.fromJson(item));
      }

      client.close();
      // If the call to the server was successful, parse the JSON
      return patients;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<List<Comment>> get_comments(String postId) async {
    Client client = Client();
    var url = ServerAddresses.serverAddress + "comments?post_id=" + postId;

    final response = await client.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'token': Storage().token,
    });
    List<Comment> comments = [];
    print(response.body.toString());
    if (response.statusCode == 200) {
      var res = utf8.decode(response.bodyBytes);
      var res2 = jsonDecode(response.body);
      for (var item in res2) {
        comments.add(Comment.fromJson(item));
      }

      client.close();

      return comments;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<List<Question>> getQuestions(
      String type, String selectedQuestion) async {
    Client client = Client();
    List<String> questions = [];
    List<Question> questionList = [];
    final data = await client.get(Uri.parse(ServerAddresses.serverAddress +
        "questions?type=" +
        type +
        '&parents=' +
        selectedQuestion));
    var responseBody = data.body;
    print(responseBody);
    final parsed = json.decode(data.body).cast<Map<String, dynamic>>();

    questionList =
        parsed.map<Question>((json) => Question.fromJson(json)).toList();

    for (int i = 0; i < questionList.length; i++) {
      questions.add(questionList[i].text ?? 'سؤال 1');
    }
    return questionList;
  }

  Future<List<Book>> getBooks() async {
    Client client = Client();
    var url = ServerAddresses.serverAddress + "books";
    print(url);
    final response = await client.get(Uri.parse(url));
    List<Book> books = [];
    print(response.body.toString());
    if (response.statusCode == 200) {
      var res = utf8.decode(response.bodyBytes);
      var res2 = jsonDecode(response.body);
      for (var item in res2) {
        books.add(Book.fromJson(item));
      }

      client.close();
      // If the call to the server was successful, parse the JSON
      return books;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<List<T_Image>> getImages() async {
    Client client = Client();
    var url = ServerAddresses.serverAddress + "images";
    print(url);
    final response = await client.get(Uri.parse(url));
    List<T_Image> images = [];
    print(response.body.toString());
    if (response.statusCode == 200) {
      var res = utf8.decode(response.bodyBytes);
      var res2 = jsonDecode(response.body);
      for (var item in res2) {
        images.add(T_Image.fromJson(item));
      }

      client.close();
      // If the call to the server was successful, parse the JSON
      return images;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<List<T_Video>> getVideos() async {
    Client client = Client();
    var url = ServerAddresses.serverAddress + "videos";
    print(url);
    final response = await client.get(Uri.parse(url));
    List<T_Video> videos = [];
    print(response.body.toString());
    if (response.statusCode == 200) {
      var res = utf8.decode(response.bodyBytes);
      var res2 = jsonDecode(response.body);
      for (var item in res2) {
        videos.add(T_Video.fromJson(item));
      }

      client.close();
      // If the call to the server was successful, parse the JSON
      return videos;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  // Future<void> uploadImage()
}
