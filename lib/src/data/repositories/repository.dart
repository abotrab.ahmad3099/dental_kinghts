import 'dart:async';

import 'package:dental_kinghts/src/data/models/book.dart';
import 'package:dental_kinghts/src/data/models/comment.dart';
import 'package:dental_kinghts/src/data/models/cv.dart';
import 'package:dental_kinghts/src/data/models/doctor_model.dart';
import 'package:dental_kinghts/src/data/models/factory.dart';
import 'package:dental_kinghts/src/data/models/patient_model.dart';
import 'package:dental_kinghts/src/data/models/post.dart';
import 'package:dental_kinghts/src/data/models/question.dart';
import 'package:dental_kinghts/src/data/models/t_image.dart';
import 'package:dental_kinghts/src/data/models/t_video.dart';
import 'package:dental_kinghts/src/data/server/countries_api.dart';

import 'api_provider.dart';

class Repository {
  final billApiProvider = ApiProvider();

  Future<List<Doctor>> patient_doctors(String counrtyId, String cityId) =>
      billApiProvider.patient_doctors(counrtyId, cityId);

  Future<List<Doctor>> doctors(String specId) =>
      billApiProvider.doctors(specId);
  Future<List<Doctor>> cvs() => billApiProvider.cvs();
  Future<List<Question>> getQuestions(String type, String selectedQuestion) =>
      billApiProvider.getQuestions(type, selectedQuestion);
  Future<List<T_Factory>> getFactories(String counrtyId, String cityId) =>
      billApiProvider.factories(counrtyId, cityId);
  Future<List<Book>> getBooks() => billApiProvider.getBooks();
  Future<List<PatientModel>> getPatients() => billApiProvider.get_patients();

  Future<List<T_Image>> getImages() => billApiProvider.getImages();
  Future<List<T_Video>> getVideos() => billApiProvider.getVideos();
  Future<Doctor> my_profile() => billApiProvider.my_profile();
  Future<List<Images>> my_images() => billApiProvider.my_images();
  Future<List<Post>> fetchPosts(String spec, int page) =>
      billApiProvider.get_posts(spec, page);

  Future<List<Comment>> getComments(String postId) =>
      billApiProvider.get_comments(postId);

  Future<CV> getCv(String? id) => billApiProvider.getCv(id);
}
