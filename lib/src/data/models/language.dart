// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class Language {
  Language({
    this.language,
    this.level,
  });

  factory Language.fromJson(String source) => Language.fromMap(json.decode(source) as Map<String, dynamic>);

  factory Language.fromMap(Map<String, dynamic> map) {
    return Language(
      language: map['language'] != null ? map['language'] as String : null,
      level: map['level'] != null ? map['level'] as String : null,
    );
  }

  String? language;
  String? level;

  @override
  bool operator ==(covariant Language other) {
    if (identical(this, other)) return true;
  
    return 
      other.language == language &&
      other.level == level;
  }

  @override
  int get hashCode => language.hashCode ^ level.hashCode;

  @override
  String toString() => 'Language(language: $language, level: $level)';

  Language copyWith({
    String? language,
    String? level,
  }) {
    return Language(
      language: language ?? this.language,
      level: level ?? this.level,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'language': language,
      'level': level,
    };
  }

  String toJson() => json.encode(toMap());
}
