// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class Education {
  Education({
    this.nameOfSpecilization,
    this.nameOfUniversity,
    this.startDate,
    this.endDate,
    this.moreDetials,
  });

  factory Education.fromJson(String source) => Education.fromMap(json.decode(source) as Map<String, dynamic>);

  factory Education.fromMap(Map<String, dynamic> map) {
    return Education(
      nameOfSpecilization: map['nameOfSpecilization'] != null ? map['nameOfSpecilization'] as String : null,
      nameOfUniversity: map['nameOfUniversity'] != null ? map['nameOfUniversity'] as String : null,
      startDate: map['startDate'] != null ? map['startDate'] as String : null,
      endDate: map['endDate'] != null ? map['endDate'] as String : null,
      moreDetials: map['moreDetials'] != null ? map['moreDetials'] as String : null,
    );
  }

  String? endDate;
  String? moreDetials;
  String? nameOfSpecilization;
  String? nameOfUniversity;
  String? startDate;

  @override
  bool operator ==(covariant Education other) {
    if (identical(this, other)) return true;
  
    return 
      other.nameOfSpecilization == nameOfSpecilization &&
      other.nameOfUniversity == nameOfUniversity &&
      other.startDate == startDate &&
      other.endDate == endDate &&
      other.moreDetials == moreDetials;
  }

  @override
  int get hashCode {
    return nameOfSpecilization.hashCode ^
      nameOfUniversity.hashCode ^
      startDate.hashCode ^
      endDate.hashCode ^
      moreDetials.hashCode;
  }

  @override
  String toString() {
    return 'Education(nameOfSpecilization: $nameOfSpecilization, nameOfUniversity: $nameOfUniversity, startDate: $startDate, endDate: $endDate, moreDetials: $moreDetials)';
  }

  Education copyWith({
    String? nameOfSpecilization,
    String? nameOfUniversity,
    String? startDate,
    String? endDate,
    String? moreDetials,
  }) {
    return Education(
      nameOfSpecilization: nameOfSpecilization ?? this.nameOfSpecilization,
      nameOfUniversity: nameOfUniversity ?? this.nameOfUniversity,
      startDate: startDate ?? this.startDate,
      endDate: endDate ?? this.endDate,
      moreDetials: moreDetials ?? this.moreDetials,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'nameOfSpecilization': nameOfSpecilization,
      'nameOfUniversity': nameOfUniversity,
      'startDate': startDate,
      'endDate': endDate,
      'moreDetials': moreDetials,
    };
  }

  String toJson() => json.encode(toMap());
}
