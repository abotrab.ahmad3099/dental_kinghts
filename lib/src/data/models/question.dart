class Question {
  String? id;
  String? text;
  String? answer;

  Question({this.id, this.text, this.answer});

  Question.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    text = json['text'];
    answer = json['answer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['text'] = this.text;
    return data;
  }
}
