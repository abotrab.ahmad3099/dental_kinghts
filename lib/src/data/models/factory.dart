class T_Factory {
  String? id;
  String? factoryName;
  String? managerName;
  String? factoryAddress;
  String? countryId;
  String? cityId;
  String? email;
  String? mobile;
  String? recordNumber;
  String? factoryLogo;
  String? workerNumbers;
  String? isActive;
  String? createdDate;
  String? createdBy;
  String? sortname;
  String? name;
  String? phonecode;
  String? selected;
  String? country;
  String? city;
  List<FactoryImages>? factoryImages;
  List<FactoryWorkImages>? factoryWorkImages;
  List<Products>? products;

  T_Factory(
      {this.id,
      this.factoryName,
      this.managerName,
      this.factoryAddress,
      this.countryId,
      this.cityId,
      this.email,
      this.mobile,
      this.recordNumber,
      this.factoryLogo,
      this.workerNumbers,
      this.isActive,
      this.createdDate,
      this.createdBy,
      this.sortname,
      this.name,
      this.country,
      this.city,
      this.phonecode,
      this.selected,
      this.factoryImages,
      this.factoryWorkImages,
      this.products});

  T_Factory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    factoryName = json['factory_name'];
    managerName = json['manager_name'];
    factoryAddress = json['factory_address'];
    countryId = json['country_id'];
    cityId = json['city_id'];
    email = json['email'];
    mobile = json['mobile'];
    recordNumber = json['record_number'];
    factoryLogo = json['factory_logo'];
    workerNumbers = json['worker_numbers'];
    isActive = json['is_active'];
    createdDate = json['created_date'];
    createdBy = json['created_by'];
    sortname = json['sortname'];
    name = json['name'];
    country = json['country'];
    city = json['city'];
    // phonecode = json['phonecode'];
    selected = json['selected'];
    if (json['factory_images'] != null) {
      factoryImages = <FactoryImages>[];
      json['factory_images'].forEach((v) {
        factoryImages!.add(new FactoryImages.fromJson(v));
      });
    }
    if (json['factory_work_images'] != null) {
      factoryWorkImages = <FactoryWorkImages>[];
      json['factory_work_images'].forEach((v) {
        factoryWorkImages!.add(new FactoryWorkImages.fromJson(v));
      });
    }
    if (json['products'] != null) {
      products = <Products>[];
      json['products'].forEach((v) {
        products!.add(new Products.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['factory_name'] = this.factoryName;
    data['manager_name'] = this.managerName;
    data['factory_address'] = this.factoryAddress;
    data['country_id'] = this.countryId;
    data['city_id'] = this.cityId;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['record_number'] = this.recordNumber;
    data['factory_logo'] = this.factoryLogo;
    data['worker_numbers'] = this.workerNumbers;
    data['is_active'] = this.isActive;
    data['created_date'] = this.createdDate;
    data['created_by'] = this.createdBy;
    data['sortname'] = this.sortname;
    data['country'] = this.country;
    data['city'] = this.city;
    data['name'] = this.name;
    data['phonecode'] = this.phonecode;
    data['selected'] = this.selected;
    if (this.factoryImages != null) {
      data['factory_images'] =
          this.factoryImages!.map((v) => v.toJson()).toList();
    }
    if (this.factoryWorkImages != null) {
      data['factory_work_images'] =
          this.factoryWorkImages!.map((v) => v.toJson()).toList();
    }
    if (this.products != null) {
      data['products'] = this.products!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class FactoryImages {
  String? id;
  String? factoryId;
  String? factoryImage;
  String? createdDate;

  FactoryImages({this.id, this.factoryId, this.factoryImage, this.createdDate});

  FactoryImages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    factoryId = json['factory_id'];
    factoryImage = json['factory_image'];
    createdDate = json['created_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['factory_id'] = this.factoryId;
    data['factory_image'] = this.factoryImage;
    data['created_date'] = this.createdDate;
    return data;
  }
}

class FactoryWorkImages {
  String? id;
  String? factoryId;
  String? factoryWorkImage;
  String? createdDate;

  FactoryWorkImages(
      {this.id, this.factoryId, this.factoryWorkImage, this.createdDate});

  FactoryWorkImages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    factoryId = json['factory_id'];
    factoryWorkImage = json['factory_work_image'];
    createdDate = json['created_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['factory_id'] = this.factoryId;
    data['factory_work_image'] = this.factoryWorkImage;
    data['created_date'] = this.createdDate;
    return data;
  }
}

class Products {
  String? id;
  String? factoryId;
  String? productName;
  String? productPrice;

  Products({this.id, this.factoryId, this.productName, this.productPrice});

  Products.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    factoryId = json['factory_id'];
    productName = json['product_name'];
    productPrice = json['product_price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['factory_id'] = this.factoryId;
    data['product_name'] = this.productName;
    data['product_price'] = this.productPrice;
    return data;
  }
}
