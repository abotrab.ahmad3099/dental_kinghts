class Specialization {
  String? id;
  String? specName;
  String? isActive;

  Specialization({this.id, this.specName, this.isActive});

  Specialization.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    specName = json['spec_name'];
    isActive = json['is_active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['spec_name'] = this.specName;
    data['is_active'] = this.isActive;
    return data;
  }
}
