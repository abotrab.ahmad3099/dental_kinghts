// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class Lab {
  Lab({
    this.nameLab,
  });

  factory Lab.fromJson(String source) => Lab.fromMap(json.decode(source) as Map<String, dynamic>);

  factory Lab.fromMap(Map<String, dynamic> map) {
    return Lab(
      nameLab: map['nameLab'] != null ? map['nameLab'] as String : null,
    );
  }

  String? nameLab;

  @override
  bool operator ==(covariant Lab other) {
    if (identical(this, other)) return true;
  
    return 
      other.nameLab == nameLab;
  }

  @override
  int get hashCode => nameLab.hashCode;

  @override
  String toString() => 'Lab(nameLab: $nameLab)';

  Lab copyWith({
    String? nameLab,
  }) {
    return Lab(
      nameLab: nameLab ?? this.nameLab,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'nameLab': nameLab,
    };
  }

  String toJson() => json.encode(toMap());
}
