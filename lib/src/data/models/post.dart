import 'package:dental_kinghts/src/constants/server_addresses.dart';

class Post {
  String? id;
  String? specilizationId;
  String? postDescription;
  String? postVideo;
  String? createdDate;
  String? createdBy;
  String? customer;
  String? specialization;
  String? likes;
  String? comments;
  bool? isLiked;
  List<PostImages>? images;

  Post(
      {this.id,
      this.specilizationId,
      this.postDescription,
      this.postVideo,
      this.createdDate,
      this.createdBy,
      this.customer,
      this.specialization,
      this.images,
      this.isLiked,
      this.likes,
      this.comments});

  Post.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    specilizationId = json['specilization_id'];
    postDescription = json['post_description'];
    postVideo = json['post_video'];
    createdDate = json['created_date'];
    createdBy = json['created_by'];
    customer = json['customer'];
    likes = json['likes'];
    comments = json['comments_count'];
    isLiked = json['isLiked'] == "1" ? true : false;
    specialization = json['specialization'];
    if (json['post_images'] != null) {
      images = <PostImages>[];
      json['post_images'].forEach((v) {
        images!.add(new PostImages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['specilization_id'] = specilizationId;
    data['post_description'] = postDescription;
    data['post_video'] = postVideo;
    data['created_date'] = createdDate;
    data['created_by'] = createdBy;
    data['customer'] = customer;
    data['specialization'] = specialization;
    data['likes'] = likes;
    if (images != null) {
      data['images'] = images!.map((v) => v.toJson()).toList();
    }

    return data;
  }
}

class PostImages {
  String? id;
  String? image;
  String? postId;

  PostImages({this.id, this.image, this.postId});

  PostImages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = ServerAddresses.uploadUrl + json['image'];
    postId = json['post_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['image'] = image;
    data['post_id'] = postId;
    return data;
  }
}
