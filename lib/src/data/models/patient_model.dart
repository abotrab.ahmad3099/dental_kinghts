class PatientModel {
  String? id;
  String? userId;
  String? name;
  String? number;
  String? notes;

  PatientModel({this.id, this.userId, this.name, this.number, this.notes});

  PatientModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    userId = json['user_id'];
    name = json['name'];
    number = json['number'];
    notes = json['notes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['name'] = this.name;
    data['number'] = this.number;
    data['notes'] = this.notes;
    return data;
  }
}
