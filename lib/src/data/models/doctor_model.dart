import 'package:dental_kinghts/src/constants/server_addresses.dart';

class Doctor {
  String? id;
  String? name;
  String? phone;
  String? password;
  String? email;
  String? address;
  String? createdDate;
  String? registerDate;
  String? firebaseToken;
  String? info;
  String? medicalComplex;
  String? nationality;
  String? adressOfMedicalComplext;
  String? gender;
  String? yearExperience;
  String? image;
  String? country;
  String? city;
  String? specialization;
  List<Images>? images;

  Doctor(
      {this.id,
      this.name,
      this.phone,
      this.password,
      this.email,
      this.address,
      this.createdDate,
      this.registerDate,
      this.firebaseToken,
      this.info,
      this.medicalComplex,
      this.nationality,
      this.adressOfMedicalComplext,
      this.gender,
      this.yearExperience,
      this.image,
      this.country,
      this.city,
      this.specialization,
      this.images});

  Doctor.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    phone = json['mobile'];
    password = json['password'];
    email = json['email'];
    address = json['address'];
    createdDate = json['created_date'];
    registerDate = json['register_date'];
    firebaseToken = json['firebase_token'];
    info = json['info'];
    medicalComplex = json['medicalComplex'];
    nationality = json['nationality'];
    adressOfMedicalComplext = json['adressOfMedicalComplext'];
    gender = json['gender'];
    yearExperience = json['yearExperience'];
    image = ServerAddresses.uploadUrl + (json['image'] ?? 'default.png');
    country = json['country'];
    city = json['city'];
    specialization = json['specialization'];
    if (json['images'] != null) {
      images = <Images>[];
      json['images'].forEach((v) {
        images!.add(new Images.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['mobile'] = this.phone;
    data['password'] = this.password;
    data['email'] = this.email;
    data['address'] = this.address;
    data['created_date'] = this.createdDate;
    data['register_date'] = this.registerDate;
    data['firebase_token'] = this.firebaseToken;
    data['info'] = this.info;
    data['medicalComplex'] = this.medicalComplex;
    data['nationality'] = this.nationality;
    data['adressOfMedicalComplext'] = this.adressOfMedicalComplext;
    data['gender'] = this.gender;
    data['yearExperience'] = this.yearExperience;
    data['image'] = ServerAddresses.uploadUrl + (this.image ?? 'default.png');
    data['country'] = this.country;
    data['city'] = this.city;
    data['specialization'] = this.specialization;
    if (this.images != null) {
      data['images'] = this.images!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Images {
  String? id;
  String? customerId;
  String? image;
  String? isPublic;
  String? imageTitle;
  String? PatientNumber;
  String? PatientNotes;

  Images(
      {this.id, this.customerId, this.image, this.isPublic, this.imageTitle});

  Images.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customerId = json['customer_id'];
    image = ServerAddresses.uploadUrl + (json['image'] ?? 'default.png');
    isPublic = json['is_public'];
    imageTitle = json['name'] != null ? json['name'] : json['image_title'];
    PatientNumber = json['number'];
    PatientNotes = json['notes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['customer_id'] = this.customerId;
    data['image'] = this.image;
    data['is_public'] = this.isPublic;
    return data;
  }
}
