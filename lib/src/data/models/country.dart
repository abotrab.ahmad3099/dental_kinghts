import 'package:dental_kinghts/src/data/models/city.dart';

class Country {
  String? id;
  String? sortname;
  String? name;
  String? phonecode;
  String? selected;
  List<Cities>? states;
  Country(
      {this.id,
      this.sortname,
      this.name,
      this.phonecode,
      this.selected,
      this.states});

  Country.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sortname = json['sortname'];
    name = json['name'];
    phonecode = json['phonecode'];
    selected = json['selected'];
    if (json['cities'] != null) {
      states = <Cities>[];
      json['cities'].forEach((v) {
        states!.add(Cities.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['sortname'] = sortname;
    data['name'] = name;
    data['phonecode'] = phonecode;
    data['selected'] = selected;
    return data;
  }
}
