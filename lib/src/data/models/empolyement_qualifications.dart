// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class EmpolyementQualifications {
  EmpolyementQualifications({
    this.idDoctor,
    this.nameOfJobs,
    this.countryCity,
    this.nameOfComapny,
    this.startDate,
    this.endDate,
    this.moreDetials,
  });

  factory EmpolyementQualifications.fromJson(String source) => EmpolyementQualifications.fromMap(json.decode(source) as Map<String, dynamic>);

  factory EmpolyementQualifications.fromMap(Map<String, dynamic> map) {
    return EmpolyementQualifications(
      idDoctor: map['idDoctor'] != null ? map['idDoctor'] as String : null,
      nameOfJobs: map['nameOfJobs'] != null ? map['nameOfJobs'] as String : null,
      countryCity: map['countryCity'] != null ? map['countryCity'] as String : null,
      nameOfComapny: map['nameOfComapny'] != null ? map['nameOfComapny'] as String : null,
      startDate: map['startDate'] != null ? map['startDate'] as String : null,
      endDate: map['endDate'] != null ? map['endDate'] as String : null,
      moreDetials: map['moreDetials'] != null ? map['moreDetials'] as String : null,
    );
  }

  String? countryCity;
  String? endDate;
  String? idDoctor;
  String? moreDetials;
  String? nameOfComapny;
  String? nameOfJobs;
  String? startDate;

  @override
  bool operator ==(covariant EmpolyementQualifications other) {
    if (identical(this, other)) return true;
  
    return 
      other.idDoctor == idDoctor &&
      other.nameOfJobs == nameOfJobs &&
      other.countryCity == countryCity &&
      other.nameOfComapny == nameOfComapny &&
      other.startDate == startDate &&
      other.endDate == endDate &&
      other.moreDetials == moreDetials;
  }

  @override
  int get hashCode {
    return idDoctor.hashCode ^
      nameOfJobs.hashCode ^
      countryCity.hashCode ^
      nameOfComapny.hashCode ^
      startDate.hashCode ^
      endDate.hashCode ^
      moreDetials.hashCode;
  }

  @override
  String toString() {
    return 'EmpolyementQualifications(idDoctor: $idDoctor, nameOfJobs: $nameOfJobs, countryCity: $countryCity, nameOfComapny: $nameOfComapny, startDate: $startDate, endDate: $endDate, moreDetials: $moreDetials)';
  }

  EmpolyementQualifications copyWith({
    String? idDoctor,
    String? nameOfJobs,
    String? countryCity,
    String? nameOfComapny,
    String? startDate,
    String? endDate,
    String? moreDetials,
  }) {
    return EmpolyementQualifications(
      idDoctor: idDoctor ?? this.idDoctor,
      nameOfJobs: nameOfJobs ?? this.nameOfJobs,
      countryCity: countryCity ?? this.countryCity,
      nameOfComapny: nameOfComapny ?? this.nameOfComapny,
      startDate: startDate ?? this.startDate,
      endDate: endDate ?? this.endDate,
      moreDetials: moreDetials ?? this.moreDetials,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'idDoctor': idDoctor,
      'nameOfJobs': nameOfJobs,
      'countryCity': countryCity,
      'nameOfComapny': nameOfComapny,
      'startDate': startDate,
      'endDate': endDate,
      'moreDetials': moreDetials,
    };
  }

  String toJson() => json.encode(toMap());
}
