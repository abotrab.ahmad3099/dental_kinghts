// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class Hobbit {
  Hobbit({
    this.nameOfHobbit,
  });

  factory Hobbit.fromJson(String source) => Hobbit.fromMap(json.decode(source) as Map<String, dynamic>);

  factory Hobbit.fromMap(Map<String, dynamic> map) {
    return Hobbit(
      nameOfHobbit: map['nameOfHobbit'] != null ? map['nameOfHobbit'] as String : null,
    );
  }

  String? nameOfHobbit;

  @override
  bool operator ==(covariant Hobbit other) {
    if (identical(this, other)) return true;
  
    return 
      other.nameOfHobbit == nameOfHobbit;
  }

  @override
  int get hashCode => nameOfHobbit.hashCode;

  @override
  String toString() => 'Hobbit(nameOfHobbit: $nameOfHobbit)';

  Hobbit copyWith({
    String? nameOfHobbit,
  }) {
    return Hobbit(
      nameOfHobbit: nameOfHobbit ?? this.nameOfHobbit,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'nameOfHobbit': nameOfHobbit,
    };
  }

  String toJson() => json.encode(toMap());
}
