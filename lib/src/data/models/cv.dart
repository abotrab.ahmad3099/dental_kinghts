import 'package:dental_kinghts/src/data/models/doctor_model.dart';

class CV {
  PersonalData? personalData;
  List<Hobbits>? hobbits;
  List<Courses>? courses;
  List<Languages>? languages;
  List<Educations>? educations;
  List<Experiences>? experiences;

  CV(
      {this.personalData,
      this.hobbits,
      this.courses,
      this.languages,
      this.educations,
      this.experiences});

  CV.fromJson(Map<String, dynamic> json) {
    personalData = json['personal_data'] != null
        ? PersonalData.fromJson(json['personal_data'])
        : null;
    if (json['hobbits'] != null) {
      hobbits = <Hobbits>[];
      json['hobbits'].forEach((v) {
        hobbits!.add(Hobbits.fromJson(v));
      });
    }
    if (json['courses'] != null) {
      courses = <Courses>[];
      json['courses'].forEach((v) {
        courses!.add(Courses.fromJson(v));
      });
    }
    if (json['languages'] != null) {
      languages = <Languages>[];
      json['languages'].forEach((v) {
        languages!.add(Languages.fromJson(v));
      });
    }
    if (json['educations'] != null) {
      educations = <Educations>[];
      json['educations'].forEach((v) {
        educations!.add(Educations.fromJson(v));
      });
    }
    if (json['experiences'] != null) {
      experiences = <Experiences>[];
      json['experiences'].forEach((v) {
        experiences!.add(Experiences.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.personalData != null) {
      data['personal_data'] = this.personalData!.toJson();
    }
    if (this.hobbits != null) {
      data['hobbits'] = this.hobbits!.map((v) => v.toJson()).toList();
    }
    if (this.courses != null) {
      data['courses'] = this.courses!.map((v) => v.toJson()).toList();
    }
    if (this.languages != null) {
      data['languages'] = this.languages!.map((v) => v.toJson()).toList();
    }
    if (this.educations != null) {
      data['educations'] = this.educations!.map((v) => v.toJson()).toList();
    }
    if (this.experiences != null) {
      data['experiences'] = this.experiences!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PersonalData {
  String? id;
  String? name;
  String? mobile;
  String? email;
  String? address;
  String? createdDate;
  String? registerDate;
  String? info;
  String? medicalComplex;
  String? nationality;
  String? adressOfMedicalComplext;
  String? gender;
  String? yearExperience;
  String? image;
  String? uploadCv;
  String? birthdate;
  String? activeCv;
  String? country;
  String? city;
  String? specialization;
  List<Images>? images;

  PersonalData(
      {this.id,
      this.name,
      this.mobile,
      this.email,
      this.address,
      this.createdDate,
      this.registerDate,
      this.info,
      this.medicalComplex,
      this.nationality,
      this.adressOfMedicalComplext,
      this.gender,
      this.yearExperience,
      this.image,
      this.uploadCv,
      this.birthdate,
      this.activeCv,
      this.country,
      this.city,
      this.specialization,
      this.images});

  PersonalData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    mobile = json['mobile'];
    email = json['email'];
    address = json['address'];
    createdDate = json['created_date'];
    registerDate = json['register_date'];
    info = json['info'];
    medicalComplex = json['medicalComplex'];
    nationality = json['nationality'];
    adressOfMedicalComplext = json['adressOfMedicalComplext'];
    gender = json['gender'];
    yearExperience = json['yearExperience'];
    image = json['image'];
    uploadCv = json['upload_cv'];
    birthdate = json['birthdate'];
    activeCv = json['active_cv'];
    country = json['country'];
    city = json['city'];
    specialization = json['specialization'];
    if (json['images'] != null) {
      images = <Images>[];
      json['images'].forEach((v) {
        images!.add(Images.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['mobile'] = this.mobile;
    data['email'] = this.email;
    data['address'] = this.address;
    data['created_date'] = this.createdDate;
    data['register_date'] = this.registerDate;
    data['info'] = this.info;
    data['medicalComplex'] = this.medicalComplex;
    data['nationality'] = this.nationality;
    data['adressOfMedicalComplext'] = this.adressOfMedicalComplext;
    data['gender'] = this.gender;
    data['yearExperience'] = this.yearExperience;
    data['image'] = this.image;
    data['upload_cv'] = this.uploadCv;
    data['birthdate'] = this.birthdate;
    data['active_cv'] = this.activeCv;
    data['country'] = this.country;
    data['city'] = this.city;
    data['specialization'] = this.specialization;
    if (this.images != null) {
      data['images'] = this.images!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Hobbits {
  String? id;
  String? hobbitName;
  String? customerId;

  Hobbits({this.id, this.hobbitName, this.customerId});

  Hobbits.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    hobbitName = json['hobbit_name'];
    customerId = json['customer_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['hobbit_name'] = this.hobbitName;
    data['customer_id'] = this.customerId;
    return data;
  }
}

class Courses {
  String? id;
  String? courseName;
  String? courseDate;
  String? customerId;

  Courses({this.id, this.courseName, this.courseDate, this.customerId});

  Courses.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    courseName = json['course_name'];
    courseDate = json['course_date'];
    customerId = json['customer_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['course_name'] = this.courseName;
    data['course_date'] = this.courseDate;
    data['customer_id'] = this.customerId;
    return data;
  }
}

class Languages {
  String? id;
  String? customerId;
  String? language;
  String? level;

  Languages({this.id, this.customerId, this.language, this.level});

  Languages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customerId = json['customer_id'];
    language = json['language'];
    level = json['level'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['customer_id'] = this.customerId;
    data['language'] = this.language;
    data['level'] = this.level;
    return data;
  }
}

class Educations {
  String? id;
  String? customerId;
  String? specName;
  String? university;
  String? eduDetails;
  String? startDate;
  String? endDate;

  Educations(
      {this.id,
      this.customerId,
      this.specName,
      this.university,
      this.eduDetails,
      this.startDate,
      this.endDate});

  Educations.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customerId = json['customer_id'];
    specName = json['spec_name'];
    university = json['university'];
    eduDetails = json['edu_details'];
    startDate = json['start_date'];
    endDate = json['end_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['customer_id'] = this.customerId;
    data['spec_name'] = this.specName;
    data['university'] = this.university;
    data['edu_details'] = this.eduDetails;
    data['start_date'] = this.startDate;
    data['end_date'] = this.endDate;
    return data;
  }
}

class Experiences {
  String? id;
  String? customerId;
  String? jobName;
  String? companyName;
  String? exprienceDetails;
  String? countryCity;
  String? expStartDate;
  String? expEndDate;

  Experiences(
      {this.id,
      this.customerId,
      this.jobName,
      this.companyName,
      this.exprienceDetails,
      this.countryCity,
      this.expStartDate,
      this.expEndDate});

  Experiences.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customerId = json['customer_id'];
    jobName = json['job_name'];
    companyName = json['company_name'];
    exprienceDetails = json['exprience_details'];
    countryCity = json['country_city'];
    expStartDate = json['exp_start_date'];
    expEndDate = json['exp_end_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['customer_id'] = this.customerId;
    data['job_name'] = this.jobName;
    data['company_name'] = this.companyName;
    data['exprience_details'] = this.exprienceDetails;
    data['country_city'] = this.countryCity;
    data['exp_start_date'] = this.expStartDate;
    data['exp_end_date'] = this.expEndDate;
    return data;
  }
}
