// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class Address {
  Address({
    this.country,
    this.city,
    this.street,
  });

  factory Address.fromJson(String source) =>
      Address.fromMap(json.decode(source) as Map<String, dynamic>);

  factory Address.fromMap(Map<String, dynamic> map) {
    return Address(
      country: map['country'] != null ? map['country'] as String : null,
      city: map['city'] != null ? map['city'] as String : null,
      street: map['street'] != null ? map['street'] as String : null,
    );
  }

  String? city;
  String? country;
  String? street;

  @override
  bool operator ==(covariant Address other) {
    if (identical(this, other)) return true;

    return other.country == country &&
        other.city == city &&
        other.street == street;
  }

  @override
  int get hashCode => country.hashCode ^ city.hashCode ^ street.hashCode;

  @override
  String toString() =>
      'Address(country: $country, city: $city, street: $street)';

  Address copyWith({
    String? country,
    String? city,
    String? street,
  }) {
    return Address(
      country: country ?? this.country,
      city: city ?? this.city,
      street: street ?? this.street,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'country': country,
      'city': city,
      'street': street,
    };
  }

  String toJson() => json.encode(toMap());
}
