// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:dental_kinghts/src/data/models/education.dart';
import 'package:flutter/foundation.dart';

import 'package:dental_kinghts/src/data/models/course.dart';
import 'package:dental_kinghts/src/data/models/empolyement_qualifications.dart';
import 'package:dental_kinghts/src/data/models/hobbit.dart';
import 'package:dental_kinghts/src/data/models/language.dart';
import 'package:dental_kinghts/src/data/models/skill.dart';

class Experiences {
  String? introduction;
  List<EmpolyementQualifications>? empolyementQualifications;
  List<Education>? educations;
  List<Hobbit>? hobbits;
  List<Language>? languages;
  List<Skill>? skills;
  List<Course>? courses;
  Experiences({
    this.introduction,
    this.empolyementQualifications,
    this.educations,
    this.hobbits,
    this.languages,
    this.skills,
    this.courses,
  });

  Experiences copyWith({
    String? introduction,
    List<EmpolyementQualifications>? empolyementQualifications,
    List<Education>? educations,
    List<Hobbit>? hobbits,
    List<Language>? languages,
    List<Skill>? skills,
    List<Course>? courses,
  }) {
    return Experiences(
      introduction: introduction ?? this.introduction,
      empolyementQualifications:
          empolyementQualifications ?? this.empolyementQualifications,
      educations: educations ?? this.educations,
      hobbits: hobbits ?? this.hobbits,
      languages: languages ?? this.languages,
      skills: skills ?? this.skills,
      courses: courses ?? this.courses,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'introduction': introduction,
      'empolyementQualifications':
          empolyementQualifications!.map((x) => x.toMap()).toList(),
      'educations': educations!.map((x) => x.toMap()).toList(),
      'hobbits': hobbits!.map((x) => x.toMap()).toList(),
      'languages': languages!.map((x) => x.toMap()).toList(),
      'skills': skills!.map((x) => x.toMap()).toList(),
      'courses': courses!.map((x) => x.toMap()).toList(),
    };
  }

  factory Experiences.fromMap(Map<String, dynamic> map) {
    return Experiences(
      introduction:
          map['introduction'] != null ? map['introduction'] as String : null,
      empolyementQualifications: map['empolyementQualifications'] != null
          ? List<EmpolyementQualifications>.from(
              (map['empolyementQualifications'] as List<int>)
                  .map<EmpolyementQualifications?>(
                (x) => EmpolyementQualifications.fromMap(
                    x as Map<String, dynamic>),
              ),
            )
          : null,
      educations: map['educations'] != null
          ? List<Education>.from(
              (map['educations'] as List<int>).map<Education?>(
                (x) => Education.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      hobbits: map['hobbits'] != null
          ? List<Hobbit>.from(
              (map['hobbits'] as List<int>).map<Hobbit?>(
                (x) => Hobbit.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      languages: map['languages'] != null
          ? List<Language>.from(
              (map['languages'] as List<int>).map<Language?>(
                (x) => Language.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      skills: map['skills'] != null
          ? List<Skill>.from(
              (map['skills'] as List<int>).map<Skill?>(
                (x) => Skill.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      courses: map['courses'] != null
          ? List<Course>.from(
              (map['courses'] as List<int>).map<Course?>(
                (x) => Course.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Experiences.fromJson(String source) =>
      Experiences.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Experiences(introduction: $introduction, empolyementQualifications: $empolyementQualifications, educations: $educations, hobbits: $hobbits, languages: $languages, skills: $skills, courses: $courses)';
  }

  @override
  bool operator ==(covariant Experiences other) {
    if (identical(this, other)) return true;

    return other.introduction == introduction &&
        listEquals(
            other.empolyementQualifications, empolyementQualifications) &&
        listEquals(other.educations, educations) &&
        listEquals(other.hobbits, hobbits) &&
        listEquals(other.languages, languages) &&
        listEquals(other.skills, skills) &&
        listEquals(other.courses, courses);
  }

  @override
  int get hashCode {
    return introduction.hashCode ^
        empolyementQualifications.hashCode ^
        educations.hashCode ^
        hobbits.hashCode ^
        languages.hashCode ^
        skills.hashCode ^
        courses.hashCode;
  }
}
