// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class Course {
  String? nameCourse;
  String? date;
  Course({
    this.nameCourse,
    this.date,
  });
  

  Course copyWith({
    String? nameCourse,
    String? date,
  }) {
    return Course(
      nameCourse: nameCourse ?? this.nameCourse,
      date: date ?? this.date,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'nameCourse': nameCourse,
      'date': date,
    };
  }

  factory Course.fromMap(Map<String, dynamic> map) {
    return Course(
      nameCourse: map['nameCourse'] != null ? map['nameCourse'] as String : null,
      date: map['date'] != null ? map['date'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Course.fromJson(String source) => Course.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'Course(nameCourse: $nameCourse, date: $date)';

  @override
  bool operator ==(covariant Course other) {
    if (identical(this, other)) return true;
  
    return 
      other.nameCourse == nameCourse &&
      other.date == date;
  }

  @override
  int get hashCode => nameCourse.hashCode ^ date.hashCode;
}
