import '../../constants/server_addresses.dart';

class T_Video {
  String? id;
  String? videoTitle;
  String? videoImage;
  String? video;
  String? isActive;
  String? createdDate;

  T_Video(
      {this.id,
      this.videoTitle,
      this.videoImage,
      this.video,
      this.isActive,
      this.createdDate});

  T_Video.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    videoTitle = json['video_title'];
    videoImage = ServerAddresses.uploadUrl + json['video_image'];
    video = ServerAddresses.uploadUrl + "videos/" + json['video'];
    isActive = json['is_active'];
    createdDate = json['created_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['video_title'] = this.videoTitle;
    data['video_image'] = this.videoImage;
    data['video'] = this.video;
    data['is_active'] = this.isActive;
    data['created_date'] = this.createdDate;
    return data;
  }
}
