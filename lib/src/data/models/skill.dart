// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class Skill {
  Skill({
    this.nameSkill,
    this.level,
  });

  factory Skill.fromJson(String source) => Skill.fromMap(json.decode(source) as Map<String, dynamic>);

  factory Skill.fromMap(Map<String, dynamic> map) {
    return Skill(
      nameSkill: map['nameSkill'] != null ? map['nameSkill'] as String : null,
      level: map['level'] != null ? map['level'] as String : null,
    );
  }

  String? level;
  String? nameSkill;

  @override
  bool operator ==(covariant Skill other) {
    if (identical(this, other)) return true;
  
    return 
      other.nameSkill == nameSkill &&
      other.level == level;
  }

  @override
  int get hashCode => nameSkill.hashCode ^ level.hashCode;

  @override
  String toString() => 'Skill(nameSkill: $nameSkill, level: $level)';

  Skill copyWith({
    String? nameSkill,
    String? level,
  }) {
    return Skill(
      nameSkill: nameSkill ?? this.nameSkill,
      level: level ?? this.level,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'nameSkill': nameSkill,
      'level': level,
    };
  }

  String toJson() => json.encode(toMap());
}
