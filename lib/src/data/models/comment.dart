class Comment {
  String? id;
  String? postId;
  String? userId;
  String? comment;
  String? createdDate;
  String? customer;

  Comment(
      {this.id,
      this.postId,
      this.userId,
      this.comment,
      this.createdDate,
      this.customer});

  Comment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    postId = json['post_id'];
    userId = json['user_id'];
    comment = json['comment'];
    createdDate = json['created_date'];
    customer = json['customer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['post_id'] = this.postId;
    data['user_id'] = this.userId;
    data['comment'] = this.comment;
    data['created_date'] = this.createdDate;
    data['customer'] = this.customer;
    return data;
  }
}
