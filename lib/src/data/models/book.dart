import 'package:dental_kinghts/src/constants/server_addresses.dart';

class Book {
  String? id;
  String? bookName;
  String? bookImage;
  String? ratings;
  String? file;
  String? isActive;

  Book(
      {this.id,
      this.bookName,
      this.bookImage,
      this.ratings,
      this.file,
      this.isActive});

  Book.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bookName = json['bookName'];
    bookImage = ServerAddresses.uploadUrl + json['bookImage'];
    ratings = json['ratings'];
    file = ServerAddresses.uploadUrl + 'books/' + json['file'];
    isActive = json['is_active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['bookName'] = this.bookName;
    data['bookImage'] = this.bookImage;
    data['ratings'] = this.ratings;
    data['file'] = this.file;
    data['is_active'] = this.isActive;
    return data;
  }
}
