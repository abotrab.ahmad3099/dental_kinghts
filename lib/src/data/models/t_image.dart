import '../../constants/server_addresses.dart';

class T_Image {
  String? id;
  String? imageTitle;
  String? image;
  String? isActive;
  String? createdDate;

  T_Image(
      {this.id, this.imageTitle, this.image, this.isActive, this.createdDate});

  T_Image.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imageTitle = json['image_title'];
    image = ServerAddresses.uploadUrl + json['image'];
    isActive = json['is_active'];
    createdDate = json['created_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image_title'] = this.imageTitle;
    data['image'] = this.image;
    data['is_active'] = this.isActive;
    data['created_date'] = this.createdDate;
    return data;
  }
}
