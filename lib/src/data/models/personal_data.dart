// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';
import 'dart:io';

class PersonalData {
  PersonalData({
    this.address,
    this.birthDate,
    this.city,
    this.civilStatus,
    this.country,
    this.email,
    this.firstName,
    this.gender,
    this.lastName,
    this.linkedInLink,
    this.nameOfEmployee,
    this.natinality,
    this.phone,
    this.placeBirth,
    this.image,
  });

  factory PersonalData.fromJson(String source) =>
      PersonalData.fromMap(json.decode(source) as Map<String, dynamic>);

  factory PersonalData.fromMap(Map<String, dynamic> map) {
    return PersonalData(
      address: map['address'] != null ? map['address'] as String : null,
      birthDate: map['birthDate'] != null ? map['birthDate'] as String : null,
      city: map['city'] != null ? map['city'] as String : null,
      civilStatus:
          map['civilStatus'] != null ? map['civilStatus'] as String : null,
      country: map['country'] != null ? map['country'] as String : null,
      email: map['email'] != null ? map['email'] as String : null,
      firstName: map['firstName'] != null ? map['firstName'] as String : null,
      gender: map['gender'] != null ? map['gender'] as String : null,
      lastName: map['lastName'] != null ? map['lastName'] as String : null,
      linkedInLink:
          map['linkedInLink'] != null ? map['linkedInLink'] as String : null,
      nameOfEmployee: map['nameOfEmployee'] != null
          ? map['nameOfEmployee'] as String
          : null,
      natinality:
          map['natinality'] != null ? map['natinality'] as String : null,
      phone: map['phone'] != null ? map['phone'] as String : null,
      placeBirth:
          map['placeBirth'] != null ? map['placeBirth'] as String : null,
    
    );
  }

  String? address;
  String? birthDate;
  String? city;
  String? civilStatus;
  String? country;
  String? email;
  String? firstName;
  String? gender;
  File? image;
  String? lastName;
  String? linkedInLink;
  String? nameOfEmployee;
  String? natinality;
  String? phone;
  String? placeBirth;

  @override
  bool operator ==(covariant PersonalData other) {
    if (identical(this, other)) return true;

    return other.address == address &&
        other.birthDate == birthDate &&
        other.city == city &&
        other.civilStatus == civilStatus &&
        other.country == country &&
        other.email == email &&
        other.firstName == firstName &&
        other.gender == gender &&
        other.lastName == lastName &&
        other.linkedInLink == linkedInLink &&
        other.nameOfEmployee == nameOfEmployee &&
        other.natinality == natinality &&
        other.phone == phone &&
        other.placeBirth == placeBirth &&
        other.image == image;
  }

  @override
  int get hashCode {
    return address.hashCode ^
        birthDate.hashCode ^
        city.hashCode ^
        civilStatus.hashCode ^
        country.hashCode ^
        email.hashCode ^
        firstName.hashCode ^
        gender.hashCode ^
        lastName.hashCode ^
        linkedInLink.hashCode ^
        nameOfEmployee.hashCode ^
        natinality.hashCode ^
        phone.hashCode ^
        placeBirth.hashCode ^
        image.hashCode;
  }

  @override
  String toString() {
    return 'PersonalData(address: $address, birthDate: $birthDate, city: $city, civilStatus: $civilStatus, country: $country, email: $email, firstName: $firstName, gender: $gender, lastName: $lastName, linkedInLink: $linkedInLink, nameOfEmployee: $nameOfEmployee, natinality: $natinality, phone: $phone, placeBirth: $placeBirth, image: $image)';
  }

  PersonalData copyWith({
    String? address,
    String? birthDate,
    String? city,
    String? civilStatus,
    String? country,
    String? email,
    String? firstName,
    String? gender,
    String? lastName,
    String? linkedInLink,
    String? nameOfEmployee,
    String? natinality,
    String? phone,
    String? placeBirth,
    File? image,
  }) {
    return PersonalData(
      address: address ?? this.address,
      birthDate: birthDate ?? this.birthDate,
      city: city ?? this.city,
      civilStatus: civilStatus ?? this.civilStatus,
      country: country ?? this.country,
      email: email ?? this.email,
      firstName: firstName ?? this.firstName,
      gender: gender ?? this.gender,
      lastName: lastName ?? this.lastName,
      linkedInLink: linkedInLink ?? this.linkedInLink,
      nameOfEmployee: nameOfEmployee ?? this.nameOfEmployee,
      natinality: natinality ?? this.natinality,
      phone: phone ?? this.phone,
      placeBirth: placeBirth ?? this.placeBirth,
      image: image ?? this.image,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'address': address,
      'birthDate': birthDate,
      'city': city,
      'civilStatus': civilStatus,
      'country': country,
      'email': email,
      'firstName': firstName,
      'gender': gender,
      'lastName': lastName,
      'linkedInLink': linkedInLink,
      'nameOfEmployee': nameOfEmployee,
      'natinality': natinality,
      'phone': phone,
      'placeBirth': placeBirth,
      // 'image': image?.toMap(),
    };
  }

  String toJson() => json.encode(toMap());
}
