import 'package:dio/dio.dart';

class CountriesApi {
  Dio? dio;
  var options = BaseOptions(
      // connectTimeout: 20 * 1000,
      // receiveTimeout: 20 * 1000,
      );

  CountriesApi() {
    dio = Dio(options);
  }

  nationalityApiMethods() async {
    var result =
        await dio!.get('https://api.manatal.com/open/v3/nationalities/');
    return result;
  }
}
