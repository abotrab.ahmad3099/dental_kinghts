import 'package:flutter/material.dart';

class ColorApp {
  static Color blueColor = const Color.fromRGBO(127, 157, 186, 50);
  static Color imageOrDetialsColor = const Color.fromARGB(255, 123, 100, 113);
  static Color primaryColor = const Color.fromARGB(218, 112, 39, 56);
  static Color titleColorInRegisterPage = const Color(0xFFB6B3B3);
  static String primaryColorStr = "#702738";
}
