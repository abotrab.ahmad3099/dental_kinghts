// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dental_kinghts/src/data/models/course.dart';
import 'package:dental_kinghts/src/data/models/empolyement_qualifications.dart';
import 'package:dental_kinghts/src/data/models/hobbit.dart';
import 'package:dental_kinghts/src/data/models/language.dart';
import 'package:dental_kinghts/src/data/models/skill.dart';
import 'package:dental_kinghts/src/presentation/pages/education.dart';

import '/src/data/models/experiences.dart';
import '/src/data/models/personal_data.dart';

class StaticData {
  static Experiences? experiences = Experiences(
    introduction: '',
    empolyementQualifications: <EmpolyementQualifications>[],
    courses: <Course>[],
    educations: [],
    hobbits: <Hobbit>[],
    languages: <Language>[],
    skills: <Skill>[],
  );
  static PersonalData? personalData = PersonalData();
  static String? introduction;
}
