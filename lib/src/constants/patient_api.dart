import '../data/models/patient.dart';

class PatientApi {
  static List<Patient> patients = [
    Patient(name: 'momo', age: 10, address: "boob"),
    Patient(name: 'soso', age: 20, address: "momo"),
    Patient(name: 'sasa', age: 40, address: "sdad"),
    Patient(name: 'fofo', age: 30, address: "bofdsfaob"),
    Patient(name: 'toto', age: 30, address: "fsaf"),
    Patient(name: 'lolo', age: 60, address: "basfaoob"),
  ];
}
