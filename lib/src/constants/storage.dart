/*
 * @author Martin Appelmann <exlo89@gmail.com
 * @copyright 2020 Open E-commerce App
 * @see storage.dart
 */

import 'package:dental_kinghts/src/data/models/doctor_model.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class Storage {
  static final Storage _instance = Storage._internal();

  factory Storage() => _instance;

  Storage._internal();

  FlutterSecureStorage secureStorage = FlutterSecureStorage();
  String token = '';
  String name = '';
  String image = '';
  String mobile = '';
  String address = '';
  String email = '';
  String firebase_token = '';
  String userId = '35a62786-d698-45b3-8286-12754a0de6d2';
  String isMovingStarted = "false";
  String busLine = "1";
  Doctor? doctor;
}
