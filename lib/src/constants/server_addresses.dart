class ServerAddresses {
  //Server address of your project should go here
  static const serverAddress = 'http://dentalknights.com/Api/';
  static const serverAddress2 = 'http://dentalknights.com/Login/';
  static const uploadUrl = 'https://dentalknights.com/uploads/';

  static String get getSliders => serverAddress + 'slider';
}
