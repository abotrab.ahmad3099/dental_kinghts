// ignore_for_file: unnecessary_const

import 'package:flutter/material.dart';

class MyColor {
  static const MaterialColor kToDark = const MaterialColor(
    0xFFB6B3B3, // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
    const <int, Color>{
      50: const Color(0xffFFFFFF), //10%
      100: const Color(0xffFFFFFF), //20%
      200: const Color(0xffFFFFFF), //30%
      300: const Color(0xffFFFFFF), //40%
      400: const Color(0xffFFFFFF), //50%
      500: const Color(0xffFFFFFF), //60%
      600: const Color(0xffFFFFFF), //70%
      700: const Color(0xffFFFFFF), //80%
      800: const Color(0xffFFFFFF), //90%
      900: const Color(0xffFFFFFF), //100%
    },
  );
}
