import 'dart:io';
import 'dart:ui';

import 'package:dental_kinghts/src/constants/color_app.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:galleryimage/gallery_item_model.dart';
import 'package:path_provider/path_provider.dart';

import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:sizer/sizer.dart';
import 'package:photo_view/photo_view.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image/image.dart' as img;
import 'package:http/http.dart' as http;

class ThumbnailImage extends StatelessWidget {
  final String imageUrl;
  final double? thumbWidth;
  final double? thumbHieght;
  ThumbnailImage({required this.imageUrl, this.thumbHieght, this.thumbWidth});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => LargerImagePage(imageUrl: imageUrl),
          ),
        );
      },
      child: Hero(
          tag: imageUrl,
          child: CachedNetworkImage(
            imageUrl: imageUrl,
            width: thumbWidth ?? 20.w,
            height: thumbHieght ?? 40.h,
            progressIndicatorBuilder: (context, url, downloadProgress) =>
                const SpinKitDoubleBounce(
              color: Colors.orange,
              size: 50.0,
            ),
            errorWidget: (context, url, error) => const Icon(Icons.error),
          )),
    );
  }
}

class LargerImagePage extends StatelessWidget {
  final String imageUrl;
  LargerImagePage({required this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: PhotoView(
        imageProvider: CachedNetworkImageProvider(imageUrl),
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final response = await http.get(Uri.parse(imageUrl));
          final image = img.decodeImage(response.bodyBytes);
          final directory = await getApplicationDocumentsDirectory();
          final path = '/storage/emulated/0/Download/' + '/myImage.png';
          final file = File(path);
          file.writeAsBytesSync(img.encodePng(image!));
          Fluttertoast.showToast(msg: 'تم حفظ الصورة في التنزيلات');
        },
        child: Icon(Icons.save),
      ),
    );
  }
}
