import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dental_kinghts/src/common/thumbnil.dart';
import 'package:dental_kinghts/src/data/repositories/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sizer/sizer.dart';

import '../../constants/color_app.dart';
import '../../constants/patient_api.dart';
import '../../constants/server_addresses.dart';
import '../../data/models/t_image.dart';
import '../../local_data.dart';
import '../widgets/drawer_doctor.dart';
import 'package:http/http.dart' as http;

class ImageScientific extends StatefulWidget {
  const ImageScientific({Key? key}) : super(key: key);

  @override
  State<ImageScientific> createState() => _ImageScientificState();
}

class _ImageScientificState extends State<ImageScientific> {
  List images = [];
  File? image;
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  TextEditingController search = TextEditingController();
  Repository repository = Repository();

  @override
  void initState() {
    if (LocalData.doctor == null) LocalData();
    super.initState();
  }

  searchPatient(String query) {
    final suggessitons = PatientApi.patients.where((patient) {
      final name = patient.name!.toLowerCase();
      final input = query.toLowerCase();
      return name.contains(input);
    }).toList();
    setState(() {
      if (suggessitons.isEmpty || query.isEmpty) {
        images = [];
      } else {
        images = suggessitons;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: CustomScrollView(
                slivers: [
                  SliverFillRemaining(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 2.h,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: const Color(0xFF907785),
                              borderRadius: BorderRadius.circular(20)),
                          width: 90.w,
                          height: 7.h,
                          child: Center(
                            child: Text(
                              'صور علمية',
                              style:
                                  TextStyle(fontSize: 5.w, color: Colors.white),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 4.5.w),
                          child: TextField(
                            controller: search,
                            textAlign: TextAlign.right,
                            textAlignVertical: TextAlignVertical.center,
                            onChanged: searchPatient,
                            decoration: InputDecoration(
                              suffixIcon: Icon(Icons.search,
                                  color: ColorApp.titleColorInRegisterPage),
                              hintText: 'search',
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide(
                                  color: ColorApp.titleColorInRegisterPage,
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 2.h),
                        Container(
                          margin: EdgeInsets.only(right: 60.w),
                          child: SizedBox(
                              height: 5.2.h,
                              width: 40.w,
                              child: TextButton(
                                  onPressed: addPhoto,
                                  child: Text(
                                    'إضافة صورة +',
                                    style: TextStyle(
                                      color: ColorApp.primaryColor,
                                    ),
                                  ))),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 0),
                          height: 58.h,
                          width: 90.w,
                          child: Center(
                            child: Container(
                                margin: EdgeInsets.only(left: 3.w, top: 0),
                                child: FutureBuilder<List<T_Image>>(
                                    future: repository.getImages(),
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        return GridView.builder(
                                          itemCount: snapshot.data!.length,
                                          // shrinkWrap: true,
                                          physics: const ScrollPhysics(),
                                          gridDelegate:
                                              const SliverGridDelegateWithFixedCrossAxisCount(
                                                  crossAxisCount: 2,
                                                  childAspectRatio: 0.98),
                                          itemBuilder: (context, index) {
                                            return Row(
                                              children: [
                                                Container(
                                                    width: 40.w,
                                                    height: 26.h,
                                                    decoration:
                                                        const BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                        Radius.circular(13),
                                                      ),
                                                    ),
                                                    child: ThumbnailImage(imageUrl: snapshot
                                                          .data![index].image!,)
                                                    
                                                   ),
                                              ],
                                            );
                                          },
                                        );
                                      }
                                      return const Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    })),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: 4.5.h,
            child: Text(
              'التعليم',
              style: TextStyle(color: Colors.white, fontSize: 5.w),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  addPhoto() {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: const Text('من أين تريد إضافة الصورة'),
        content: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              MaterialButton(
                onPressed: addPhotofromGallary,
                color: Colors.grey[400],
                child: const Text(
                  'من استديو الصور',
                  style: TextStyle(color: Colors.black),
                ),
              ),
              SizedBox(height: 2.h),
              MaterialButton(
                onPressed: addPhotoFromCamera,
                color: Colors.grey[400],
                child: const Text(
                  'التقاط من الكاميرا',
                  style: TextStyle(color: Colors.black),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  addPhotofromGallary() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      if (image == null) return;
      File imageTemp = File(image.path);
      setState(() => this.image = imageTemp);
      TextEditingController book_name = TextEditingController();
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          actionsAlignment: MainAxisAlignment.end,
          title: const Text('تم تحميل الصورة'),
          content: Container(
            height: 15.h,
            child: Column(
              children: [
                Text(imageTemp.path.split('/').last),
                SizedBox(
                  height: 1.h,
                ),
                TextField(
                  controller: book_name,
                  textDirection: TextDirection.rtl,
                  decoration: InputDecoration(
                    hintText: 'أدخل عنوان الصورة',
                    hintTextDirection: TextDirection.rtl,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                )
              ],
            ),
          ),
          actions: [
            MaterialButton(
              onPressed: () async {
                Fluttertoast.showToast(msg: 'جاري ترفيع الصورة...');
                var url = '${ServerAddresses.serverAddress}save_image';
                var request = http.MultipartRequest('POST', Uri.parse(url));
                List<http.MultipartFile> newList = <http.MultipartFile>[];

                newList.add(await http.MultipartFile.fromPath(
                    'image', imageTemp.path));
                request.fields['image_title'] = book_name.text;
                request.files.addAll(newList);
                var res = await request.send();
                var response = await http.Response.fromStream(res);
                print(response.body);
                if (res.statusCode == 200) {
                  Fluttertoast.showToast(msg: 'تم إضافة الصورة بنجاح');
                  Navigator.pop(context);
                  Navigator.pop(context);
                  setState(() {});
                }
              },
              child: const Text('حفظ'),
            )
          ],
        ),
      );
    } on PlatformException catch (e) {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text('تحذير'),
          content: Text(e.message.toString()),
        ),
      );
    }
  }

  addPhotoFromCamera() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.camera);
      if (image == null) return;
      final imageTemp = File(image.path);
      setState(() => this.image = imageTemp);
      TextEditingController book_name = TextEditingController();
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          actionsAlignment: MainAxisAlignment.end,
          title: const Text('تم تحميل الصورة'),
          content: Container(
            height: 30.h,
            child: Column(
              children: [
                ClipRRect(
                    child: Image.memory(
                  imageTemp.readAsBytesSync(),
                  fit: BoxFit.cover,
                  width: 38.w,
                  height: 35.w,
                )),
                Text(imageTemp.path.split('/').last),
                SizedBox(
                  height: 1.h,
                ),
                TextField(
                  controller: book_name,
                  textDirection: TextDirection.rtl,
                  decoration: InputDecoration(
                    hintText: 'أدخل عنوان الصورة',
                    hintTextDirection: TextDirection.rtl,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                )
              ],
            ),
          ),
          actions: [
            MaterialButton(
              onPressed: () async {
                Fluttertoast.showToast(msg: 'جاري ترفيع الصورة...');
                var url = '${ServerAddresses.serverAddress}save_image';
                var request = http.MultipartRequest('POST', Uri.parse(url));
                List<http.MultipartFile> newList = <http.MultipartFile>[];

                newList.add(await http.MultipartFile.fromPath(
                    'image', imageTemp.path));
                request.fields['image_title'] = book_name.text;
                request.files.addAll(newList);
                var res = await request.send();
                var response = await http.Response.fromStream(res);
                print(response.body);
                if (res.statusCode == 200) {
                  Fluttertoast.showToast(msg: 'تم إضافة الصورة بنجاح');
                  Navigator.pop(context);
                  Navigator.pop(context);
                  setState(() {});
                }
              },
              child: const Text('حفظ'),
            )
          ],
        ),
      );
    } on PlatformException catch (e) {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text('تحذير'),
          content: Text(e.message.toString()),
        ),
      );
    }
  }
}
