import 'dart:convert';

import 'package:dental_kinghts/src/common/gallerysitting.dart';
import 'package:dental_kinghts/src/constants/server_addresses.dart';
import 'package:dental_kinghts/src/constants/storage.dart';
import 'package:dental_kinghts/src/controllers/homepage_controller.dart';
import 'package:dental_kinghts/src/data/models/post.dart';
import 'package:dental_kinghts/src/data/models/spec.dart';
import 'package:dental_kinghts/src/data/repositories/repository.dart';
import 'package:dental_kinghts/src/logic/special_controller.dart';
import 'package:dental_kinghts/src/presentation/pages/comments.dart';
import 'package:dental_kinghts/src/presentation/pages/profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:like_button/like_button.dart';
import 'package:readmore/readmore.dart';
import 'package:sizer/sizer.dart';

import '/src/constants/color_app.dart';
import '../../data/models/doctor_model.dart';
import '../widgets/drawer_doctor.dart';
import 'create_post.dart';

// ignore: must_be_immutable
class HomePageDoctor extends StatefulWidget {
  const HomePageDoctor({Key? key}) : super(key: key);

  @override
  State<HomePageDoctor> createState() => _HomePageDoctorState();
}

class _HomePageDoctorState extends State<HomePageDoctor> {
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  List<String> listTitleItem = [];
  List<Specialization> specList = [];
  List<Post> posts = [];
  List<Post> suggestion = [];
  int slectedIndex = 0;
  late ScrollController scrollController;

  Future<List> getSpec() async {
    if (listTitleItem.isEmpty) {
      final data = await http
          .get(Uri.parse(ServerAddresses.serverAddress + "specilzations"));
      var responseBody = data.body;
      print(responseBody);
      final parsed = json.decode(data.body).cast<Map<String, dynamic>>();

      specList = parsed
          .map<Specialization>((json) => Specialization.fromJson(json))
          .toList();

      for (int i = 0; i < specList.length; i++) {
        listTitleItem.add(specList[i].specName ?? 'اختصاص');
      }
    }

    return specList;
  }

  final Repository repo = Repository();
  late String specId;

  @override
  void initState() {
    // TODO: implement initState
    specId = "1";
    scrollController =
        ScrollController(initialScrollOffset: 0, keepScrollOffset: true);
    getSpec();
    super.initState();
  }

  generateWidget(list) {
    List<Widget> widgets = [];
    for (var element in list) {
      widgets.add(Container(
        width: 5.w,
        height: 3.h,
        decoration: BoxDecoration(
            color: ColorApp.primaryColor,
            borderRadius: BorderRadius.circular(16)),
        child: Center(child: Text(element.specName!)),
      ));
    }
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Container(
        margin: const EdgeInsets.only(bottom: 10),
        child: FloatingActionButton(
            elevation: 0.0,
            backgroundColor: const Color.fromARGB(248, 190, 75, 75),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (_) {
                return CreatePost(
                  spec: specList,
                );
              }));
            },
            child: const Icon(Icons.create, color: Colors.white)),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 6.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: SizedBox(
                          width: 80.w,
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: GetBuilder<SpecController>(
                              init: SpecController(),
                              builder: (_) => SingleChildScrollView(
                                controller: scrollController,
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: _.getWidget(),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        color: Colors.black,
                        child: VerticalDivider(
                          indent: 0.h,
                          endIndent: 4.5.h,
                          width: 1,
                        ),
                      ),
                      SizedBox(width: 1.w),
                      GestureDetector(
                          onTap: () {
                            showDialog(
                              barrierDismissible: false,
                              anchorPoint: const Offset(10, 10),
                              context: context,
                              builder: (_) => AlertDialog(
                                title: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    IconButton(
                                      icon: const Icon(Icons.close),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                    ),
                                    Text(
                                      'فلترة حسب الاختصاص',
                                      style: TextStyle(
                                          color: ColorApp.primaryColor),
                                    ),
                                  ],
                                ),
                                titlePadding: EdgeInsets.only(
                                  bottom: 10,
                                  left: 3.w,
                                  right: 3.w,
                                ),
                                // contentPadding: const EdgeInsets.all(10.0),
                                content: SizedBox(
                                  width: 90.w,
                                  height: 20.h,
                                  child: GridView.builder(
                                      gridDelegate:
                                          const SliverGridDelegateWithMaxCrossAxisExtent(
                                        maxCrossAxisExtent: 160,
                                        childAspectRatio: 3,
                                        crossAxisSpacing: 10,
                                        mainAxisSpacing: 10,
                                      ),
                                      itemCount: specList.length,
                                      itemBuilder: (context, index) {
                                        return Expanded(
                                          child: GestureDetector(
                                            onTap: () {
                                              Get.put(SpecController())
                                                  .addNewSelectedSpec(
                                                      specList[index].specName);
                                              Navigator.of(context).pop();
                                              Specialization spec = specList
                                                  .firstWhere((element) =>
                                                      element.specName ==
                                                      listTitleItem[index]);
                                              specId = spec.id ?? '1';
                                              Get.find<HomePageController>()
                                                  .changeSpecId(specId);
                                              setState(() {
                                                slectedIndex = index;
                                              });
                                            },
                                            child: Container(
                                              // width: 5.w,
                                              height: 1.h,
                                              decoration: BoxDecoration(
                                                  color: ColorApp.primaryColor,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          12)),
                                              child: Center(
                                                  child: Text(
                                                specList[index].specName!,
                                                style: const TextStyle(
                                                    color: Colors.white),
                                              )),
                                            ),
                                          ),
                                        );
                                      }),
                                ),
                              ),
                            );
                          },
                          child: Container(
                            width: 10.w,
                            height: 5.h,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              color: ColorApp.primaryColor,
                            ),
                            child: Image.asset('assets/images/filter.png',
                                color: Colors.white),
                          )),
                      SizedBox(width: 5.w),
                    ],
                  ),
                  Expanded(
                      flex: 5,
                      child: SizedBox(
                          width: 92.w,
                          child: GetBuilder<HomePageController>(
                              init: HomePageController(),
                              builder: (_) {
                                if (_.status.isLoading) {
                                  return const SpinKitDoubleBounce(
                                    color: Colors.brown,
                                    size: 50.0,
                                  );
                                }
                                posts.addAll(_.posts);
                                suggestion.addAll(_.posts);
                                return ListView.builder(
                                  controller: _.scroll,
                                  itemCount: _.posts.length,
                                  itemBuilder: (context, index) {
                                    return GestureDetector(
                                      onTap: () {},
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          PostHomePage(post: _.posts[index]),
                                          SizedBox(
                                            height: 1.h,
                                          )
                                        ],
                                      ),
                                    );
                                  },
                                );
                              }))),
                  GetBuilder<HomePageController>(
                      init: HomePageController(),
                      builder: (_) {
                        if (_.status.isLoadingMore) {
                          return SpinKitCubeGrid(
                            color: ColorApp.primaryColor,
                            size: 50.0,
                          );
                        }
                        return Container();
                      })
                ],
              ),
            ),
          ),
          Positioned(
            top: 5.5.h,
            left: 35.w,
            child: Text(
              'الصفحة الرئيسية',
              style: TextStyle(
                  color: ColorApp.titleColorInRegisterPage, fontSize: 5.w),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 87.w,
            child: IconButton(
              onPressed: () {
                showSearch(
                    context: context,
                    delegate: MySearchDelegate(posts, suggestion));
              },
              icon: const Icon(
                Icons.search,
                color: Colors.white,
              ),
            ),
          ),
          Positioned(
            top: 4.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  goToProfile(context, Doctor doctor) {
    Navigator.push(context, MaterialPageRoute(builder: (_) {
      if (doctor.name == Storage().doctor!.name) {
        return Profile(doctor: doctor, isUserDoctor: true);
      } else {
        return Profile(
          doctor: doctor,
          isUserDoctor: false,
        );
      }
    }));
  }

  sepratedFunctionListView(context, index) {
    return SizedBox(
      width: 2.w,
    );
  }

  itemsBuilderListView(context, index) {
    return InkWell(
      onTap: () {
        Specialization spec = specList
            .firstWhere((element) => element.specName == listTitleItem[index]);
        specId = spec.id ?? '1';
        Get.find<HomePageController>().changeSpecId(specId);
        setState(() {
          slectedIndex = index;
        });
      },
      child: Align(
        alignment: Alignment.topCenter,
        child: Container(
          decoration: BoxDecoration(
            color: slectedIndex == index
                ? Colors.green
                : const Color.fromARGB(174, 170, 170, 170),
            borderRadius: BorderRadius.circular(4),
          ),
          width: 20.w,
          height: 5.h,
          child: Center(child: Text(listTitleItem[index])),
        ),
      ),
    );
  }
}

class MySearchDelegate extends SearchDelegate<Post> {
  List<Post> posts = [];
  List<Post> suggestions = [];

  MySearchDelegate(this.posts, this.suggestions);

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(onPressed: () {}, icon: const Icon(Icons.clear));
  }

  @override
  Widget buildResults(BuildContext context) {
    final list = posts
        .where((post) =>
            post.postDescription!.toLowerCase().contains(query.toLowerCase()))
        .toList();
    print('search delegertds fdsfmsdljfsdklfjs');
    return ListView.separated(
      itemCount: list.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.only(left: 15),
          child: PostHomePage(
            post: list[index],
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 2.h,
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final list = suggestions
        .where((post) =>
            post.postDescription!.toLowerCase().contains(query.toLowerCase()))
        .toList();
    print('suggestion');
    print(query);
    print(suggestions.length);
    return ListView.separated(
      itemCount: list.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.only(left: 15),
          child: PostHomePage(
            post: list[index],
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 2.h,
        );
      },
    );
  }
}

// ignore: must_be_immutable

class PostHomePage extends StatelessWidget {
  PostHomePage({
    Key? key,
    required this.post,
  }) : super(key: key);
  Post post;

  Future<bool> onLikeButtonTapped(bool isLiked) async {
    final data = await http.get(
        Uri.parse(
            "${ServerAddresses.serverAddress}likePost?post_id=${post.id}"),
        headers: {
          'Content-Type': 'application/json',
          'token': Storage().token,
        });
    print(data.body);
    if (data.statusCode == 200) {
      return true;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    List<String> limages = <String>[];
    limages += post.images!.map((e) => e.image!).toList();
    return Row(
      children: [
        SizedBox(
          width: 1.w,
        ),
        Container(
          width: 89.w,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: const Color(0xFFe6e6e6),
          ),
          child: Stack(
            alignment: Alignment.topRight,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 1.h, right: 2.w),
                child: CircleAvatar(
                  backgroundColor: ColorApp.primaryColor,
                  radius: 17,
                  child: Image.asset(
                    'assets/images/doctor.png',
                  ),
                ),
              ),

              Padding(
                padding: EdgeInsets.only(top: 1.h, right: 12.w),
                child: Column(
                  children: [
                    Text(post.customer!),
                    Text(
                      post.specialization!,
                      style: TextStyle(fontSize: 3.w),
                    ),
                  ],
                ),
              ),

              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 8.h, right: 3.5.w, left: 1.w),
                    child: ReadMoreText(
                      post.postDescription!,
                      trimLines: 3,
                      colorClickableText: Colors.pink,
                      trimMode: TrimMode.Line,
                      trimCollapsedText: 'عرض المزيد...',
                      trimExpandedText: 'عرض أقل...',
                      textAlign: TextAlign.right,
                      moreStyle: const TextStyle(
                          fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 3.h,
                    ),
                    child: Container(
                      child: GalleryImage2(imageUrls: limages),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 0.h,
                    ),
                    child: const Divider(),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 0.h,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            IconButton(
                              onPressed: () {
                                Navigator.of(context)
                                    .push(MaterialPageRoute(builder: (_) {
                                  return CommentsPage(
                                    postId: post.id,
                                  );
                                }));
                              },
                              icon: const Icon(Icons.message_outlined),
                            ),
                            Text(
                              post.comments! != '0' ? post.comments! : '',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 3.w,
                        ),
                        LikeButton(
                          size: 24,
                          circleColor: const CircleColor(
                              start: Color(0xff00ddff), end: Color(0xff0099cc)),
                          bubblesColor: const BubblesColor(
                            dotPrimaryColor: Color(0xff33b5e5),
                            dotSecondaryColor: Color(0xff0099cc),
                          ),
                          likeBuilder: (bool isLiked) {
                            if (!post.isLiked!) {
                              post.isLiked = isLiked;
                            }
                            return Icon(
                              post.isLiked!
                                  ? Icons.favorite
                                  : Icons.favorite_border,
                              color: post.isLiked! ? Colors.red : Colors.grey,
                              size: 24,
                            );
                          },
                          likeCount: int.parse(post.likes!),
                          countBuilder:
                              (int? count, bool isLiked, String text) {
                            var color =
                                isLiked ? Colors.redAccent : Colors.grey;
                            Widget? result;
                            if (count == 0) {
                              result = Text(
                                "love",
                                style: TextStyle(color: color),
                              );
                            } else {
                              result = Text(
                                text,
                                style: TextStyle(color: color),
                              );
                            }
                            return result;
                          },
                          onTap: onLikeButtonTapped,
                          // onTap: (bool isLiked) async {
                          //   if (isLiked) {
                          //     await addLike();
                          //   }
                          //   return null;
                          // },
                        ),
                        SizedBox(
                          width: 3.w,
                        ),
                      ],
                    ),
                  ),
                ],
              ),

              ///   here image about doctor if there is

              ///   like and comment on doctor
            ],
          ),
        ),
        SizedBox(
          width: 1.w,
        )
      ],
    );
  }

  onPressedLikeButton() {}

  onPressedFavoriteButton() {}
}
