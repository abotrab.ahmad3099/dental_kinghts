import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:chewie/chewie.dart';
import 'package:dental_kinghts/src/data/models/t_video.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sizer/sizer.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import '../../constants/color_app.dart';
import '../../constants/patient_api.dart';
import '../../constants/server_addresses.dart';
import '../../data/repositories/repository.dart';
import '../../local_data.dart';
import '../widgets/drawer_doctor.dart';
import 'package:http/http.dart' as http;

class VideoScientfic extends StatefulWidget {
  const VideoScientfic({Key? key}) : super(key: key);

  @override
  State<VideoScientfic> createState() => _VideoScientficState();
}

class _VideoScientficState extends State<VideoScientfic> {
  String dataSource = "https://www.youtube.com/watch?v=rUWxSEwctFU";
  List images = [];
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  TextEditingController search = TextEditingController();
  List<VideoPlayerController> videoPlayerController = [];
  List<ChewieController> chewieController = [];
  List<Chewie> playerWidget = [];
  Repository repository = Repository();
  List<String> thumbnils = [];
  @override
  void initState() {
    if (LocalData.doctor == null) LocalData();

    super.initState();
  }

  searchPatient(String query) {
    final suggessitons = PatientApi.patients.where((patient) {
      final name = patient.name!.toLowerCase();
      final input = query.toLowerCase();
      return name.contains(input);
    }).toList();
    setState(() {
      if (suggessitons.isEmpty || query.isEmpty) {
        images = [];
      } else {
        images = suggessitons;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: CustomScrollView(
                slivers: [
                  SliverFillRemaining(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 2.h,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: const Color(0xFF907785),
                              borderRadius: BorderRadius.circular(20)),
                          width: 90.w,
                          height: 7.h,
                          child: Center(
                            child: Text(
                              'فيديوهات علمية',
                              style:
                                  TextStyle(fontSize: 5.w, color: Colors.white),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 4.5.w),
                          child: TextField(
                            controller: search,
                            textAlign: TextAlign.right,
                            textAlignVertical: TextAlignVertical.center,
                            onChanged: searchPatient,
                            decoration: InputDecoration(
                              suffixIcon: Icon(Icons.search,
                                  color: ColorApp.titleColorInRegisterPage),
                              hintText: 'search',
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide(
                                  color: ColorApp.titleColorInRegisterPage,
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 2.h),
                        Padding(
                          padding: EdgeInsets.only(right: 65.w),
                          child: SizedBox(
                            height: 5.h,
                            child: TextButton(
                              onPressed: addNewVideo,
                              child: Text(
                                'إضافة فيديو +',
                                style: TextStyle(color: ColorApp.primaryColor),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                            height: 60.h,
                            // margin: EdgeInsets.only(left: 2.w, top: 0),
                            width: 90.w,
                            child: FutureBuilder<List<T_Video>>(
                                future: repository.getVideos(),
                                builder: ((context, snapshot) {
                                  if (snapshot.hasData) {
                                    if (thumbnils.isEmpty) {
                                      thumbnils = snapshot.data!
                                          .map((e) => '')
                                          .toList();
                                    }

                                    return displayAllVideos(
                                      snapshot.data!.length,
                                      snapshot.data!,
                                    );
                                  }
                                  return const Center(
                                    child: CircularProgressIndicator(),
                                  );
                                }))),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: 4.5.h,
            child: Text(
              'التعليم',
              style: TextStyle(color: Colors.white, fontSize: 5.w),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  bool isUploading = false;
  addNewVideo() async {
    final result = await FilePicker.platform
        .pickFiles(allowMultiple: false, type: FileType.video);
    if (result == null) return;
    final appStore = await getApplicationDocumentsDirectory();
    final file = result.files.first;
    final newFile = File('${appStore.path}/${file.path}');
    TextEditingController book_name = TextEditingController();
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        actionsAlignment: MainAxisAlignment.end,
        title: const Text('تم تحميل الفيديو'),
        content: Container(
          height: 15.h,
          child: Column(
            children: [
              Text(file.name),
              SizedBox(
                height: 1.h,
              ),
              TextField(
                controller: book_name,
                textDirection: TextDirection.rtl,
                decoration: InputDecoration(
                  hintText: 'أدخل عنوان الفيديو',
                  hintTextDirection: TextDirection.rtl,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              )
            ],
          ),
        ),
        actions: [
          MaterialButton(
            onPressed: () async {
              Fluttertoast.showToast(msg: 'جاري ترفيع الفيديو...');
              if (!isUploading) {
                isUploading = true;
                var url = ServerAddresses.serverAddress + 'save_video';
                var request = http.MultipartRequest('POST', Uri.parse(url));
                List<http.MultipartFile> newList = <http.MultipartFile>[];

                newList.add(
                    await http.MultipartFile.fromPath('video', file.path!));
                request.fields['video_title'] = book_name.text;
                request.files.addAll(newList);
                var res = await request.send();
                var response = await http.Response.fromStream(res);
                print(response.body);
                isUploading = false;
                if (res.statusCode == 200) {
                  Fluttertoast.showToast(msg: 'تم إضافة الفيديو بنجاح');
                  Navigator.pop(context);
                  setState(() {});
                }
              }
            },
            child: const Text('حفظ'),
          )
        ],
      ),
    );
  }

  Future<String> get_thumbnil(String url, int index) async {
    if (thumbnils[index] == "") {
      final fileName = await VideoThumbnail.thumbnailFile(
        video: url,
        thumbnailPath: (await getTemporaryDirectory()).path,
        imageFormat: ImageFormat.JPEG,
        quality: 100,
      );
      thumbnils[index] = fileName ?? ' ';
    }

    return thumbnils[index];
  }

  ListView displayAllVideos(length, List<T_Video> listVideos) {
    if (videoPlayerController.isEmpty) {
      videoPlayerController = listVideos
          .map<VideoPlayerController>(
              (e) => VideoPlayerController.network(e.video!))
          .toList();
      chewieController = videoPlayerController
          .map((e) => ChewieController(
              videoPlayerController: e,
              autoPlay: false,
              looping: false,
              allowFullScreen: true,
              allowMuting: true,
              showControls: true,
              autoInitialize: true,
              showControlsOnInitialize: true))
          .toList();
      playerWidget = chewieController
          .map((e) => Chewie(
                controller: e,
              ))
          .toList();
    }

    return ListView.separated(
      itemCount: length,
      separatorBuilder: ((context, index) => SizedBox(
            height: 3.h,
          )),
      itemBuilder: (context, index) {
        return FutureBuilder<String?>(
          builder: ((context, snapshot) {
            if (snapshot.hasData) {
              final file = File(snapshot.data ?? '');
              var bytes = file.readAsBytesSync();
              final _image = Image.memory(
                bytes,
                fit: BoxFit.cover,
                width: 90.w,
              );
              return Container(
                width: 80.w,
                height: 30.h,
                decoration: const BoxDecoration(
                  color: Color(0xFFe3e3e3),
                  borderRadius: BorderRadius.all(
                    Radius.circular(13),
                  ),
                ),
                child: Column(
                  children: [
                    InkWell(
                      onTap: () {
                        var urlVideo = listVideos[index].video;

                        AspectRatio(
                            aspectRatio:
                                videoPlayerController[index].value.aspectRatio,
                            child: VideoPlayer(videoPlayerController[index]));
                      },
                      child: Container(
                          height: 20.h,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(13),
                              topLeft: Radius.circular(13),
                            ),
                          ),
                          child: playerWidget[index]
                          // CachedNetworkImage(
                          //     imageUrl: snapshot.data ?? '')
                          ),
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    Container(
                      height: 7.h,
                      decoration: const BoxDecoration(
                        color: Color(0xFFf5f5f5),
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(13),
                          bottomRight: Radius.circular(13),
                        ),
                      ),
                      child: Center(
                        child: Text(
                          listVideos[index].videoTitle!,
                          style: TextStyle(fontSize: 4.w),
                        ),
                      ),
                    )
                  ],
                ),
              );
            } else {
              return Container(
                width: 80.w,
                height: 30.h,
                decoration: const BoxDecoration(
                  color: Color(0xFFe3e3e3),
                  borderRadius: BorderRadius.all(
                    Radius.circular(13),
                  ),
                ),
                child: Column(
                  children: [
                    InkWell(
                      onTap: () {
                        var urlVideo = listVideos[index].video;

                        AspectRatio(
                            aspectRatio:
                                videoPlayerController[index].value.aspectRatio,
                            child: VideoPlayer(videoPlayerController[index]));
                      },
                      child: Container(
                          height: 20.h,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(13),
                              topLeft: Radius.circular(13),
                            ),
                          ),
                          child: SpinKitDoubleBounce(
                            color: Colors.orange,
                            size: 50.0,
                          )),
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    Container(
                      height: 7.h,
                      decoration: const BoxDecoration(
                        color: Color(0xFFf5f5f5),
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(13),
                          bottomRight: Radius.circular(13),
                        ),
                      ),
                      child: Center(
                        child: Text(
                          listVideos[index].videoTitle!,
                          style: TextStyle(fontSize: 4.w),
                        ),
                      ),
                    )
                  ],
                ),
              );
            }
          }),
          future: get_thumbnil(listVideos[index].video ?? '', index),
        );
      },
    );
  }
}
