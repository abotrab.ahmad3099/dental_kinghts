import 'dart:convert';
import 'dart:io';

import 'package:dental_kinghts/src/common/thumbnil.dart';
import 'package:dental_kinghts/src/data/models/doctor_model.dart';
import 'package:dental_kinghts/src/data/models/patient_model.dart';
import 'package:dental_kinghts/src/data/repositories/repository.dart';
import 'package:dental_kinghts/src/logic/my_dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:sizer/sizer.dart';

import '../../constants/color_app.dart';
import '../../constants/server_addresses.dart';
import '../../constants/storage.dart';
import '../widgets/drawer_doctor.dart';

class Albums extends StatefulWidget {
  const Albums({Key? key}) : super(key: key);

  @override
  State<Albums> createState() => _AlbumsState();
}

class _AlbumsState extends State<Albums> {
  List<Images> patients = [];
  List<Images> repo_patients = [];
  List<PatientModel> patientsList = [];
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  TextEditingController search = TextEditingController();
  File? image;
  bool isLoading = false;

  @override
  initState() {
    super.initState();
    loadPatientsList();
  }

  void loadPatientsList() async {
    patientsList.addAll(await repo.getPatients());
  }

  searchPatient(String query) {
    final suggessitons = repo_patients.where((patient) {
      final name = patient.imageTitle!.toLowerCase();
      final input = query.toLowerCase();
      return name.contains(input) ||
          (patient.PatientNumber != null &&
              patient.PatientNumber!.contains(input));
    }).toList();
    setState(() {
      if (suggessitons.isEmpty || query.isEmpty) {
        patients = [];
      } else {
        patients = suggessitons;
      }
    });
  }

  Row buttonsAdded() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          width: 7.w,
        ),
        Container(
          decoration: BoxDecoration(
            color: const Color(0xFF907785),
            borderRadius: BorderRadius.circular(12),
          ),
          height: 7.h,
          width: 42.w,
          child: TextButton(
            onPressed: addFilePatient,
            child: const Center(
              child:
                  Text('إضافة ملف مريض', style: TextStyle(color: Colors.white)),
            ),
          ),
        ),
        SizedBox(
          width: 2.w,
        ),
        Container(
          decoration: BoxDecoration(
            color: const Color(0xFF907785),
            borderRadius: BorderRadius.circular(12),
          ),
          height: 7.h,
          width: 42.w,
          child: TextButton(
            onPressed: addPhoto,
            child: const Center(
              child: Text('إضافة صورة', style: TextStyle(color: Colors.white)),
            ),
          ),
        ),
        SizedBox(
          width: 7.w,
        ),
      ],
    );
  }

  addFilePatient() {
    TextEditingController name = TextEditingController();
    TextEditingController id = TextEditingController();
    TextEditingController note = TextEditingController();
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        actionsAlignment: MainAxisAlignment.start,
        title: const Text('إضافة ملف مريض'),
        content: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 3.w),
                child: TextFormField(
                  controller: name,
                  textAlign: TextAlign.end,
                  textDirection: TextDirection.rtl,
                  decoration: const InputDecoration(
                    hintText: 'اسم المريض',
                  ),
                ),
              ),
              SizedBox(height: 4.h),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 3.w),
                child: TextFormField(
                  controller: id,
                  textAlign: TextAlign.end,
                  textDirection: TextDirection.rtl,
                  decoration: const InputDecoration(
                    hintText: 'رقم الملف',
                  ),
                ),
              ),
              SizedBox(height: 4.h),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 3.w),
                child: TextFormField(
                  controller: note,
                  textAlign: TextAlign.end,
                  textDirection: TextDirection.rtl,
                  decoration: const InputDecoration(
                    hintText: ' ملاحظات',
                  ),
                ),
              ),
              SizedBox(height: 2.h),
            ],
          ),
        ),
        actions: [
          isLoading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : MaterialButton(
                  onPressed: () async {
                    setState(() {
                      isLoading = true;
                    });
                    Map<String, String> headers = {};
                    var url = '${ServerAddresses.serverAddress}patient';
                    var request = http.MultipartRequest('POST', Uri.parse(url));
                    request.headers['token'] = Storage().token;
                    request.fields['number'] = id.text.trim();
                    request.fields['name'] = name.text.trim();
                    request.fields['notes'] = note.text.trim();
                    var res = await request.send();
                    var response = await http.Response.fromStream(res);
                    if (res.statusCode == 200) {
                      patientsList.add(
                          PatientModel.fromJson(jsonDecode(response.body)));
                    }
                    debugPrint(response.body);

                    setState(() {
                      isLoading = false;
                      Navigator.of(context).pop();
                    });
                  },
                  child: const Text('إضافة'),
                ),
        ],
      ),
    );
  }

  addPhoto() {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: const Text('إضافة صورة لملف المريض'),
        content: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              MaterialButton(
                onPressed: addPhotofromGallary,
                color: Colors.grey[400],
                child: const Text(
                  'من استديو الصور',
                  style: TextStyle(color: Colors.black),
                ),
              ),
              SizedBox(height: 2.h),
              MaterialButton(
                onPressed: addPhotoFromCamera,
                color: Colors.grey[400],
                child: const Text(
                  'التقاط من الكاميرا',
                  style: TextStyle(color: Colors.black),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String? selectedPatient;
  bool saveRequested = false;

  addPhotofromGallary() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      if (image == null) return;
      File imageTemp = File(image.path);
      setState(() => this.image = imageTemp);
      TextEditingController book_name = TextEditingController();
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          actionsAlignment: MainAxisAlignment.end,
          title: const Text('تم تحميل الصورة'),
          content: Container(
            height: 15.h,
            child: Column(
              children: [
                MyDropdownSearch(
                    items: patientsList
                        .map((patient) => "${patient.name}/-/${patient.number}")
                        .toList(),
                    onChanged: (newValue) {
                      setState(() {
                        selectedPatient = patientsList
                            .firstWhere(
                                (element) =>
                                    element.number! == newValue.split('/-/')[1],
                                orElse: null)
                            .id;
                      });
                    }),
                SizedBox(
                  height: 20,
                ),
                Text(imageTemp.path.split('/').last),
              ],
            ),
          ),
          actions: [
            MaterialButton(
              onPressed: () async {
                Fluttertoast.showToast(msg: 'جاري ترفيع الصورة...');
                if (!saveRequested) {
                  saveRequested = true;
                  var url = '${ServerAddresses.serverAddress}patientImage';
                  var request = http.MultipartRequest('POST', Uri.parse(url));
                  List<http.MultipartFile> newList = <http.MultipartFile>[];
                  newList.add(await http.MultipartFile.fromPath(
                      'image', imageTemp.path));
                  request.fields['patient_id'] = selectedPatient ?? "-1";
                  request.files.addAll(newList);
                  var res = await request.send();
                  var response = await http.Response.fromStream(res);
                  print(response.body);
                  if (res.statusCode == 200) {
                    Fluttertoast.showToast(msg: 'تم إضافة الصورة بنجاح');
                    Navigator.pop(context);
                    Navigator.pop(context);
                    setState(() {});
                  }
                }
              },
              child: const Text('حفظ'),
            )
          ],
        ),
      );
    } on PlatformException catch (e) {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text('تحذير'),
          content: Text(e.message.toString()),
        ),
      );
    }
  }

  addPhotoFromCamera() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.camera);
      if (image == null) return;
      final imageTemp = File(image.path);
      setState(() => this.image = imageTemp);
      TextEditingController book_name = TextEditingController();
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          actionsAlignment: MainAxisAlignment.end,
          title: const Text('تم تحميل الصورة'),
          content: Container(
            height: 30.h,
            child: Column(
              children: [
                ClipRRect(
                    child: Image.memory(
                  imageTemp.readAsBytesSync(),
                  fit: BoxFit.cover,
                  width: 38.w,
                  height: 35.w,
                )),
                MyDropdownSearch(
                    items: patientsList
                        .map((patient) => "${patient.name} - ${patient.number}")
                        .toList(),
                    onChanged: (newValue) {
                      setState(() {
                        selectedPatient = patientsList
                            .firstWhere(
                                (element) =>
                                    element.number! == newValue.split('/-/')[1],
                                orElse: null)
                            .id;
                        int x;
                      });
                    }),
                SizedBox(
                  height: 20,
                ),
                Text(imageTemp.path.split('/').last),
              ],
            ),
          ),
          actions: [
            MaterialButton(
              onPressed: () async {
                Fluttertoast.showToast(msg: 'جاري ترفيع الصورة...');
                var url = '${ServerAddresses.serverAddress}patientImage';
                var request = http.MultipartRequest('POST', Uri.parse(url));
                List<http.MultipartFile> newList = <http.MultipartFile>[];

                newList.add(
                    await http.MultipartFile.fromPath('image', imageTemp.path));
                request.fields['patient_id'] = selectedPatient ?? "-1";
                request.files.addAll(newList);
                var res = await request.send();
                var response = await http.Response.fromStream(res);
                print(response.body);
                if (res.statusCode == 200) {
                  Fluttertoast.showToast(msg: 'تم إضافة الصورة بنجاح');
                  Navigator.pop(context);
                  Navigator.pop(context);
                  setState(() {});
                }
              },
              child: const Text('حفظ'),
            )
          ],
        ),
      );
    } on PlatformException catch (e) {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text('تحذير'),
          content: Text(e.message.toString()),
        ),
      );
    }
  }

  Repository repo = Repository();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 3.h,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 5.w),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: Colors.grey[300]),
                    height: 18.h,
                    width: 90.w,
                    child: const Align(
                      alignment: Alignment.center,
                      child: FittedBox(
                        child: Text(
                          'هذه الخدمة مخصصة لفرسان الأسنان\nيمكنك الاحتفاظ بصور وفيديوهات خاصة بطب الأسنان\nصاحب الحساب فقط يمكنه مشاهدتها كما يمكنك\nمشاركتها مع الزملاءأو الرجوع إليها في أي وقت عند الحاجة\nللاطلاع عليها',
                          maxLines: 6,
                          textAlign: TextAlign.justify,
                          textDirection: TextDirection.rtl,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  buttonsAdded(),
                  SizedBox(
                    height: 2.h,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4.5.w),
                    child: TextField(
                      controller: search,
                      textAlign: TextAlign.end,
                      textDirection: TextDirection.rtl,
                      onChanged: searchPatient,
                      decoration: InputDecoration(
                        suffixIcon: Icon(Icons.search,
                            color: ColorApp.titleColorInRegisterPage),
                        hintText: 'البحث عن مريض',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color: ColorApp.titleColorInRegisterPage,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: FutureBuilder<List<Images>>(
                        future: repo.my_images(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData &&
                              snapshot.data!.isNotEmpty &&
                              patients.isEmpty) {
                            patients = snapshot.data!;
                            repo_patients = snapshot.data!;
                          }
                          if (snapshot.hasData && snapshot.data!.length > 0) {
                            return ListView.builder(
                              itemCount: patients.length,
                              itemBuilder: (context, index) {
                                return ListTile(
                                  leading: ThumbnailImage(
                                    imageUrl: patients[index].image ?? '',
                                  ),
                                  title: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text(patients[index].imageTitle ?? ''),
                                      Text(patients[index].PatientNumber ?? ''),
                                      Text(patients[index].PatientNotes ?? ''),
                                    ],
                                  ),
                                );
                              },
                            );
                          } else if (snapshot.hasData &&
                              snapshot.data!.length == 0) {
                            return const Center(child: Text('no data found'));
                          }
                          return Center(child: CircularProgressIndicator());
                        }),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: 5.h,
            child: Text(
              'ألبوماتي',
              style: TextStyle(color: Colors.white, fontSize: 5.w),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 88.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
