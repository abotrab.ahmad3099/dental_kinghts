import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import '../../local_data.dart';
import '../widgets/drawer_doctor.dart';
import 'cvs/create_cvs.dart';
import 'cvs/list_of_cvs.dart';

class Jobs extends StatefulWidget {
  const Jobs({Key? key}) : super(key: key);

  @override
  State<Jobs> createState() => _JobsState();
}

class _JobsState extends State<Jobs> {
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();

  Container buttonScientfic(text, function) {
    return Container(
      width: 80.w,
      height: 9.h,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: const Color(0xFF907785),
      ),
      child: MaterialButton(
        onPressed: function,
        child: Text(
          text,
          style: TextStyle(color: Colors.white, fontSize: 5.w),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 3.h,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 5.w),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: Colors.grey[300]),
                    height: 14.h,
                    width: 90.w,
                    child: const Align(
                      alignment: Alignment.center,
                      child: Text(
                        'هذه الخدمة تتيح لفرسان الأسنان البحث عن عمل أو طلب لصاحب العمل',
                        maxLines: 6,
                        textAlign: TextAlign.justify,
                        textDirection: TextDirection.rtl,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  buttonScientfic('أنشئ سيرتك الذاتية', () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => const CreateCVs()));
                  }),
                  SizedBox(
                    height: 2.h,
                  ),
                  buttonScientfic('عرض السير الذاتية', () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => const ListOfCVs()));
                  }),
                ],
              ),
            ),
          ),
          Positioned(
            top: 4.5.h,
            child: Text(
              'الوظائف',
              style: TextStyle(color: Colors.white, fontSize: 5.w),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
