import 'package:comment_box/comment/comment.dart';
import 'package:dental_kinghts/src/constants/color_app.dart';
import 'package:dental_kinghts/src/constants/server_addresses.dart';
import 'package:dental_kinghts/src/constants/storage.dart';
import 'package:dental_kinghts/src/data/models/comment.dart';
import 'package:dental_kinghts/src/data/repositories/repository.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CommentsPage extends StatefulWidget {
  final String? postId;

  const CommentsPage({Key? key, required this.postId}) : super(key: key);
  @override
  _CommentsPageState createState() => _CommentsPageState();
}

class _CommentsPageState extends State<CommentsPage> {
  final formKey = GlobalKey<FormState>();
  final TextEditingController commentController = TextEditingController();
  List filedata = [
    {
      'name': 'Chuks Okwuenu',
      'pic': 'https://picsum.photos/300/30',
      'message': 'I love to code',
      'date': '2021-01-01 12:00:00'
    },
    {
      'name': 'Biggi Man',
      'pic': 'https://www.adeleyeayodeji.com/img/IMG_20200522_121756_834_2.jpg',
      'message': 'Very cool',
      'date': '2021-01-01 12:00:00'
    },
    {
      'name': 'Tunde Martins',
      'pic': 'assets/img/userpic.jpg',
      'message': 'Very cool',
      'date': '2021-01-01 12:00:00'
    },
    {
      'name': 'Biggi Man',
      'pic': 'https://picsum.photos/300/30',
      'message': 'Very cool',
      'date': '2021-01-01 12:00:00'
    },
  ];

  Future<bool> sendComment() async {
    final data = await http
        .post(Uri.parse("${ServerAddresses.serverAddress}comment"), body: {
      "post_id": widget.postId,
      "comment": commentController.text
    }, headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'token': Storage().token,
    });
    print(data.body);

    return true;
  }

  Widget commentChild(List<Comment> data) {
    return ListView(
      children: [
        for (var i = 0; i < data.length; i++)
          Padding(
            padding: const EdgeInsets.fromLTRB(2.0, 8.0, 2.0, 0.0),
            child: ListTile(
              leading: GestureDetector(
                onTap: () async {
                  // Display the image in large form.
                  print("Comment Clicked");
                },
                child: Container(
                  height: 50.0,
                  width: 50.0,
                  decoration: new BoxDecoration(
                      color: Colors.blue,
                      borderRadius: new BorderRadius.all(Radius.circular(50))),
                  child: CircleAvatar(
                      radius: 50,
                      backgroundImage: CommentBox.commentImageParser(
                          imageURLorPath: 'https://picsum.photos/300/30')),
                ),
              ),
              title: Text(
                data[i].customer ?? '',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                data[i].comment ?? '',
                softWrap: true,
              ),
              trailing: Text(data[i].createdDate ?? '',
                  style: TextStyle(fontSize: 10)),
            ),
          )
      ],
    );
  }

  final Repository repo = Repository();
  @override
  Widget build(BuildContext context) {
    return Directionality(
        // add this
        textDirection: TextDirection.rtl, // set this property
        child: Scaffold(
          appBar: AppBar(
            title: const Text("التعليقات"),
            backgroundColor: ColorApp.primaryColor,
          ),
          body: Container(
            child: CommentBox(
              userImage: CommentBox.commentImageParser(
                  imageURLorPath: "assets/img/userpic.jpg"),
              child: FutureBuilder<List<Comment>>(
                  future: repo.getComments(widget.postId!),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return commentChild(snapshot.data!);
                    } else {
                      return const Center(child: CircularProgressIndicator());
                    }
                  }),
              labelText: 'اكتب تعليقا...',
              errorText: 'يجب كتابة تعليق',
              withBorder: false,
              sendButtonMethod: () async {
                if (formKey.currentState!.validate()) {
                  print(commentController.text);
                  await sendComment();
                  setState(() {
                    var value = {
                      'name': Storage().name,
                      'pic':
                          'https://lh3.googleusercontent.com/a-/AOh14GjRHcaendrf6gU5fPIVd8GIl1OgblrMMvGUoCBj4g=s400',
                      'message': commentController.text,
                      'date': DateTime.now().toString()
                    };
                    filedata.insert(0, value);
                  });
                  commentController.clear();
                  FocusScope.of(context).unfocus();
                } else {
                  print("Not validated");
                }
              },
              formKey: formKey,
              commentController: commentController,
              backgroundColor: Color.fromARGB(97, 122, 24, 57),
              textColor: Colors.white,
              sendWidget: Icon(Icons.send_sharp, size: 30, color: Colors.white),
            ),
          ),
        ));
  }
}
