import 'dart:async';
import 'dart:io';

import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dental_kinghts/src/constants/server_addresses.dart';
import 'package:dental_kinghts/src/data/repositories/repository.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:path_provider/path_provider.dart';
// import 'package:pdf_render/pdf_render_widgets.dart';
import 'package:sizer/sizer.dart';
import 'package:http/http.dart' as http;
import '../../constants/color_app.dart';
import '../../constants/storage.dart';
import '../../data/models/book.dart';
import '../../local_data.dart';
import '../widgets/PDFScreen.dart';
import '../widgets/drawer_doctor.dart';

class BookScientific extends StatefulWidget {
  const BookScientific({Key? key}) : super(key: key);

  @override
  State<BookScientific> createState() => _BookScientificState();
}

class _BookScientificState extends State<BookScientific> {
  List<Book> books = [];
  TextEditingController controller = TextEditingController();
  String? directoryPath;
  String? extension;
  /////Get new book
  String? fileName;

  int iconSizeStar = 6;
  List images = [];
  bool isLoading = false;
  bool multiPick = false;
  List<PlatformFile>? paths;
  FileType pickingType = FileType.custom;
  Repository repository = Repository();
  String? saveAsFileName;
  final scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  TextEditingController search = TextEditingController();
  bool userAborted = false;

  @override
  void initState() {
    if (LocalData.doctor == null) LocalData();
    controller.addListener(() => extension = controller.text);
    super.initState();
  }

  void pickFiles() async {
    _resetState();
    try {
      directoryPath = null;
      paths = (await FilePicker.platform.pickFiles(
        type: pickingType,
        allowedExtensions: ['pdf'],
        allowMultiple: multiPick,
        onFileLoading: (FilePickerStatus status) => print(status),
      ))
          ?.files;
    } on PlatformException catch (e) {
      _logException('Unsupported operation$e');
    } catch (e) {
      _logException(e.toString());
    }
    if (!mounted) return;
    setState(() {
      isLoading = false;
      fileName = paths != null ? paths!.map((e) => e.name).toString() : '...';
      userAborted = paths == null;
    });
  }

  void clearCachedFiles() async {
    _resetState();
    try {
      bool? result = await FilePicker.platform.clearTemporaryFiles();
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: result! ? Colors.green : Colors.red,
          content: Text((result
              ? 'Temporary files removed with success.'
              : 'Failed to clean temporary files')),
        ),
      );
    } on PlatformException catch (e) {
      _logException('Unsupported operation$e');
    } catch (e) {
      _logException(e.toString());
    } finally {
      setState(() => isLoading = false);
    }
  }

  void selectFolder() async {
    _resetState();
    try {
      String? path = await FilePicker.platform.getDirectoryPath();
      setState(() {
        directoryPath = path;
        userAborted = path == null;
      });
    } on PlatformException catch (e) {
      _logException('Unsupported operation$e');
    } catch (e) {
      _logException(e.toString());
    } finally {
      setState(() => isLoading = false);
    }
  }

  Future<void> saveFile() async {
    _resetState();
    try {
      String? fileName = await FilePicker.platform.saveFile(
        allowedExtensions: (extension?.isNotEmpty ?? false)
            ? extension?.replaceAll(' ', '').split(',')
            : null,
        type: pickingType,
      );
      setState(() {
        saveAsFileName = fileName;
        userAborted = fileName == null;
      });
    } on PlatformException catch (e) {
      _logException('Unsupported operation$e');
    } catch (e) {
      _logException(e.toString());
    } finally {
      setState(() => isLoading = false);
    }
  }

  searchPatient(String query) async {
    List<Book> books = await repository.getBooks();
    final suggessitons = books.where((patient) {
      final name = patient.bookName!.toLowerCase();
      final input = query.toLowerCase();
      return name.contains(input);
    }).toList();
    setState(() {
      if (suggessitons.isEmpty || query.isEmpty) {
        images = [];
      } else {
        images = suggessitons;
      }
    });
  }

  bool isUploading = false;
  addNewFile() async {
    final result = await FilePicker.platform.pickFiles(
      allowMultiple: false,
      type: pickingType,
      allowedExtensions: ['pdf'],
    );
    if (result == null) return;
    final appStore = await getApplicationDocumentsDirectory();
    final file = result.files.first;
    final newFile = File('${appStore.path}/${file.path}');
    TextEditingController book_name = TextEditingController();
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        actionsAlignment: MainAxisAlignment.end,
        title: const Text('تم تحميل الكتاب'),
        content: Container(
          height: 15.h,
          child: Column(
            children: [
              Text(file.name),
              SizedBox(
                height: 1.h,
              ),
              TextField(
                controller: book_name,
                textDirection: TextDirection.rtl,
                decoration: InputDecoration(
                  hintText: 'أدخل اسم الكتاب',
                  hintTextDirection: TextDirection.rtl,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              )
            ],
          ),
        ),
        actions: [
          MaterialButton(
            onPressed: () async {
              Fluttertoast.showToast(msg: 'جاري إضافة الكتاب...');
              if (!isUploading) {
                isUploading = true;
                var url = ServerAddresses.serverAddress + 'save_book';
                var request = http.MultipartRequest('POST', Uri.parse(url));
                List<http.MultipartFile> newList = <http.MultipartFile>[];

                newList
                    .add(await http.MultipartFile.fromPath('file', file.path!));
                request.fields['bookName'] = book_name.text;
                request.files.addAll(newList);
                var res = await request.send();
                var response = await http.Response.fromStream(res);
                print(response.body);
                isUploading = false;
                if (res.statusCode == 200) {
                  Fluttertoast.showToast(msg: 'تم إضافة الكتاب بنجاح');
                  Navigator.pop(context);
                  setState(() {});
                }
              }
            },
            child: const Text('حفظ'),
          )
        ],
      ),
    );
  }

  void _logException(String message) {
    scaffoldMessengerKey.currentState?.hideCurrentSnackBar();
    scaffoldMessengerKey.currentState?.showSnackBar(
      SnackBar(
        content: Text(message),
      ),
    );
  }

  void _resetState() {
    if (!mounted) {
      return;
    }
    setState(() {
      isLoading = true;
      directoryPath = null;
      fileName = null;
      paths = null;
      saveAsFileName = null;
      userAborted = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: CustomScrollView(
                slivers: [
                  SliverFillRemaining(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 2.h,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: const Color(0xFF907785),
                              borderRadius: BorderRadius.circular(20)),
                          width: 90.w,
                          height: 7.h,
                          child: Center(
                            child: Text(
                              'كتب علمية',
                              style:
                                  TextStyle(fontSize: 5.w, color: Colors.white),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 4.5.w),
                          child: TextField(
                            controller: search,
                            textAlign: TextAlign.right,
                            textAlignVertical: TextAlignVertical.center,
                            onChanged: searchPatient,
                            decoration: InputDecoration(
                              suffixIcon: Icon(Icons.search,
                                  color: ColorApp.titleColorInRegisterPage),
                              hintText: 'search',
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide(
                                  color: ColorApp.titleColorInRegisterPage,
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 2.h),
                        Padding(
                          padding: EdgeInsets.only(right: 65.w),
                          child: SizedBox(
                            height: 6.h,
                            child: TextButton(
                              onPressed: addNewFile,
                              child: Text(
                                'إضافة كتاب +',
                                style: TextStyle(color: ColorApp.primaryColor),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 1.h),
                        Container(
                          height: 58.h,
                          margin: EdgeInsets.only(left: 2.w, top: 0),
                          width: 90.w,
                          // color: Colors.black,
                          child: Padding(
                            padding: EdgeInsets.only(left: 3.w),
                            child: FutureBuilder<List<Book>>(
                                future: repository.getBooks(),
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    return GridView.builder(
                                      itemCount: snapshot.data!.length,
                                      shrinkWrap: true,
                                      physics: const ScrollPhysics(),
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 2,
                                              mainAxisSpacing: 0.6.h,
                                              childAspectRatio: 0.52),
                                      itemBuilder: (context, index) {
                                        return DesginBook(
                                          iconSizeStar: iconSizeStar,
                                          book: snapshot.data![index],
                                        );
                                      },
                                    );
                                  }
                                  return const Center(
                                    child: CircularProgressIndicator(),
                                  );
                                }),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: 4.5.h,
            child: Text(
              'التعليم',
              style: TextStyle(color: Colors.white, fontSize: 5.w),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class DesginBook extends StatelessWidget {
  String remotePDFpath = '';
  DesginBook({
    Key? key,
    required this.book,
    required this.iconSizeStar,
  }) : super(key: key);

  Book book;
  final int iconSizeStar;

  Future<String> downloadFile(String url, String fileName, String dir) async {
    Fluttertoast.showToast(msg: 'جاري التحميل', toastLength: Toast.LENGTH_LONG);
    HttpClient httpClient = HttpClient();
    File file;
    String filePath = '';
    String myUrl = '';

    try {
      myUrl = url;
      var request = await httpClient.getUrl(Uri.parse(myUrl));
      var response = await request.close();
      if (response.statusCode == 200) {
        var bytes = await consolidateHttpClientResponseBytes(response);
        filePath = '$dir/$fileName';
        file = File(filePath);
        await file.writeAsBytes(bytes);
        Fluttertoast.showToast(
            msg: 'تم التحميل إلى مجلد التنزيلات',
            toastLength: Toast.LENGTH_LONG);
      } else {
        filePath = 'Error code: ${response.statusCode}';
      }
    } catch (ex) {
      filePath = 'Can not fetch url';
    }

    return filePath;
  }

  Future<File> createFileOfPdfUrl() async {
    Completer<File> completer = Completer();

    try {
      var url = book.file!;
      final filename = url.substring(url.lastIndexOf("/") + 1);
      var request = await HttpClient().getUrl(Uri.parse(url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      var dir = await getApplicationDocumentsDirectory();
      File file = File("${dir.path}/$filename");

      await file.writeAsBytes(bytes, flush: true);

      completer.complete(file);
    } catch (e) {
      throw Exception('Error parsing asset file!');
    }

    return completer.future;
  }

  Row buttons(context, file) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: 19.8.w,
          height: 5.h,
          decoration: const BoxDecoration(
              color: Color.fromARGB(255, 216, 216, 216),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(13),
              )),
          child: Center(
            child: TextButton(
                onPressed: () async {
                  // todo add new page to read pdf file
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //     builder: (_) => PDFpage(
                  //       fileUrl: file,
                  //     ),
                  //   ),
                  // );
                  createFileOfPdfUrl().then((f) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PDFScreen(path: f.path),
                      ),
                    );
                  });
                },
                child: const Text('قراءة')),
          ),
        ),
        Container(
          width: 0.5,
          height: 5.h,
          decoration: const BoxDecoration(
            color: Color.fromARGB(255, 0, 0, 0),
          ),
        ),
        Container(
          width: 19.8.w,
          height: 5.h,
          decoration: const BoxDecoration(
              color: Color.fromARGB(255, 216, 216, 216),
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(13),
              )),
          child: Center(
            child: TextButton(
                onPressed: () async {
                  await downloadFile(book.file ?? '', '${book.id ?? ''}.pdf',
                      '/storage/emulated/0/Download/');
                },
                child: const Text('تحميل')),
          ),
        ),
      ],
    );
  }

  rateBook() {
    return Container(
      padding: EdgeInsets.only(left: 7.w),
      width: 40.w,
      height: 3.h,
      child: RatingBar.builder(
        initialRating: double.parse(book.ratings ?? "1"),
        minRating: 1,
        direction: Axis.horizontal,
        allowHalfRating: true,
        itemCount: 5,
        itemSize: 5.w,
        itemBuilder: (context, _) => const Icon(
          Icons.star,
          color: Colors.amber,
        ),
        onRatingUpdate: (rating) {},
      ),
    );
  }

  // Container imageBook() {
  //   PdfViewerController? controller;
  //   return Container(
  //     width: 40.w,
  //     height: 25.h,
  //     decoration: const BoxDecoration(
  //         borderRadius: BorderRadius.only(
  //       topLeft: Radius.circular(13),
  //       topRight: Radius.circular(13),
  //     )),
  //     child: PdfViewer.openFutureFile(
  //       () async =>
  //           (await DefaultCacheManager().getSingleFile(book.file!)).path,
  //       viewerController: controller,
  //       params: PdfViewerParams(
  //           pageNumber: 1,
  //           padding: 0,
  //           onViewerControllerInitialized: (PdfViewerController c) {
  //             controller = c;
  //             // scrolling animation to page 3.
  //           }),
  //       loadingBannerBuilder: (p0) {
  //         return const SpinKitDoubleBounce(
  //           color: Colors.orange,
  //           size: 50.0,
  //         );
  //       },
  //     ),

  //     // CachedNetworkImage(
  //     //   imageUrl: book.bookImage ?? 'default.png',
  //     //   fit: BoxFit.fill,
  //     // ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 40.w,
          height: 39.h,
          decoration: const BoxDecoration(
            color: Color(0xFFf5f5f5),
            borderRadius: BorderRadius.all(
              Radius.circular(13),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // imageBook(),
              SizedBox(height: 1.h),
              rateBook(),
              SizedBox(height: 1.h),
              Text(book.bookName ?? 'Book'),
              SizedBox(height: 1.h),
              buttons(context, "book.pdf")
            ],
          ),
        ),
      ],
    );
  }
}

class PDFpage extends StatefulWidget {
  PDFpage({Key? key, required this.fileUrl}) : super(key: key);

  String fileUrl;

  @override
  State<PDFpage> createState() => _PDFpageState();
}

class _PDFpageState extends State<PDFpage> {
  int? currentPage = 0;
  PDFDocument? doc;
  String errorMessage = '';
  File? fileFile;
  bool? isLoading = true;
  bool isReady = false;
  int? pages = 0;
  String remotePDFpath = '';

  final Completer<PDFViewController> _controller =
      Completer<PDFViewController>();

  @override
  void initState() {
    super.initState();

    createFileOfPdfUrl().then((f) {
      setState(() {
        remotePDFpath = f.path;
      });
    });
  }

  Future<File> createFileOfPdfUrl() async {
    Completer<File> completer = Completer();

    try {
      const url = "${ServerAddresses.uploadUrl}/books/book.pdf)";
      final filename = url.substring(url.lastIndexOf("/") + 1);
      var request = await HttpClient().getUrl(Uri.parse(url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      var dir = await getApplicationDocumentsDirectory();
      File file = File("${dir.path}/$filename");

      await file.writeAsBytes(bytes, flush: true);
      setState(() {
        fileFile = file;
      });
      completer.complete(file);
    } catch (e) {
      throw Exception('Error parsing asset file!');
    }

    return completer.future;
  }

  future() async {
    doc = await PDFDocument.fromURL(
        "https://dentalknights.com/uploads/books/book.pdf");
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: isLoading!
              ? const CircularProgressIndicator()
              : Stack(
                  children: <Widget>[
                    PDFView(
                      filePath: fileFile!.path,
                      enableSwipe: true,
                      swipeHorizontal: true,
                      autoSpacing: false,
                      pageFling: true,
                      pageSnap: true,
                      defaultPage: currentPage!,
                      fitPolicy: FitPolicy.BOTH,
                      preventLinkNavigation:
                          false, // if set to true the link is handled in flutter
                      onRender: (pages) {
                        setState(() {
                          pages = pages;
                          isReady = true;
                        });
                      },
                      onError: (error) {
                        setState(() {
                          errorMessage = error.toString();
                        });
                        print(error.toString());
                      },
                      onPageError: (page, error) {
                        setState(() {
                          errorMessage = '$page: ${error.toString()}';
                        });
                        print('$page: ${error.toString()}');
                      },
                      onViewCreated: (PDFViewController pdfViewController) {
                        _controller.complete(pdfViewController);
                      },
                      onLinkHandler: (String? uri) {},
                      onPageChanged: (int? page, int? total) {
                        print('page change: $page/$total');
                        setState(() {
                          currentPage = page;
                        });
                      },
                    ),
                    errorMessage.isEmpty
                        ? !isReady
                            ? const Center(
                                child: CircularProgressIndicator(),
                              )
                            : Container()
                        : Center(
                            child: Text(errorMessage),
                          )
                  ],
                )),
    );
  }
}
