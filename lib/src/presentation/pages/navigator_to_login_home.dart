import 'package:dental_kinghts/src/app_color.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '/src/presentation/pages/filter_doctor_by_country.dart';
import 'login_page.dart';

class NavigatorToLoginOrHome extends StatefulWidget {
  const NavigatorToLoginOrHome({Key? key}) : super(key: key);

  @override
  State<NavigatorToLoginOrHome> createState() => _NavigatorToLoginOrHomeState();
}

class _NavigatorToLoginOrHomeState extends State<NavigatorToLoginOrHome> {
  onPressedTextButtonLogin() {
    Navigator.push(
        context, MaterialPageRoute(builder: (_) => const LoginPage()));
  }

  onTapImageToSearchDoctor() {
    Navigator.push(context,
        MaterialPageRoute(builder: (_) => const FilterDoctorByCountry()));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 20.h),
            DefaultTextStyle(
              textAlign: TextAlign.end,
              maxLines: 2,
              style: TextStyle(
                fontSize: 6.w,
                color: Colors.black,
              ),
              child: const Text(
                'دليلك لأفضل طبيب أسنان',
                style: TextStyle(
                  fontFamily: 'Droid Sans Arabic',
                ),
              ),
            ),
            GestureDetector(
              onTap: onTapImageToSearchDoctor,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    height: MediaQuery.of(context).size.height * 0.5,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.contain,
                        image: AssetImage(
                          'assets/images/logo1.png',
                        ),
                      ),
                    ),
                  ),
                  const Positioned(
                    child: Padding(
                      padding: EdgeInsets.only(right: 10, bottom: 5),
                      child: DefaultTextStyle(
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                            color: Colors.black),
                        child: Text('ابحث'),
                      ),
                    ),
                  ),

                ],
              ),

            ),

            Padding(
              padding: EdgeInsets.only(top: 10.h),
              child: Container(
                width: 50.w,
                height: 6.h,

                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15), color: AppColor().primaryColor),
                child: MaterialButton(
                  onPressed: onPressedTextButtonLogin,
                  child: DefaultTextStyle(
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: 6.w,
                      color: const Color.fromRGBO(58, 64, 72, 50),
                    ),
                    child: const Text(
                      'دخول الممارسين',
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontFamily: 'Droid Sans Arabic',
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ));
    // return Container(
    //   width: double.infinity,
    //   height: double.infinity,
    //   decoration: const BoxDecoration(
    //     //     gradient: LinearGradient(colors: [
    //     //   AppColor().primaryColor,
    //     //   AppColor().secondaryColorBlod,
    //     // ])
    //     image: DecorationImage(
    //         fit: BoxFit.fill,
    //         image: AssetImage('assets/images/background.png')),
    //   ),
    //   child: Center(
    //     child: Column(
    //       crossAxisAlignment: CrossAxisAlignment.center,
    //       children: [
    //         Expanded(child: Container()),
    //         Align(
    //           alignment: Alignment.center,
    //           child: DefaultTextStyle(
    //             textAlign: TextAlign.end,
    //             maxLines: 2,
    //             style: TextStyle(
    //               fontSize: 6.w,
    //               color: Colors.white,
    //             ),
    //             child: const Text(
    //               'دليلك لأفضل طبيب أسنان',
    //               style: TextStyle(
    //                 fontFamily: 'Droid Sans Arabic',
    //               ),
    //             ),
    //           ),
    //         ),
    //         SizedBox(
    //           height: 2.h,
    //         ),
    //         Center(
    //           child: Stack(
    //             children: [
    //               GestureDetector(

    //                 child: Container(
    //                   height: 30.h,
    //                   width: 50.w,
    //                   decoration: const BoxDecoration(
    //                       image: DecorationImage(
    //                           image: AssetImage('assets/images/logo.png'))),
    //                   child: const Padding(
    //                     padding: EdgeInsets.only(bottom: 25.0),
    //                     child: Center(
    //                       child: DefaultTextStyle(
    //                         style: TextStyle(
    //                             color: Colors.white,
    //                             fontSize: 17,
    //                             fontWeight: FontWeight.w700),
    //                         child: Text('ابحث'),
    //                       ),
    //                     ),
    //                   ),
    //                 ),
    //               ),
    //             ],
    //           ),
    //         ),
    //         Expanded(child: Container()),
    //         Container(
    //           width: 50.w,
    //           height: 6.h,
    //           decoration: BoxDecoration(
    //               borderRadius: BorderRadius.circular(15), color: Colors.white),
    //           child: MaterialButton(
    //             onPressed: onPressedTextButtonLogin,
    //             child: DefaultTextStyle(
    //               textAlign: TextAlign.justify,
    //               style: TextStyle(
    //                 fontSize: 6.w,
    //                 color: const Color.fromRGBO(58, 64, 72, 50),
    //               ),
    //               child: const Text(
    //                 'دخول الممارسين',
    //                 style: TextStyle(
    //                   color: Colors.black,
    //                   fontFamily: 'Droid Sans Arabic',
    //                 ),
    //               ),
    //             ),
    //           ),
    //         ),
    //         SizedBox(
    //           height: 3.h,
    //         )
    //       ],
    //     ),
    //   ),
    // );
  }
}
