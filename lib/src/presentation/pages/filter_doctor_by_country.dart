import 'dart:convert';

import 'package:dental_kinghts/src/app_color.dart';
import 'package:dental_kinghts/src/constants/server_addresses.dart';
import 'package:dental_kinghts/src/data/models/city.dart';
import 'package:dental_kinghts/src/data/models/country.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sizer/sizer.dart';

import '/src/presentation/pages/home_page_patients.dart';

class FilterDoctorByCountry extends StatefulWidget {
  const FilterDoctorByCountry({Key? key}) : super(key: key);

  @override
  State<FilterDoctorByCountry> createState() => _FilterDoctorByCountryState();
}

class _FilterDoctorByCountryState extends State<FilterDoctorByCountry> {
  String currentCountry = 'السعودية';
  String currentCity = 'الرياض';
  Set<String> countriesName = {'السعودية'};
  Set<String> citiesName = {'الرياض'};

  onPressedNextButton() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (_) => HomePagePatients(
                  cityId: _selectedCityObj?.id ?? '1',
                  countryId: _selectedCountryObj?.id ?? '',
                )));
  }

  @override
  void initState() {
    super.initState();
  }

  List<Country> countriesList = [];

  Future<List> getCountries() async {
    if (countriesName.length == 1) {
      final data = await http
          .get(Uri.parse("${ServerAddresses.serverAddress}countries"));
      var responseBody = data.body;
      print(responseBody);
      final parsed = json.decode(data.body).cast<Map<String, dynamic>>();
      countriesList =
          parsed.map<Country>((json) => Country.fromJson(json)).toList();

      for (int i = 0; i < countriesList.length; i++) {
        countriesName.add(countriesList[i].name ?? 'اختر البلد');
      }
    }

    return countriesName.toList();
  }

  List<Cities> citiesList = [];

  Future<Set<String>> getStates(String country_id) async {
    if (citiesName.length == 1) {
      final data = await http.get(Uri.parse(
          ServerAddresses.serverAddress + "cities?country_id=" + country_id));
      var responseBody = data.body;
      print(responseBody);
      final parsed = json.decode(data.body).cast<Map<String, dynamic>>();

      citiesList = parsed.map<Cities>((json) => Cities.fromJson(json)).toList();

      for (int i = 0; i < citiesList.length; i++) {
        citiesName.add(citiesList[i].name ?? 'المدينة');
      }
    }

    return citiesName;
  }

  Country? _selectedCountryObj;
  Cities? _selectedCityObj;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Expanded(
              child: Container(),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15.0),
              child: Image.asset(
                'assets/images/logo1.png',
                fit: BoxFit.cover,
                height: 20.h,
                width: 33.w,
              ),
            ),
            SizedBox(height: 2.h),
            DefaultTextStyle(
              textAlign: TextAlign.justify,
              style: TextStyle(
                fontSize: 7.w,
                color: Colors.black,
              ),
              child: const Text(
                'اختر الدولة',
                textAlign: TextAlign.right,
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 70.w,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: FutureBuilder<List>(
                      future: getCountries(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return DropdownButtonFormField<String>(
                            elevation: 10,
                            alignment: Alignment.center,
                            decoration: InputDecoration(
                              // fillColor: Colors.grey,
                              // focusColor: Colors.grey,
                              // hoverColor: Colors.grey,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15),
                                borderSide: const BorderSide(
                                  color: Colors.black,
                                  width: 1,
                                ),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15),
                                borderSide: const BorderSide(
                                  color: Colors.black,
                                  width: 1,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15),
                                borderSide: const BorderSide(
                                  color: Colors.black,
                                  width: 1,
                                ),
                              ),
                            ),
                            style: const TextStyle(color: Colors.black),
                            value: currentCountry,
                            isExpanded: true,
                            icon: const Icon(
                              Icons.keyboard_arrow_down,
                              color: Colors.black,
                            ),
                            items: countriesName.map(
                              (String item) {
                                return DropdownMenuItem(
                                  alignment: Alignment.centerRight,
                                  value: item,
                                  child: Padding(
                                    padding: EdgeInsets.only(right: 4.w),
                                    child: Text(
                                      item,
                                      textAlign: TextAlign.right,
                                      style: const TextStyle(
                                          color: Color.fromARGB(255, 0, 0, 0),
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                );
                              },
                            ).toList(),
                            onChanged: (String? newvalue) async {
                              Country country = countriesList.firstWhere(
                                  (element) =>
                                      element.name == newvalue.toString());
                              _selectedCountryObj = country;
                              citiesName.clear();
                              if (country.states != null) {
                                citiesList.clear();
                                citiesList.addAll(country.states!);
                                for (var element in country.states!) {
                                  citiesName.add(element.name!);
                                }
                              }
                              currentCity = citiesName.first;
                              setState(() {
                                currentCountry = newvalue!;
                              });
                            },
                          );
                        }
                        return const Center(child: CircularProgressIndicator());
                      },
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 3.h,
            ),
            DefaultTextStyle(
              textAlign: TextAlign.justify,
              style: TextStyle(
                fontSize: 7.w,
                color: Colors.black,
              ),
              child: const Text(
                'اختر المدينة',
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 70.w,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: DropdownButtonFormField<String>(
                      elevation: 10,
                      alignment: Alignment.center,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: const BorderSide(
                            color: Colors.black,
                            width: 1,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: const BorderSide(
                            color: Colors.black,
                            width: 1,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: const BorderSide(
                            color: Colors.black,
                            width: 1,
                          ),
                        ),
                      ),
                      // style: const TextStyle(
                      //   color: Colors.white,
                      // ),
                      isExpanded: true,
                      value: currentCity,
                      icon: const Icon(
                        Icons.keyboard_arrow_down,
                        color: Colors.black,
                      ),
                      items: citiesName.map(
                        (String item) {
                          return DropdownMenuItem(
                            alignment: Alignment.centerRight,
                            value: item,
                            child: Padding(
                              padding: EdgeInsets.only(right: 4.w),
                              child: Text(
                                item,
                                textAlign: TextAlign.right,
                                style: const TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          );
                        },
                      ).toList(),
                      onChanged: (String? newvalue) {
                        Cities city = citiesList.firstWhere(
                            (element) => element.name == newvalue.toString());
                        _selectedCityObj = city;
                        setState(() {
                          currentCity = newvalue!;
                        });
                      },
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 1.h,
            ),
            Expanded(child: Container()),
            Container(
              width: 40.w,
              height: 6.h,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: AppColor().primaryColor),
              child: MaterialButton(
                onPressed: onPressedNextButton,
                child: const Text(
                  'التالي',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            Expanded(child: Container()),
          ],
        ),
      ),
    );
  }
}
