import 'package:dental_kinghts/src/data/models/doctor_model.dart';
import 'package:dental_kinghts/src/data/repositories/repository.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../constants/color_app.dart';
import '../widgets/card_doctor.dart';

class HomePagePatients extends StatefulWidget {
  final String? cityId;
  final String? countryId;

  const HomePagePatients({Key? key, this.cityId, this.countryId})
      : super(key: key);

  @override
  State<HomePagePatients> createState() => _HomePagePatientsState();
}

class _HomePagePatientsState extends State<HomePagePatients> {
  List<Doctor> doctors = [];
  List<Doctor> suggesstions = [];
  final Repository repo = Repository();

  BoxDecoration decorationOuterContainar() {
    return const BoxDecoration(
      image: DecorationImage(
        fit: BoxFit.fill,
        image: AssetImage('assets/images/background.png'),
      ),
    );
  }

  BoxDecoration decorationInnerContainer() {
    return const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(50),
        topRight: Radius.circular(50),
      ),
      boxShadow: [
        BoxShadow(
          color: Color.fromARGB(255, 0, 0, 0),
          offset: Offset(
            5.0,
            5.0,
          ),
          blurRadius: 20.0,
          spreadRadius: 2.0,
        ), //BoxShadow
        BoxShadow(
          color: Colors.white,
          offset: Offset(0.0, 0.0),
          blurRadius: 0.0,
          spreadRadius: 0.0,
        ), //BoxShadow
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: decorationOuterContainar(),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Positioned(
                top: 1.h,
                // right: 37.w,
                child: DefaultTextStyle(
                  style: const TextStyle(color: Colors.white),
                  child: Text(
                    'الصفحة الرئيسية',
                    style: TextStyle(fontSize: 5.w),
                  ),
                ),
              ),
              Positioned(
                top: 0.5.h,
                left: 85.w,
                child: IconButton(
                  onPressed: () {
                    showSearch(
                        context: context,
                        delegate: MySearchDelegate(doctors, suggesstions));
                  },
                  icon: const Icon(
                    Icons.search,
                    color: Colors.white,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 7.h),
                child: Container(
                  width: double.infinity,
                  decoration: decorationInnerContainer(),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 2.h,
                      ),
                      DefaultTextStyle(
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 5.w,
                          color: ColorApp.imageOrDetialsColor,
                        ),
                        child: const Text('الأطباء المسجلين في منطقتك'),
                      ),
                      SizedBox(
                        height: 3.h,
                      ),
                      Expanded(
                          child: FutureBuilder<List<Doctor>>(
                              future: repo.patient_doctors(
                                  widget.countryId ?? '213',
                                  widget.cityId ?? "3465"),
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  doctors.addAll(snapshot.data!);
                                  suggesstions = doctors;
                                  return ListView.separated(
                                    itemCount: snapshot.data!.length,
                                    separatorBuilder:
                                        (BuildContext context, int index) =>
                                            SizedBox(
                                      height: 3.h,
                                    ),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return CardDoctor(
                                        nameDoctor: snapshot.data![index].name,
                                        medicalComplex: snapshot
                                            .data![index].medicalComplex,
                                        doctor: snapshot.data![index],
                                      );
                                    },
                                  );
                                } else {
                                  return const Center(
                                      child: CircularProgressIndicator());
                                }
                              })),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MySearchDelegate extends SearchDelegate<Doctor> {
  List<Doctor> doctors = [];
  List<Doctor> suggestions = [];

  MySearchDelegate(this.doctors, this.suggestions);

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(onPressed: () {}, icon: const Icon(Icons.clear));
  }

  @override
  Widget buildResults(BuildContext context) {
    final list = doctors
        .where((doctor) =>
            doctor.name!.toLowerCase().contains(query.toLowerCase()))
        .toList();
    print('search delegertds fdsfmsdljfsdklfjs');
    return ListView.separated(
      itemCount: list.length,
      itemBuilder: (context, index) {
        return CardDoctor(
          nameDoctor: list[index].name,
          medicalComplex: list[index].medicalComplex,
          doctor: list[index],
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 2.h,
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final list = suggestions
        .where((doctor) =>
            doctor.name!.toLowerCase().contains(query.toLowerCase()))
        .toList();
    print('suggestion');
    print(query);
    print(suggestions.length);
    return ListView.separated(
      itemCount: list.length,
      itemBuilder: (context, index) {
        return CardDoctor(
          nameDoctor: list[index].name,
          medicalComplex: list[index].medicalComplex,
          doctor: list[index],
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 2.h,
        );
      },
    );
  }
}
