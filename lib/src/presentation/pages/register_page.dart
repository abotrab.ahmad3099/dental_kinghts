import 'dart:convert';

import 'package:dental_kinghts/src/constants/server_addresses.dart';
import 'package:dental_kinghts/src/data/models/question.dart';
import 'package:dental_kinghts/src/data/repositories/repository.dart';

import '/src/presentation/pages/second_page_register.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:http/http.dart' as http;
import '../../constants/color_app.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController email = TextEditingController();
  final formKey = GlobalKey<FormState>();
  TextEditingController password = TextEditingController();
  List<bool> values = [];
  List<String?> selectedQuestions = [];

  @override
  void initState() {
    // for (var i = 0; i < Questions.ques.length; i++) {
    //   values.add(false);
    // }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: decorationOuterContainar(),
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Positioned(
              top: 3.h,
              right: 40.w,
              child: SizedBox(
                height: 15.h,
                child: Image.asset(
                  'assets/images/logo.png',
                  color: Colors.white60,
                ),
              ),
            ),
            Positioned(
              right: 90.w,
              child: SizedBox(
                height: 15.h,
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.h),
              child: formMethod(),
            ),
          ],
        ),
      ),
    );
  }

  Form formMethod() {
    return Form(
      key: formKey,
      child: Container(
        width: double.infinity,
        decoration: decorationInnerContainer(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 3.h,
            ),
            DefaultTextStyle(
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 5.w,
                color: Colors.black,
              ),
              child: const Text(
                'فرساننا الأعزاء يمكنكم التسجيل في منصة طب الفم والأسنان والاستفادة من خدماتنا الكثيرة',
                maxLines: 2,
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color(0xFFefeff1),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: DefaultTextStyle(
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    style: TextStyle(
                      fontSize: 4.w,
                      color: const Color.fromARGB(255, 126, 125, 125),
                    ),
                    child: const Text(
                        'سوف تجد كل ما تجتاجه وستتمكن من التواصل مع بقيٌة الفرسان في منصة اجتماعيٌة خاصة بطب الأسنان'),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 3.h,
            ),
            questionsList(),
            SizedBox(
              height: 2.h,
            ),
            Expanded(child: Container()),
            Container(
              width: 35.w,
              height: 7.h,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                gradient: LinearGradient(
                  colors: [ColorApp.primaryColor, ColorApp.blueColor],
                ),
              ),
              child: MaterialButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => SecondPageRegister(
                        values: values,
                        selectedQuestions: selectedQuestions,
                      ),
                    ),
                  );
                },
                child: const Text(
                  'التالي',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 3.h,
            ),
          ],
        ),
      ),
    );
  }

  List<String> questions = [];
  List<Question> countriesList = [];
  final Repository repo = Repository();

  questionsList() {
    return Flexible(
        flex: 3,
        child: FutureBuilder<List<Question>>(
            future: repo.getQuestions('check_question', ''),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                if (values.isEmpty) {
                  values = snapshot.data!.map((e) => false).toList();
                  selectedQuestions = snapshot.data!.map((e) => '').toList();
                }

                return ListView.builder(
                  itemCount: snapshot.data!.length,
                  itemBuilder: (context, index) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Checkbox(
                              value: values[index],
                              focusColor: Colors.black,
                              checkColor: Colors.black,
                              activeColor:
                                  const Color.fromARGB(255, 224, 223, 223),
                              splashRadius: 2,
                              onChanged: (value) {
                                setState(() {
                                  values[index] = value!;
                                  if (value == true) {
                                    selectedQuestions[index] =
                                        (snapshot.data![index].id);
                                  } else {
                                    selectedQuestions[index] = '';
                                  }
                                });
                              },
                            ),
                            Container(
                              alignment: Alignment.topRight,
                              margin: EdgeInsets.only(right: 3.w),
                              child: Row(
                                textDirection: TextDirection.rtl,
                                children: [
                                  Text('- ${index + 1}'),
                                  SizedBox(
                                    width: 5.w,
                                  ),
                                  Text(snapshot.data![index].text!),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 1.w),
                        const Divider()
                      ],
                    );
                  },
                );
              }
              return const Center(child: CircularProgressIndicator());
            }));
  }

  BoxDecoration decorationInnerContainer() {
    return const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(50),
        topRight: Radius.circular(50),
      ),
      boxShadow: [
        BoxShadow(
          color: Color.fromARGB(255, 0, 0, 0),
          offset: Offset(
            5.0,
            5.0,
          ),
          blurRadius: 20.0,
          spreadRadius: 2.0,
        ), //BoxShadow
        BoxShadow(
          color: Colors.white,
          offset: Offset(0.0, 0.0),
          blurRadius: 0.0,
          spreadRadius: 0.0,
        ), //BoxShadow
      ],
    );
  }

  BoxDecoration decorationOuterContainar() {
    return const BoxDecoration(
      image: DecorationImage(
        fit: BoxFit.fill,
        image: AssetImage('assets/images/background.png'),
      ),
    );
  }
}
