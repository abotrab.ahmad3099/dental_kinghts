import '/src/data/models/doctor_model.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../constants/color_app.dart';
import '../widgets/details_info_personality.dart';
import '../widgets/drawer_doctor.dart';
import 'home_page_doctor.dart';

// ignore: must_be_immutable
class Profile extends StatefulWidget {
  Profile({
    Key? key,
    required this.doctor,
    required this.isUserDoctor,
  }) : super(key: key);

  Doctor doctor;
  bool isUserDoctor;

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  bool acceptToAll = false;
  bool faceCovering = false;
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();

  whereIsWork(staticTitle, content) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 2.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          DefaultTextStyle(
            style: const TextStyle(
                color: Colors.grey, fontWeight: FontWeight.bold),
            child: Text(
              staticTitle,
            ),
          ),
          SizedBox(
            height: 1.h,
          ),
          DefaultTextStyle(
            style: const TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold),
            child: Text(content),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 13.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: CustomScrollView(
                slivers: [
                  SliverFillRemaining(
                    hasScrollBody: false,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: 2.5.h),
                        CircleAvatar(
                          backgroundColor: ColorApp.primaryColor,
                          radius: 11.w,
                          child: Image.asset('assets/images/doctor.png'),
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 2.w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              widget.isUserDoctor
                                  ? IconButton(
                                      onPressed: () {},
                                      icon: Icon(
                                        Icons.edit,
                                        color: ColorApp.primaryColor,
                                      ),
                                      iconSize: 4.w,
                                    )
                                  : Container(),
                              const Text(
                                'المعلومات الشخصية',
                                style: TextStyle(fontWeight: FontWeight.w600),
                              )
                            ],
                          ),
                        ),
                        const Divider(),
                        SizedBox(height: 1.5.h),
                        Details(
                          title: 'الاسم',
                          content: widget.doctor.name!,
                          icon: Icons.person,
                        ),
                        SizedBox(height: 1.5.h),
                        Details(
                          title: 'الجنسية',
                          content: widget.doctor.nationality!,
                          icon: Icons.access_alarm,
                        ),
                        SizedBox(height: 1.5.h),
                        Details(
                          title: 'التخصص',
                          content: widget.doctor.specialization!,
                          icon: Icons.pages_outlined,
                        ),
                        SizedBox(height: 1.5.h),
                        Details(
                          title: 'معلومات أخرى',
                          content: widget.doctor.info!,
                          icon: Icons.perm_camera_mic_outlined,
                        ),
                        SizedBox(height: 2.h),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 2.w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              widget.isUserDoctor
                                  ? IconButton(
                                      onPressed: () {},
                                      icon: Icon(
                                        Icons.edit,
                                        color: ColorApp.primaryColor,
                                      ),
                                      iconSize: 4.w,
                                    )
                                  : Container(),
                              const Text(
                                'معلومات التواصل',
                                style: TextStyle(fontWeight: FontWeight.w600),
                              )
                            ],
                          ),
                        ),
                        const Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            whereIsWork(
                              'المدينة التي يعمل بها',
                              widget.doctor.city!,
                            ),
                            Expanded(child: Container()),
                            whereIsWork(
                              'الدولة التي يعمل بها',
                              widget.doctor.country!,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 3.w),
                              child: CircleAvatar(
                                backgroundColor: ColorApp.primaryColor,
                                radius: 3.w,
                                child: Icon(
                                  Icons.location_on,
                                  color: Colors.white,
                                  size: 4.w,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            whereIsWork(
                              'عنوان المجمع أو العيادة',
                              widget.doctor.adressOfMedicalComplext!,
                            ),
                            Expanded(child: Container()),
                            whereIsWork(
                              'اسم المجمع أو العيادة',
                              widget.doctor.medicalComplex!,
                            ),
                            SizedBox(width: 12.w),
                          ],
                        ),
                        SizedBox(height: 2.h),
                        GestureDetector(
                          onTap: () => launch('tel://${widget.doctor.phone}'),
                          child: Padding(
                            padding: EdgeInsets.only(right: 1.w),
                            child: Details(
                                title: 'رقم التواصل',
                                content: widget.doctor.phone!,
                                icon: Icons.phone),
                          ),
                        ),
                        SizedBox(height: 2.h),
                        GestureDetector(
                          onTap: () => launch('https://${widget.doctor.email}'),
                          child: Padding(
                            padding: EdgeInsets.only(right: 1.w),
                            child: Details(
                              title: 'البريد الالكتروني',
                              content: widget.doctor.email!,
                              icon: Icons.email,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 3.h,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 2.w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              widget.isUserDoctor
                                  ? IconButton(
                                      onPressed: () {},
                                      icon: Icon(
                                        Icons.edit,
                                        color: ColorApp.primaryColor,
                                      ),
                                      iconSize: 4.w,
                                    )
                                  : Container(),
                              const Text(
                                'الشروط والموافقات',
                                style: TextStyle(fontWeight: FontWeight.w600),
                              )
                            ],
                          ),
                        ),
                        const Divider(),
                        SizedBox(
                          height: 1.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            const Expanded(
                              child: Text(
                                'أوافق علي نشر معلوماتي لغرض التسويق كما أتعهد بأن جميع معلوماتي صحيحة',
                                maxLines: 2,
                                textAlign: TextAlign.right,
                                textDirection: TextDirection.rtl,
                              ),
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            Checkbox(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              value: acceptToAll,
                              activeColor: ColorApp.titleColorInRegisterPage,
                              checkColor: Colors.black,
                              onChanged: ((value) => setState(
                                    () {
                                      acceptToAll = value!;
                                    },
                                  )),
                            ),
                            SizedBox(
                              height: 2.h,
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 4.5.h,
            left: 35.w,
            child: Text(
              'ملفي الشخصي',
              style: TextStyle(
                  color: ColorApp.titleColorInRegisterPage, fontSize: 5.w),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 90.w,
            child: IconButton(
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (_) => const HomePageDoctor()),
                    (route) => false);
              },
              icon: const Icon(
                Icons.home_outlined,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
