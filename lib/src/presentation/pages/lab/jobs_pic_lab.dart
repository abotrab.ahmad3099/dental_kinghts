import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:sizer/sizer.dart';

import '../../../constants/patient_api.dart';
import '../../../constants/server_addresses.dart';
import '../../../data/models/factory.dart';
import '../../../data/repositories/repository.dart';
import '../../../local_data.dart';
import '../../widgets/drawer_doctor.dart';

class JobsPictureLab extends StatefulWidget {
  JobsPictureLab({Key? key, required this.factoryWorkImages}) : super(key: key);
  List<FactoryWorkImages> factoryWorkImages = [];

  @override
  State<JobsPictureLab> createState() => _JobsPictureLabState();
}

class _JobsPictureLabState extends State<JobsPictureLab> {
  File? image;
  List images = [];
  Repository repository = Repository();
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    if (LocalData.doctor == null) LocalData();
    super.initState();
  }

  searchPatient(String query) {
    final suggessitons = PatientApi.patients.where((patient) {
      final name = patient.name!.toLowerCase();
      final input = query.toLowerCase();
      return name.contains(input);
    }).toList();
    setState(() {
      if (suggessitons.isEmpty || query.isEmpty) {
        images = [];
      } else {
        images = suggessitons;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: CustomScrollView(
                slivers: [
                  SliverFillRemaining(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 2.h,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: const Color(0xFF907785),
                              borderRadius: BorderRadius.circular(20)),
                          width: 90.w,
                          height: 7.h,
                          child: Center(
                            child: Text(
                              'صور أعمال معمل الأسنان',
                              style:
                                  TextStyle(fontSize: 5.w, color: Colors.white),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 0),
                          height: 58.h,
                          width: 90.w,
                          child: Center(
                            child: Container(
                              margin: EdgeInsets.only(left: 3.w, top: 0),
                              child: widget.factoryWorkImages.isEmpty
                                  ? const Center(
                                      child: CircularProgressIndicator(),
                                    )
                                  : GridView.builder(
                                      itemCount:
                                          widget.factoryWorkImages.length,
                                      // shrinkWrap: true,
                                      physics: const ScrollPhysics(),
                                      gridDelegate:
                                          const SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 2,
                                              childAspectRatio: 0.98),
                                      itemBuilder: (context, index) {
                                        return Row(
                                          children: [
                                            Container(
                                              width: 40.w,
                                              height: 26.h,
                                              decoration: const BoxDecoration(
                                                color: Colors.white,
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(13),
                                                ),
                                              ),
                                              child: CachedNetworkImage(
                                                imageUrl:
                                                    "${ServerAddresses.uploadUrl}${widget.factoryWorkImages[index].factoryWorkImage!}",
                                                progressIndicatorBuilder:
                                                    (context, url,
                                                            downloadProgress) =>
                                                        const SpinKitDoubleBounce(
                                                  color: Colors.orange,
                                                  size: 50.0,
                                                ),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        const Icon(Icons.error),
                                              ),
                                            ),
                                          ],
                                        );
                                      },
                                    ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: 4.5.h,
            child: Text(
              'صور أعمال معلم الأسنان',
              style: TextStyle(color: Colors.white, fontSize: 5.w),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
