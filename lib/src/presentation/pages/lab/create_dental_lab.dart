import 'dart:convert';
import 'dart:io';

import 'package:dental_kinghts/src/controllers/product_controller.dart';
import 'package:dental_kinghts/src/logic/lab_controller.dart';
import 'package:dental_kinghts/src/presentation/pages/lab/products_price_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:loading_btn/loading_btn.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sizer/sizer.dart';

import '../../../constants/color_app.dart';
import '../../../constants/server_addresses.dart';
import '../../../constants/storage.dart';
import '../../../data/models/city.dart';
import '../../../data/models/country.dart';
import '../../widgets/drawer_doctor.dart';

class CreateDentalLab extends StatefulWidget {
  const CreateDentalLab({Key? key}) : super(key: key);

  @override
  State<CreateDentalLab> createState() => _CreateDentalLabState();
}

class _CreateDentalLabState extends State<CreateDentalLab> {
  Color backgroundColor = const Color.fromARGB(255, 245, 244, 244);
  List<Cities> citiesList = [];
  List<String> citiesName = ['اختر مدينة'];
  List<Country> countriesList = [];
  List<String> countriesName = ['اختر البلد'];
  String currentCity = 'اختر مدينة';
  String currentCountry = 'اختر البلد';
  File? logoImage;
  File? tempImage;

  bool thereImage = false;
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  Cities? selectedCityObj;
  Country? selectedCountryObj;
  List<String> constantTitleText = [
    'البريد الالكتروني',
    'رقم التواصل',
    'مدير المعمل',
    'رقم السجل',
    'عنوان المعمل'
  ];
  List<TextEditingController> textController = [];
  var controllerLab = Get.put(LabController());

  Future<List<String>> getStates(String country_id) async {
    citiesName = ['اختر مدينة'];
    if (citiesName.length == 1) {
      final data = await http.get(Uri.parse(
          "${ServerAddresses.serverAddress}cities?country_id=$country_id"));
      var responseBody = data.body;
      final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

      citiesList = parsed.map<Cities>((json) => Cities.fromJson(json)).toList();

      for (int i = 0; i < citiesList.length; i++) {
        citiesName.add(citiesList[i].name ?? 'المدينة');
      }
    }

    return citiesName;
  }

  Future<List> getCountries() async {
    if (countriesName.length == 1) {
      final data = await http
          .get(Uri.parse("${ServerAddresses.serverAddress}countries"));
      var responseBody = data.body;
      final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

      countriesList =
          parsed.map<Country>((json) => Country.fromJson(json)).toList();

      for (int i = 0; i < countriesList.length; i++) {
        countriesName.add(countriesList[i].name ?? 'اختر البلد');
      }
    }

    return countriesName;
  }

  textFieldInput(text, controller, {type}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 3.w),
      child: TextField(
        keyboardType: type ?? TextInputType.text,
        controller: controller,
        textAlign: TextAlign.right,
        decoration: InputDecoration(
          fillColor: backgroundColor,
          filled: true,
          focusColor: backgroundColor,
          hoverColor: backgroundColor,
          hintText: text,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(
              color: backgroundColor,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(
              color: backgroundColor,
            ),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(
              color: backgroundColor,
            ),
          ),
        ),
      ),
    );
  }

  lableText(text) {
    return Padding(
      padding: EdgeInsets.only(right: 7.w),
      child: Align(
        alignment: Alignment.centerRight,
        child: Text(
          text,
          style: TextStyle(
            fontSize: 5.w,
            color: ColorApp.titleColorInRegisterPage,
          ),
        ),
      ),
    );
  }

  dropdownCountry() {
    return Expanded(
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: 60.w,
            child: Align(
              alignment: Alignment.center,
              child: FutureBuilder<List>(
                  future: getCountries(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return DropdownButtonFormField<String>(
                        isExpanded: true,
                        elevation: 10,
                        alignment: Alignment.center,
                        dropdownColor: Colors.white,
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide(
                              color: ColorApp.titleColorInRegisterPage,
                              width: 1,
                            ),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide(
                              color: ColorApp.titleColorInRegisterPage,
                              width: 1,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide(
                              color: ColorApp.titleColorInRegisterPage,
                              width: 1,
                            ),
                          ),
                        ),
                        value: currentCountry,
                        icon: const Icon(Icons.keyboard_arrow_down),
                        items: countriesName.map(
                          (String item) {
                            return DropdownMenuItem(
                              alignment: Alignment.centerRight,
                              value: item,
                              child: Text(
                                item,
                                // textAlign: TextAlign.right,

                                style: const TextStyle(
                                  color: Color.fromARGB(255, 0, 0, 0),
                                ),
                              ),
                            );
                          },
                        ).toList(),
                        onChanged: (String? newvalue) async {
                          Country country = countriesList.firstWhere(
                              (element) => element.name == newvalue.toString());
                          selectedCountryObj = country;

                          citiesName.clear();
                          if (country.states != null) {
                            citiesList.clear();
                            citiesList.addAll(country.states!);
                            country.states!.forEach((element) {
                              citiesName.add(element.name!);
                            });
                          }
                          currentCity = citiesName.first;
                          setState(() {
                            currentCountry = newvalue!;
                          });
                        },
                      );
                    }
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }),
            ),
          ),
        ],
      ),
    );
  }

  dropdowncity() {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: 60.w,
            // height: 30.h,
            child: Align(
              alignment: Alignment.center,
              child: DropdownButtonFormField<String>(
                isExpanded: true,
                borderRadius: BorderRadius.circular(20),
                dropdownColor: Colors.grey,
                // focusColor: Colors.black,
                elevation: 10,
                alignment: Alignment.center,
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(
                      color: ColorApp.titleColorInRegisterPage,
                      width: 1,
                    ),
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(
                      color: ColorApp.titleColorInRegisterPage,
                      width: 1,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(
                      color: ColorApp.titleColorInRegisterPage,
                      width: 1,
                    ),
                  ),
                ),
                value: currentCity,
                icon: const Icon(Icons.keyboard_arrow_down),
                items: citiesName.map(
                  (String item) {
                    return DropdownMenuItem(
                      alignment: Alignment.centerRight,
                      value: item,
                      child: FittedBox(
                        child: Text(
                          item,
                          // textAlign: TextAlign.right,
                          style: const TextStyle(
                            color: Color.fromARGB(255, 0, 0, 0),
                          ),
                        ),
                      ),
                    );
                  },
                ).toList(),
                onChanged: (String? newvalue) {
                  Cities city = citiesList.firstWhere(
                      (element) => element.name == newvalue.toString());
                  selectedCityObj = city;
                  setState(() {
                    currentCity = newvalue!;
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future pickImage() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      if (image == null) return;
      final imageTemp = File(image.path);
      final compressedImage = await compressImage(imageTemp);
      setState(() {
        logoImage = imageTemp;
        thereImage = true;
      });
    } on PlatformException catch (e) {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text(
            'warning',
            style: TextStyle(color: Colors.yellow),
          ),
          content: Text('Failed to pick image: $e'),
        ),
      );
    }
  }

  Future<File?> compressImage(File file) async {
    final filePath = file.absolute.path;
    final lastIndex = filePath.lastIndexOf(new RegExp(r'.png|.jp'));
    final splitted = filePath.substring(0, (lastIndex));
    final outPath = "${splitted}_out${filePath.substring(lastIndex)}";

    if (lastIndex == filePath.lastIndexOf(new RegExp(r'.png'))) {
      final compressedImage = await FlutterImageCompress.compressAndGetFile(
          filePath, outPath,
          minWidth: 1000,
          minHeight: 1000,
          quality: 50,
          format: CompressFormat.png);
      return compressedImage;
    } else {
      final compressedImage = await FlutterImageCompress.compressAndGetFile(
        filePath,
        outPath,
        minWidth: 1000,
        minHeight: 1000,
        quality: 50,
      );
      return compressedImage;
    }
  }

  Future addImageToJobFactory2() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      if (image == null) return;
      final imageTemp = File(image.path);
      final compressedImage = await compressImage(imageTemp);
      // Get.put(LabController).addNewImage(imageTemp);
      controllerLab.addNewImageToJobFactory(compressedImage);
      if (!controllerLab.addedNewPhotoJob) {
        showDialog(
          context: context,
          builder: (_) => AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            title: const Text(
              'تحذير',
              style: TextStyle(color: Colors.yellow),
            ),
            content: const Text('لا يمكن إضافة أكثر من ٢٠ صورة'),
          ),
        );
      }
    } on PlatformException catch (e) {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text(
            'warning',
            style: TextStyle(color: Colors.yellow),
          ),
          content: Text('Failed to pick image: $e'),
        ),
      );
    }
  }

  Future addImageToJobFactory() async {
    List<Asset> resultList = <Asset>[];

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 20,
        enableCamera: true,
        selectedAssets: controllerLab.jobsInImageAsset,
        materialOptions: MaterialOptions(
          actionBarTitle: "صور المعمل ",
          actionBarColor: ColorApp.primaryColorStr,
        ),
      );
    } on Exception catch (e) {
      print(e);
    }
    List<File> files = [];
    for (Asset asset in resultList) {
      File file = await getImageFileFromAssets(asset);
      files.add(file);
    }
    controllerLab.jobsInImageAsset = resultList;
    controllerLab.addNewImageToJobFactory(files);
  }

  Future<File> getImageFileFromAssets(Asset asset) async {
    final byteData = await asset.getByteData();

    final tempFile =
        File("${(await getTemporaryDirectory()).path}/${asset.name}");
    final file = await tempFile.writeAsBytes(
      byteData.buffer
          .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes),
    );

    return file;
  }

  Future addImageToFactory() async {
    List<Asset> resultList = <Asset>[];

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 5,
        enableCamera: true,
        selectedAssets: controllerLab.imagesAsset,
        materialOptions: MaterialOptions(
          actionBarTitle: "صور المعمل ",
          actionBarColor: ColorApp.primaryColorStr,
        ),
      );
    } on Exception catch (e) {
      print(e);
    }
    List<File> files = [];
    for (Asset asset in resultList) {
      File file = await getImageFileFromAssets(asset);
      files.add(file);
    }
    controllerLab.imagesAsset = resultList;
    controllerLab.addNewImageforFactory(files);
  }

  uploadProfilePhotoEvent() async {
    pickImage();
  }

  // addNewFactory() async {}
  Future<void> addNewFactory() async {
    var url = '${ServerAddresses.serverAddress}factory';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    List<http.MultipartFile> newList = <http.MultipartFile>[];
    Map<String, String> headers = {'token': Storage().token};
    request.fields['factory_name'] = textController[0].text;
    request.fields['email'] = textController[1].text;
    request.fields['mobile'] = textController[2].text;
    request.fields['manager_name'] = textController[3].text;
    request.fields['record_number'] = textController[4].text;
    request.fields['factory_address'] = textController[5].text;
    request.fields['worker_numbers'] = textController[6].text;

    int indexCountry =
        countriesList.indexWhere((element) => (currentCountry == element.name));
    int indexCity =
        citiesList.indexWhere((element) => (currentCity == element.name));
    request.fields['country_id'] = countriesList[indexCountry].id!;
    request.fields['city_id'] = citiesList[indexCity].id!;

    for (var i = 0; i < Get.find<ProductController>().products.length; i++) {
      print(i);
      request.fields["products_name[$i]"] =
          Get.find<ProductController>().products[i].productName!;
      request.fields['products_price[$i]'] =
          Get.find<ProductController>().products[i].productPrice!;

      print(Get.find<ProductController>().products[i].productName!);
      print(Get.find<ProductController>().products[i].productPrice!);
      print("--------------------------------");
    }
    print(Get.find<ProductController>().products.length);

    newList
        .add(await http.MultipartFile.fromPath('factoryLogo', logoImage!.path));
    for (int i = 0; i < controllerLab.imageForFactory.length; i++) {
      newList.add(await http.MultipartFile.fromPath(
          'factoryImage[]', controllerLab.imageForFactory[i].path));
    }
    for (int i = 0; i < controllerLab.jobsInFactoryImage.length; i++) {
      newList.add(await http.MultipartFile.fromPath(
          'jobImage[]', controllerLab.jobsInFactoryImage[i].path));
    }
    request.files.addAll(newList);
    var res = await request.send();

    var response = await http.Response.fromStream(res);
    debugPrint(response.toString());
    int x;
  }

  @override
  void initState() {
    for (var i = 0; i < 8; i++) {
      textController.add(TextEditingController());
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.only(top: 1.5.h),
                child: CustomScrollView(
                  slivers: [
                    SliverFillRemaining(
                      hasScrollBody: false,
                      child: Column(
                        children: [
                          SizedBox(height: 3.h),
                          lableText('اسم المعمل التجاري'),
                          SizedBox(height: 2.h),
                          textFieldInput('اسم المعمل', textController[0]),
                          SizedBox(height: 3.h),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                const Expanded(child: SizedBox()),
                                lableText('المدينة'),
                                const Expanded(child: SizedBox()),
                                lableText('الدولة'),
                              ],
                            ),
                          ),
                          SizedBox(height: 3.h),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(width: 2.w),
                              dropdowncity(),
                              SizedBox(width: 4.w),
                              dropdownCountry(),
                              SizedBox(width: 2.w),
                            ],
                          ),
                          SizedBox(height: 3.h),
                          for (int i = 0; i < 5; i++)
                            Column(
                              children: [
                                lableText(constantTitleText[i]),
                                SizedBox(height: 2.h),
                                textFieldInput(constantTitleText[i],
                                    textController[i + 1]),
                                SizedBox(height: 3.h),
                              ],
                            ),
                          // SizedBox(height: 1.h),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height: 20.h,
                                // decoration: BoxDecoration(
                                //   color: const Color(0xFFbdbcbc),
                                // ),
                                padding: EdgeInsets.only(left: 4.w),
                                child: Center(
                                  child: logoImage != null
                                      ? Image.file(
                                          logoImage!,
                                          fit: BoxFit.fill,
                                          width: 40.w,
                                        )
                                      : Image.asset(
                                          'assets/images/unknown_image.png',
                                          width: 40.w,
                                        ),
                                ),
                              ),
                              Expanded(child: Container()),
                              Padding(
                                padding: EdgeInsets.only(right: 3.w),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    const Text('شعار المعمل'),
                                    SizedBox(height: 3.h),
                                    Container(
                                      height: 7.h,
                                      width: 50.w,
                                      decoration: BoxDecoration(
                                        color: const Color(0xFFbdbcbc),
                                        borderRadius: BorderRadius.circular(12),
                                      ),
                                      child: TextButton(
                                        onPressed: uploadProfilePhotoEvent,
                                        child: const Text(
                                          'ارفع الصورة',
                                          style: TextStyle(
                                              color: Color(0xFF8c7884),
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          lableText('قائمة أسعار منتجات المعمل'),
                          SizedBox(height: 1.h),
                          Padding(
                              padding: EdgeInsets.symmetric(horizontal: 6.w),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: backgroundColor,
                                    borderRadius: BorderRadius.circular(12),
                                    border: Border.all(color: Colors.black)),
                                child: MaterialButton(
                                  child: Center(
                                    child: Text(
                                      'انقر لإضافة المنتجات',
                                      style: TextStyle(
                                          color: ColorApp.primaryColor),
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => ProductsPriceList(
                                                products: [],
                                                factoryId: "0",
                                                fromCreateNewLab: true)));
                                  },
                                ),
                              )),
                          SizedBox(height: 1.5.h),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              TextButton(
                                onPressed: () {
                                  showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (_) => GetBuilder<LabController>(
                                          init: LabController(),
                                          builder: (labController) {
                                            return AlertDialog(
                                              title: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  IconButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                    icon:
                                                        const Icon(Icons.close),
                                                  ),
                                                  const Text('معرض الصور'),
                                                ],
                                              ),
                                              titlePadding: EdgeInsets.only(
                                                bottom: 10,
                                                left: 3.w,
                                                right: 3.w,
                                              ),
                                              contentPadding:
                                                  const EdgeInsets.all(10.0),
                                              content: SizedBox(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    .8,
                                                child: GridView.builder(
                                                    gridDelegate:
                                                        const SliverGridDelegateWithMaxCrossAxisExtent(
                                                      maxCrossAxisExtent: 200,
                                                      childAspectRatio: 0.6,
                                                      crossAxisSpacing: 20,
                                                      mainAxisSpacing: 20,
                                                    ),
                                                    itemCount: labController
                                                        .jobsInImageAsset
                                                        .length,
                                                    itemBuilder:
                                                        (context, index) {
                                                      return GestureDetector(
                                                        onDoubleTap: () {
                                                          labController
                                                              .deleteImage(
                                                                  index);
                                                        },
                                                        child: AssetThumb(
                                                          asset: labController
                                                                  .jobsInImageAsset[
                                                              index],
                                                          width: 100,
                                                          height: 100,
                                                          quality: 75,
                                                        ),
                                                      );
                                                    }),
                                              ),
                                            );
                                          }));
                                },
                                child: Text(
                                  'معاينة',
                                  style: TextStyle(
                                    color: ColorApp.primaryColor,
                                    fontWeight: FontWeight.w600,
                                    decoration: TextDecoration.underline,
                                  ),
                                ),
                              ),
                              lableText('صور أعمال المعمل'),
                            ],
                          ),
                          SizedBox(height: 0.5.h),
                          Padding(
                            padding: EdgeInsets.only(right: 7.w),
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Text(
                                'يمكنك إضافة ٢٠  صورة على الأكثر',
                                style: TextStyle(
                                  color: ColorApp.primaryColor,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 1.h),
                          Padding(
                              padding: EdgeInsets.symmetric(horizontal: 6.w),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: backgroundColor,
                                    borderRadius: BorderRadius.circular(12),
                                    border: Border.all(color: Colors.black)),
                                child: MaterialButton(
                                  child: Center(
                                    child: Text(
                                      'انقر لرفع صور للمعمل',
                                      style: TextStyle(
                                          color: ColorApp.primaryColor),
                                    ),
                                  ),
                                  onPressed: () {
                                    addImageToJobFactory();
                                  },
                                ),
                              )),
                          SizedBox(height: 3.h),
                          lableText('عدد الكادر الفني'),
                          SizedBox(height: 1.h),
                          textFieldInput('عدد الكادر الفني', textController[6]),

                          SizedBox(height: 3.h),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              TextButton(
                                onPressed: () {
                                  showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (_) => GetBuilder<LabController>(
                                          init: LabController(),
                                          builder: (labController) {
                                            return AlertDialog(
                                              title: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  IconButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                    icon:
                                                        const Icon(Icons.close),
                                                  ),
                                                  const Text('معرض الصور'),
                                                ],
                                              ),
                                              titlePadding: EdgeInsets.only(
                                                bottom: 10,
                                                left: 3.w,
                                                right: 3.w,
                                              ),
                                              contentPadding:
                                                  const EdgeInsets.all(10.0),
                                              content: SizedBox(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    .8,
                                                child: GridView.builder(
                                                    gridDelegate:
                                                        const SliverGridDelegateWithMaxCrossAxisExtent(
                                                      maxCrossAxisExtent: 200,
                                                      childAspectRatio: 0.6,
                                                      crossAxisSpacing: 20,
                                                      mainAxisSpacing: 20,
                                                    ),
                                                    itemCount: labController
                                                        .imagesAsset.length,
                                                    itemBuilder:
                                                        (context, index) {
                                                      return GestureDetector(
                                                        onDoubleTap: () {
                                                          labController
                                                              .deleteImageFromFactory(
                                                                  index);
                                                        },
                                                        child: AssetThumb(
                                                          asset: labController
                                                                  .imagesAsset[
                                                              index],
                                                          width: 100,
                                                          height: 100,
                                                          quality: 75,
                                                        ),
                                                      );
                                                    }),
                                              ),
                                            );
                                          }));
                                },
                                child: Text(
                                  'معاينة',
                                  style: TextStyle(
                                    color: ColorApp.primaryColor,
                                    fontWeight: FontWeight.w600,
                                    decoration: TextDecoration.underline,
                                  ),
                                ),
                              ),
                              lableText('صور معمل الأسنان'),
                            ],
                          ),

                          SizedBox(height: 0.5.h),
                          Padding(
                            padding: EdgeInsets.only(right: 7.w),
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Text(
                                'يمكنك إضافة ٥  صورة على الأكثر',
                                style: TextStyle(
                                  color: ColorApp.primaryColor,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 1.h),
                          Padding(
                              padding: EdgeInsets.symmetric(horizontal: 6.w),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: backgroundColor,
                                    borderRadius: BorderRadius.circular(12),
                                    border: Border.all(color: Colors.black)),
                                child: MaterialButton(
                                  child: Center(
                                    child: Text(
                                      'انقر لرفع صورة',
                                      style: TextStyle(
                                          color: ColorApp.primaryColor),
                                    ),
                                  ),
                                  onPressed: () {
                                    addImageToFactory();
                                  },
                                ),
                              )),
                          SizedBox(height: 3.h),
                          Container(
                            // margin: EdgeInsets.only(top: 4.h),
                            width: 88.w,
                            decoration: BoxDecoration(
                                color: const Color(0xFFe6e6e6),
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                    color: const Color.fromARGB(
                                        255, 201, 198, 198))),
                            child: MaterialButton(
                              onPressed: () {
                                // Navigator.push(
                                //     context,
                                //     MaterialPageRoute(
                                //         builder: (_) => FactoryProfile(
                                //               factory: lab,
                                //             )));
                              },
                              child: Text(
                                'google map انقر هنا للانتقال إلى ال',
                                style: TextStyle(
                                    color: ColorApp.primaryColor,
                                    fontSize: 4.w),
                              ),
                            ),
                          ),
                          SizedBox(height: 3.h),
                          Container(
                            decoration: BoxDecoration(
                              color: const Color(0xFF907785),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            height: 7.h,
                            width: 37.w,
                            child: LoadingBtn(
                              height: 50,
                              borderRadius: 8,
                              animate: true,
                              color: ColorApp.primaryColor,
                              width: MediaQuery.of(context).size.width * 0.45,
                              loader: Container(
                                padding: const EdgeInsets.all(10),
                                child: const Center(
                                  child: SpinKitDoubleBounce(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              child: const Text(
                                'تسجيل المعمل',
                                style: TextStyle(color: Colors.white),
                              ),
                              onTap:
                                  (startLoading, stopLoading, btnState) async {
                                if (btnState == ButtonState.idle) {
                                  startLoading();
                                  // call your network api
                                  await addNewFactory();
                                  Fluttertoast.showToast(
                                      msg:
                                          'تم إضافة المعمل بنجاح.... بانتظار موافقة الإدارة');
                                  Future.delayed(Duration(milliseconds: 800),
                                      () {
                                    Navigator.of(context).pop();
                                  });
                                }
                              },
                            ),
                          ),
                          SizedBox(height: 2.h),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            top: 5.h,
            child: Text(
              'المعامل',
              style: TextStyle(color: Colors.white, fontSize: 5.w),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 88.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
