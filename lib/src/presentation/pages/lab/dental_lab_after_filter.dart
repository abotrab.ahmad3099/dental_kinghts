import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '/src/data/models/factory.dart';
import '/src/data/repositories/repository.dart';
import '../../../constants/color_app.dart';
import '../../widgets/drawer_doctor.dart';
import 'factory_profile.dart';

class DentalLabAfterFilter extends StatefulWidget {
  final String? cityId;
  final String? countryId;

  const DentalLabAfterFilter({Key? key, this.cityId, this.countryId})
      : super(key: key);

  @override
  State<DentalLabAfterFilter> createState() => _DentalLabAfterFilterState();
}

class _DentalLabAfterFilterState extends State<DentalLabAfterFilter> {
  final Repository repo = Repository();

  late GlobalKey<ScaffoldState> scaffoldState;
  TextEditingController search = TextEditingController();
  List<T_Factory> factories = [];
  List<T_Factory> suggestions = [];

  @override
  void initState() {
    factories = [];
    suggestions = [];
    scaffoldState = GlobalKey<ScaffoldState>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: SafeArea(
        child: Container(
          decoration: decorationOuterContainar(),
          child: Stack(
            alignment: Alignment.topCenter,
            children: [
              Positioned(
                top: 2.h,
                child: DefaultTextStyle(
                  style: const TextStyle(color: Colors.white54),
                  child: Text(
                    'المعامل',
                    style: TextStyle(fontSize: 5.w),
                  ),
                ),
              ),
              Positioned(
                top: 1.h,
                left: 1.w,
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                ),
              ),

              Positioned(
                top: 1.h,
                left: 75.w,
                child: IconButton(
                  onPressed: () {
                    if(factories.isNotEmpty && suggestions.isNotEmpty) {
                      showSearch(
                          context: context,
                          delegate: MySearchDelegate(factories, suggestions));
                    }
                  },
                  icon: const Icon(
                    Icons.search,
                    color: Colors.white,
                  ),
                ),
              ),
              Positioned(
                top: 1.h,
                left: 87.w,
                child: IconButton(
                  onPressed: () {
                    scaffoldState.currentState!.openDrawer();
                  },
                  icon: const Icon(
                    Icons.menu,
                    color: Colors.white,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 9.h),
                child: Container(
                  width: double.infinity,
                  decoration: decorationInnerContainer(),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 2.h,
                      ),
                      DefaultTextStyle(
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 7.w,
                          color: ColorApp.imageOrDetialsColor,
                        ),
                        child: const Text('المعامل الموجودة في منطقتك'),
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      Expanded(
                        child: FutureBuilder<List<T_Factory>>(
                          future: repo.getFactories(
                            widget.countryId!,
                            widget.cityId!,
                          ),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              factories.addAll(snapshot.data!);
                              suggestions.addAll(factories);
                              return ListView.separated(
                                itemCount: snapshot.data!.length,
                                separatorBuilder:
                                    (BuildContext context, int index) =>
                                        SizedBox(
                                  height: 3.h,
                                ),
                                itemBuilder: (BuildContext context, int index) {
                                  return DentalLabCard(
                                    lab: snapshot.data![index],
                                  );
                                },
                              );
                            }
                            return const Center(
                                child: CircularProgressIndicator());
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  BoxDecoration decorationInnerContainer() {
    return const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(50),
        topRight: Radius.circular(50),
      ),
      boxShadow: [
        BoxShadow(
          color: Color.fromARGB(255, 0, 0, 0),
          offset: Offset(
            5.0,
            5.0,
          ),
          blurRadius: 20.0,
          spreadRadius: 2.0,
        ), //BoxShadow
        BoxShadow(
          color: Colors.white,
          offset: Offset(0.0, 0.0),
          blurRadius: 0.0,
          spreadRadius: 0.0,
        ), //BoxShadow
      ],
    );
  }

  BoxDecoration decorationOuterContainar() {
    return const BoxDecoration(
      image: DecorationImage(
        fit: BoxFit.fill,
        image: AssetImage('assets/images/background.png'),
      ),
    );
  }
}

class MySearchDelegate extends SearchDelegate<T_Factory> {
  List<T_Factory> factories = [];
  List<T_Factory> suggestions = [];

  MySearchDelegate(this.factories, this.suggestions);

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(onPressed: () {}, icon: const Icon(Icons.clear));
  }

  @override
  Widget buildResults(BuildContext context) {
    final list = factories
        .where((factory) =>
            factory.factoryName!.toLowerCase().contains(query.toLowerCase()))
        .toList();
    print('search delegertds fdsfmsdljfsdklfjs');
    return ListView.separated(
      itemCount: list.length,
      itemBuilder: (context, index) {
        return DentalLabCard(
          lab: list[index],
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 2.h,
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final list = suggestions
        .where((factory) =>
            factory.factoryName!.toLowerCase().contains(query.toLowerCase()))
        .toList();
    print('suggestion');
    print(query);
    print(suggestions.length);
    return ListView.separated(
      itemCount: list.length,
      itemBuilder: (context, index) {
        return DentalLabCard(
          lab: list[index],
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 2.h,
        );
      },
    );
  }
}

class DentalLabCard extends StatelessWidget {
  const DentalLabCard({Key? key, required this.lab}) : super(key: key);
  final T_Factory lab;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: <Widget>[
        Container(
          height: 25.h,
          width: 90.w,
          margin: EdgeInsets.all(6.w),
          child: Card(
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
            color: const Color.fromRGBO(242, 242, 242, 300),
            child: Container(),
          ),
        ),
        Container(
          padding: EdgeInsets.only(top: 10.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 3.h),
              Text(
                lab.factoryName!,
                style: TextStyle(fontSize: 4.w),
              ),
              // SizedBox(height: 6.h),
              Container(
                margin: EdgeInsets.only(top: 4.h),
                width: 30.w,
                decoration: BoxDecoration(
                    color: ColorApp.imageOrDetialsColor,
                    borderRadius: BorderRadius.circular(20)),
                child: MaterialButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => FactoryProfile(
                                  factory_: lab,
                                )));
                  },
                  child: const Text(
                    'التفاصيل',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          width: 20.w,
          height: 10.h,
          decoration: const ShapeDecoration(
            shape: CircleBorder(),
            color: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.all(1),
            child: DecoratedBox(
              position: DecorationPosition.background,
              decoration: ShapeDecoration(
                color: ColorApp.imageOrDetialsColor,
                shape: const CircleBorder(),
                image: const DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(
                    'assets/images/doctor.png',
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
