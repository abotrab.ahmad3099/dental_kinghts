import 'dart:ui';

import 'package:dental_kinghts/src/presentation/pages/lab/products_price_list.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:url_launcher/url_launcher.dart';

import '/src/data/models/factory.dart';
import '/src/presentation/pages/lab/picture_lab.dart';
import '../../../constants/color_app.dart';
import '../../widgets/drawer_doctor.dart';
import 'jobs_pic_lab.dart';

// ignore: must_be_immutable
class FactoryProfile extends StatefulWidget {
  FactoryProfile({Key? key, required this.factory_}) : super(key: key);
  T_Factory factory_ = T_Factory();

  @override
  State<FactoryProfile> createState() => _FactoryProfileState();
}

class _FactoryProfileState extends State<FactoryProfile> {
  bool acceptToAll = false;
  bool faceCovering = false;
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();

  whereIsWork(staticTitle, content) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 2.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          DefaultTextStyle(
            style: const TextStyle(
                color: Colors.grey, fontWeight: FontWeight.bold),
            child: Text(
              staticTitle,
            ),
          ),
          SizedBox(
            height: 1.h,
          ),
          DefaultTextStyle(
            style: const TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold),
            child: Text(content),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: CustomScrollView(
                slivers: [
                  SliverFillRemaining(
                    hasScrollBody: false,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 10.h,
                        ),
                        Text(
                          widget.factory_.factoryName!,
                          style: TextStyle(
                            color: ColorApp.primaryColor,
                            fontSize: 6.w,
                          ),
                        ),
                        SizedBox(height: 4.h),
                        Padding(
                          padding: EdgeInsets.only(left: 55.w),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                'مدير المعمل',
                                style: TextStyle(
                                    fontSize: 5.w, fontWeight: FontWeight.w600),
                              ),
                              SizedBox(height: 1.h),
                              Padding(
                                padding: EdgeInsets.only(right: 2.w),
                                child: Text(
                                  widget.factory_.managerName != null
                                      ? '${widget.factory_.managerName}.د'
                                      : "no name",
                                  style: TextStyle(
                                    fontSize: 5.w,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 3.h),
                        Container(
                          // margin: EdgeInsets.only(top: 4.h),
                          width: 80.w,
                          // padding: EdgeInsets.symmetric(horizontal: 2.w),
                          decoration: BoxDecoration(
                              color: ColorApp.imageOrDetialsColor,
                              borderRadius: BorderRadius.circular(20)),
                          child: MaterialButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => ProductsPriceList(
                                            products: widget.factory_.products!,
                                            factoryId:
                                                widget.factory_.id ?? "0",
                                            fromCreateNewLab: false,
                                          )));
                            },
                            child: Text(
                              'قائمة أسعار المعمل',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 4.w),
                            ),
                          ),
                        ),
                        SizedBox(height: 2.h),
                        Container(
                          // margin: EdgeInsets.only(top: 4.h),
                          width: 80.w,
                          // padding: EdgeInsets.symmetric(horizontal: 2.w),
                          decoration: BoxDecoration(
                              color: ColorApp.imageOrDetialsColor,
                              borderRadius: BorderRadius.circular(20)),
                          child: MaterialButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => JobsPictureLab(
                                            factoryWorkImages: widget
                                                .factory_.factoryWorkImages!,
                                          )));
                            },
                            child: Text(
                              'صور أعمال معمل الأسنان',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 4.w),
                            ),
                          ),
                        ),
                        SizedBox(height: 2.h),
                        Container(
                          // margin: EdgeInsets.only(top: 4.h),
                          width: 80.w,
                          // padding: EdgeInsets.symmetric(horizontal: 2.w),
                          decoration: BoxDecoration(
                              color: ColorApp.imageOrDetialsColor,
                              borderRadius: BorderRadius.circular(20)),
                          child: MaterialButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => PictureLab(
                                            factoryImages:
                                                widget.factory_.factoryImages!,
                                          )));
                            },
                            child: Text(
                              'صور معمل الأسنان',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 4.w),
                            ),
                          ),
                        ),
                        SizedBox(height: 2.h),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 2.w),
                            child: const Text(
                              'معلومات التواصل',
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                        const Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            whereIsWork(
                              'المدينة التي يعمل بها',
                              widget.factory_.city ?? "city",
                            ),
                            Expanded(child: Container()),
                            whereIsWork(
                              'الدولة التي يعمل بها',
                              widget.factory_.country ?? "country",
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 3.w),
                              child: CircleAvatar(
                                backgroundColor: ColorApp.primaryColor,
                                radius: 3.w,
                                child: Icon(
                                  Icons.location_on,
                                  color: Colors.white,
                                  size: 4.w,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            GestureDetector(
                              onTap: () =>
                                  launch('https://${widget.factory_.mobile}'),
                              child: whereIsWork(
                                '\t\t\t\t\t\t\t\t\t\t\t\t\tرقم التواصل',
                                "${widget.factory_.mobile}",
                              ),
                            ),
                            Expanded(child: Container()),
                            whereIsWork(
                              'عنوان المعمل',
                              widget.factory_.factoryAddress,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 3.w),
                              child: CircleAvatar(
                                backgroundColor: ColorApp.primaryColor,
                                radius: 3.w,
                                child: Icon(
                                  Icons.phone,
                                  color: Colors.white,
                                  size: 4.w,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 3.h),
                        Container(
                          // margin: EdgeInsets.only(top: 4.h),
                          width: 95.w,
                          decoration: BoxDecoration(
                              color: const Color(0xFFe6e6e6),
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                  color: const Color.fromARGB(
                                      255, 201, 198, 198))),
                          child: MaterialButton(
                            onPressed: () {
                              // Navigator.push(
                              //     context,
                              //     MaterialPageRoute(
                              //         builder: (_) => FactoryProfile(
                              //               factory: lab,
                              //             )));
                            },
                            child: Text(
                              'google map انقر هنا للانتقال إلى ال',
                              style: TextStyle(
                                  color: ColorApp.primaryColor, fontSize: 4.w),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 4.5.h,
            left: 40.w,
            child: CircleAvatar(
              backgroundColor: Colors.white10,
              radius: 11.w,
              child: Image.asset('assets/images/factory.png'),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 88.w,
            child: IconButton(
              onPressed: () {
                // Navigator.pushAndRemoveUntil(
                //     context,
                //     MaterialPageRoute(builder: (_) => const HomePageDoctor()),
                //     (route) => false);
              },
              icon: const Icon(
                Icons.home_outlined,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
