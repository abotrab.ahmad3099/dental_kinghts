import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sizer/sizer.dart';

import '/src/constants/server_addresses.dart';
import '/src/data/models/city.dart';
import '/src/data/models/country.dart';
import '../../../constants/color_app.dart';
import '../../widgets/drawer_doctor.dart';
import 'create_dental_lab.dart';
import 'dental_lab_after_filter.dart';

class DentalLab extends StatefulWidget {
  const DentalLab({Key? key}) : super(key: key);

  @override
  State<DentalLab> createState() => _DentalLabState();
}

class _DentalLabState extends State<DentalLab> {
  String currentCountry = 'اختر البلد';
  String currentCity = 'اختر مدينة';
  List<String> countriesName = ['اختر البلد'];
  List<String> citiesName = ['اختر مدينة'];
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  Color backgroundColor = const Color.fromARGB(255, 245, 244, 244);
  List<Country> countriesList = [];

  Future<List> getCountries() async {
    if (countriesName.length == 1) {
      final data = await http
          .get(Uri.parse("${ServerAddresses.serverAddress}countries"));
      var responseBody = data.body;
      final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

      countriesList =
          parsed.map<Country>((json) => Country.fromJson(json)).toList();

      for (int i = 0; i < countriesList.length; i++) {
        countriesName.add(countriesList[i].name ?? 'اختر البلد');
      }
    }

    return countriesName;
  }

  List<Cities> citiesList = [];

  Future<List<String>> getStates(String country_id) async {
    citiesName = ['اختر مدينة'];
    if (citiesName.length == 1) {
      final data = await http.get(Uri.parse(
          "${ServerAddresses.serverAddress}cities?country_id=$country_id"));
      var responseBody = data.body;
      final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

      citiesList = parsed.map<Cities>((json) => Cities.fromJson(json)).toList();

      for (int i = 0; i < citiesList.length; i++) {
        citiesName.add(citiesList[i].name ?? 'المدينة');
      }
    }

    return citiesName;
  }

  Country? _selectedCountryObj;
  Cities? _selectedCityObj;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 3.h,
                  ),
                  Text(
                    'ابحث عن معامل الأسنان في منطقتك',
                    textAlign: TextAlign.center,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(fontSize: 5.w),
                  ),
                  SizedBox(height: 0.5.h),
                  DefaultTextStyle(
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: 7.w,
                      color: Colors.white,
                    ),
                    child: const Text(
                      'اختر الدولة',
                    ),
                  ),
                  SizedBox(height: 0.5.h),
                  dropdownCountry(),
                  SizedBox(height: 0.5.h),
                  DefaultTextStyle(
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: 7.w,
                      color: Colors.white,
                    ),
                    child: const Text(
                      'اختر المدينة',
                    ),
                  ),
                  SizedBox(height: 0.5.h),
                  dropdowncity(),
                  SizedBox(height: 4.h),
                  // make filter
                  Container(
                    width: 40.w,
                    height: 7.h,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: const Color(0xFF907785),
                    ),
                    child: MaterialButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => DentalLabAfterFilter(
                                      cityId: _selectedCityObj?.id ?? '1',
                                      countryId: _selectedCountryObj?.id ?? '',
                                    )));
                      },
                      child: const Text(
                        'موافق',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),

                  Expanded(child: Container()),
                  //add new lab
                  Container(
                    width: 50.w,
                    height: 7.h,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: const Color(0xFF907785),
                    ),
                    child: MaterialButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => const CreateDentalLab()));
                      },
                      child: const Text(
                        'تسجيل معمل جديد +',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),

                  SizedBox(height: 3.h)
                ],
              ),
            ),
          ),
          Positioned(
            top: 5.h,
            child: Text(
              'المعامل',
              style: TextStyle(color: Colors.white, fontSize: 5.w),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 88.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Column dropdownCountry() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          width: 60.w,
          child: Align(
            alignment: Alignment.center,
            child: FutureBuilder<List>(
                future: getCountries(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return DropdownButtonFormField<String>(
                      borderRadius: BorderRadius.circular(20),
                      elevation: 10,
                      alignment: Alignment.center,
                      dropdownColor: Colors.white,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color: ColorApp.titleColorInRegisterPage,
                            width: 1,
                          ),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color: ColorApp.titleColorInRegisterPage,
                            width: 1,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(
                            color: ColorApp.titleColorInRegisterPage,
                            width: 1,
                          ),
                        ),
                      ),
                      isExpanded: true,
                      value: currentCountry,
                      icon: const Icon(Icons.keyboard_arrow_down),
                      items: countriesName.map(
                        (String item) {
                          return DropdownMenuItem(
                            alignment: Alignment.centerRight,
                            value: item,
                            child: Padding(
                              padding: EdgeInsets.only(right: 4.w),
                              child: Text(
                                item,
                                textAlign: TextAlign.right,
                                style: const TextStyle(
                                  color: Color.fromARGB(255, 0, 0, 0),
                                ),
                              ),
                            ),
                          );
                        },
                      ).toList(),
                      onChanged: (String? newvalue) async {
                        Country country = countriesList.firstWhere(
                            (element) => element.name == newvalue.toString());
                        _selectedCountryObj = country;

                        citiesName.clear();
                        if (country.states != null) {
                          citiesList.clear();
                          citiesList.addAll(country.states!);
                          country.states!.forEach((element) {
                            citiesName.add(element.name!);
                          });
                        }
                        currentCity = citiesName.first;
                        setState(() {
                          currentCountry = newvalue!;
                        });
                      },
                    );
                  }
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }),
          ),
        ),
      ],
    );
  }

  Column dropdowncity() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          width: 60.w,
          // height: 30.h,
          child: Align(
            alignment: Alignment.center,
            child: DropdownButtonFormField<String>(
              borderRadius: BorderRadius.circular(20),
              dropdownColor: Colors.white,
              // focusColor: Colors.black,
              elevation: 10,
              alignment: Alignment.center,
              decoration: InputDecoration(
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: BorderSide(
                    color: ColorApp.titleColorInRegisterPage,
                    width: 1,
                  ),
                ),
                disabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: BorderSide(
                    color: ColorApp.titleColorInRegisterPage,
                    width: 1,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: BorderSide(
                    color: ColorApp.titleColorInRegisterPage,
                    width: 1,
                  ),
                ),
              ),
              value: currentCity,
              icon: const Icon(Icons.keyboard_arrow_down),
              isExpanded: true,
              items: citiesName.map(
                (String item) {
                  return DropdownMenuItem(
                    alignment: Alignment.centerRight,
                    value: item,
                    child: Padding(
                      padding: EdgeInsets.only(right: 4.w),
                      child: Text(
                        item,
                        textAlign: TextAlign.right,
                        style: const TextStyle(
                          color: Color.fromARGB(255, 0, 0, 0),
                        ),
                      ),
                    ),
                  );
                },
              ).toList(),
              onChanged: (String? newvalue) {
                Cities city = citiesList.firstWhere(
                    (element) => element.name == newvalue.toString());
                _selectedCityObj = city;
                setState(() {
                  currentCity = newvalue!;
                });
              },
            ),
          ),
        ),
      ],
    );
  }
}
