import 'package:dental_kinghts/src/constants/color_app.dart';
import 'package:dental_kinghts/src/controllers/product_controller.dart';
import 'package:dental_kinghts/src/data/models/factory.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../widgets/drawer_doctor.dart';

// ignore: must_be_immutable
class ProductsPriceList extends StatefulWidget {
  ProductsPriceList({
    Key? key,
    required this.products,
    required this.fromCreateNewLab,
    required this.factoryId,
  }) : super(key: key);
  List<Products> products = [];
  final bool fromCreateNewLab;
  final String factoryId;

  @override
  State<ProductsPriceList> createState() => _ProductsPriceListState();
}

class _ProductsPriceListState extends State<ProductsPriceList> {
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  double radiusTable = 20.0;
  Color backgroundColor = const Color.fromARGB(255, 245, 244, 244);

  TextEditingController nameProduct = TextEditingController();
  TextEditingController priceProduct = TextEditingController();

  dataRow(List<Products> list) {
    List<DataRow> productsDataRow = [];
    for (Products product in list) {
      productsDataRow.add(
        DataRow(
          cells: [
            DataCell(Center(child: Text("us ${product.productPrice} "))),
            DataCell(Center(child: Text(product.productName!))),
          ],
        ),
      );
    }
    return productsDataRow;
  }

  textFieldInput(text, controller, {type}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 3.w),
      child: TextField(
        keyboardType: type ?? TextInputType.text,
        controller: controller,
        textAlign: TextAlign.right,
        decoration: InputDecoration(
          fillColor: backgroundColor,
          filled: true,
          focusColor: backgroundColor,
          hoverColor: backgroundColor,
          hintText: text,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: const BorderSide(
              color: Colors.black,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: const BorderSide(
              color: Colors.black,
            ),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: const BorderSide(
              color: Colors.black,
            ),
          ),
        ),
      ),
    );
  }

  lableText(text) {
    return Padding(
      padding: EdgeInsets.only(right: 7.w),
      child: Align(
        alignment: Alignment.centerRight,
        child: Text(
          text,
          style: TextStyle(
            fontSize: 5.w,
            color: ColorApp.titleColorInRegisterPage,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
            ),
          ),
          Positioned(
            top: 25.h,
            left: 20.w,
            child: DataTable(
              border: TableBorder.all(),
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(12)),
              columns: const [
                DataColumn(label: Text('سعر المنتج')),
                DataColumn(label: Text('اسم المنتج')),
              ],
              rows: dataRow(widget.products),
            ),
          ),
          Positioned(
            top: 13.h,
            left: 25.w,
            child: Text(
              'قائمة أسعار منتجات المعمل',
              style: TextStyle(fontSize: 5.w, color: ColorApp.primaryColor),
            ),
          ),
          widget.fromCreateNewLab
              ? Positioned(
                  top: 16.h,
                  left: 3.w,
                  child: TextButton(
                    child: Text(
                      'إضافة منتج',
                      style: TextStyle(
                          fontSize: 4.w,
                          fontWeight: FontWeight.w500,
                          color: ColorApp.primaryColor),
                    ),
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (_) => AlertDialog(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              contentPadding: EdgeInsets.only(bottom: 3.h),
                              content: SingleChildScrollView(
                                child: Form(
                                  child: Column(
                                    children: [
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: IconButton(
                                            onPressed: () =>
                                                Navigator.pop(context),
                                            icon: const Icon(
                                              Icons.close,
                                              color: Colors.black45,
                                            )),
                                      ),
                                      lableText('اسم المنتج'),
                                      SizedBox(height: 1.5.h),
                                      textFieldInput('اسم المنتج', nameProduct),
                                      SizedBox(height: 3.h),
                                      lableText('سعر المنتج'),
                                      SizedBox(height: 1.5.h),
                                      textFieldInput(
                                          'سعر المنتج', priceProduct,type:TextInputType.text),
                                      SizedBox(height: 3.h),
                                      Container(
                                        width: 50.w,
                                        height: 8.h,
                                        decoration: BoxDecoration(
                                          color: const Color(0xFF8c7884),
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                        child: TextButton(
                                          onPressed: () {
                                            Get.put(ProductController())
                                                .addNewProduct(Products(
                                              factoryId: widget.factoryId,
                                              productName: nameProduct.text,
                                              productPrice: priceProduct.text,
                                            ));
                                            setState(() {
                                              nameProduct.text = '';
                                              priceProduct.text = '';
                                              widget.products = Get.find<ProductController>().products;
                                            });
                                            Navigator.pop(context);
                                          },
                                          child: const Text(
                                            'إضافة منتج',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )));
                      // Navigator.pop(context);
                    },
                  ),
                )
              : const SizedBox.shrink(),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 85.w,
            child: IconButton(
              onPressed: () {
                // Navigator.pushAndRemoveUntil(
                //     context,
                //     MaterialPageRoute(builder: (_) => const HomePageDoctor()),
                //     (route) => false);
              },
              icon: const Icon(
                Icons.home_outlined,
                color: Colors.white,
              ),
            ),
          ),
          // Positioned(
          //   top: 23.h,
          //   child: Container(
          //     decoration: BoxDecoration(
          //       color: ColorApp.primaryColor,
          //       borderRadius: BorderRadius.only(
          //         topLeft: Radius.circular(radiusTable),
          //         topRight: Radius.circular(radiusTable),
          //       ),
          //     ),
          //     width: 56.w,
          //     height: 6.6.h,
          //     child: Row(
          //       mainAxisAlignment: MainAxisAlignment.spaceAround,
          //       children: const [
          //         Text(
          //           'سعر المنتج',
          //           style: TextStyle(color: Colors.white),
          //         ),
          //         Text(
          //           'اسم المنتج',
          //           style: TextStyle(color: Colors.white),
          //         ),
          //       ],
          //     ),
          //     // color: ColorApp.redColor,
          //   ),
          // ),
        ],
      ),
    );
  }
}
