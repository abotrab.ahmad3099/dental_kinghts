import 'dart:convert';

import 'package:dental_kinghts/src/constants/server_addresses.dart';
import 'package:dental_kinghts/src/data/models/city.dart';
import 'package:dental_kinghts/src/data/models/country.dart';
import 'package:dental_kinghts/src/data/models/spec.dart';

import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:http/http.dart' as http;

import '../../constants/color_app.dart';
import '../../constants/constant_strings.dart';
import '../../local_data.dart';
import '../widgets/create_post_container.dart';
import '../widgets/drawer_doctor.dart';

class CreatePost extends StatefulWidget {
  final List<Specialization> spec;
  const CreatePost({Key? key, required this.spec}) : super(key: key);

  @override
  State<CreatePost> createState() => _CreatePostState();
}

class _CreatePostState extends State<CreatePost> {
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // currentCountry = ConstantStrings.countriesName.keys.toList()[0];

    // countriesName = ConstantStrings.countriesName.keys.toList();
    // citiesName = ConstantStrings.countriesName[currentCountry]!;
    // currentCity = citiesName[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                        margin: const EdgeInsets.only(top: 30),
                        child: CreatePostContainer(spec: widget.spec))
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            top: 4.5.h,
            child: Text(
              'إنشاء مقالة',
              style: TextStyle(color: Colors.white, fontSize: 5.w),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
