import '/src/presentation/pages/book_scientific.dart';
import '/src/presentation/pages/image_scintific.dart';
import '/src/presentation/pages/video_scientific.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../local_data.dart';
import '../widgets/drawer_doctor.dart';

class Education extends StatefulWidget {
  const Education({Key? key}) : super(key: key);

  @override
  State<Education> createState() => _EducationState();
}

class _EducationState extends State<Education> {
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();

  Container buttonScientfic(text, function) {
    return Container(
      width: 80.w,
      height: 9.h,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: const Color(0xFF907785),
      ),
      child: MaterialButton(
        onPressed: function,
        child: Text(
          text,
          style: TextStyle(color: Colors.white, fontSize: 5.w),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 3.h,
                  ),
                  buttonScientfic('كتب علمية', () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => const BookScientific()));
                  }),
                  SizedBox(
                    height: 2.h,
                  ),
                  buttonScientfic('فيديوهات علمية', () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => const VideoScientfic()));
                  }),
                  SizedBox(
                    height: 2.h,
                  ),
                  buttonScientfic('صور علمية', () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => const ImageScientific()));
                  }),
                ],
              ),
            ),
          ),
          Positioned(
            top: 4.5.h,
            child: Text(
              'التعليم',
              style: TextStyle(color: Colors.white, fontSize: 5.w),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
