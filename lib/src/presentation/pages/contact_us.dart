import 'package:dental_kinghts/src/presentation/widgets/custome_contact_us.dart';
import 'package:flutter/material.dart';
import '../../constants/color_app.dart';

class Contactus extends StatelessWidget {
  const Contactus({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // ignore: prefer_const_literals_to_create_immutables

      theme: ThemeData(
          // primarySwatch: Colors.blue,
          ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
              title: const Text('اتصل بنا'),
              backgroundColor: ColorApp.primaryColor,
              actions: <Widget>[
                Row(children: <Widget>[
                  IconButton(
                    icon: const Icon(Icons.logout),
                    onPressed: () {
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(builder: (context) => Register()),
                      // );
                    },
                  ),
                ])
              ]),
          bottomNavigationBar: TextButton(
            style: TextButton.styleFrom(
              backgroundColor: Colors.white,
              onSurface: Colors.grey,
              shadowColor: Colors.white,
            ),
            onPressed: null,
            child: const Text(
              'Designed and Developed by AM-Team',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: 15,
              ),
            ),
          ),
          backgroundColor: const Color.fromARGB(255, 255, 255, 255),
          body: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                CustomeContactUs(
                    cardColor: Colors.white,
                    textColor: ColorApp.blueColor,
                    dividerColor: ColorApp.primaryColor,
                    logo: const AssetImage(
                      'assets/images/person.png',
                    ),
                    email: 'adoshi26.ad@gmail.com',
                    companyName: 'تطبيق فرسان الأسنان',
                    companyColor: ColorApp.blueColor,
                    dividerThickness: 2,
                    phoneNumber: '+917818044311',
                    website: 'https://abhishekdoshi.godaddysites.com',
                    // githubUserName: 'AbhishekDoshi26',
                    // linkedinURL:
                    //     'https://www.linkedin.com/in/abhishek-doshi-520983199/',
                    tagLine: 'فرسان الاسنان',
                    taglineColor: ColorApp.blueColor,
                    // twitterHandle: 'AbhishekDoshi26',
                    instagram: '_abhishek_doshi',
                    facebookHandle: '_abhishek_doshi'),
              ],
            ),
          )),
    );
  }
}
