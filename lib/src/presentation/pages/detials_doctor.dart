import '/src/constants/color_app.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../data/models/doctor_model.dart';
import 'package:sizer/sizer.dart';

import '../widgets/image_profile_doctor.dart';

class DetialsDoctor extends StatelessWidget {
  const DetialsDoctor({Key? key, required this.doctor}) : super(key: key);

  final Doctor? doctor;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: const BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage('assets/images/background.png'),
        ),
      ),
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Padding(
            padding: EdgeInsets.only(top: 20.h),
            child: Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(40),
                  topLeft: Radius.circular(40),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 13.h),
            child: Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(40),
                  topLeft: Radius.circular(40),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 17.h),
            child: DefaultTextStyle(
              style:
                  TextStyle(color: ColorApp.imageOrDetialsColor, fontSize: 8.w),
              child: Text(
                ' الدكتور ${doctor!.name!}',
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 24.h),
            child: DefaultTextStyle(
              style:
                  TextStyle(color: ColorApp.imageOrDetialsColor, fontSize: 7.w),
              child: Text(
                'مجمع ${doctor!.medicalComplex!}',
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 30.h, right: 4.w),
              child: DefaultTextStyle(
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 5.w,
                    fontWeight: FontWeight.bold),
                child: const Text(
                  'معلومات الخبرة',
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 36.h, right: 2.w),
              child: CircleAvatar(
                radius: 15,
                backgroundColor: ColorApp.imageOrDetialsColor,
                foregroundColor: ColorApp.imageOrDetialsColor,
                child: SizedBox(
                  height: 5.w,
                  width: 5.w,
                  child: Image.asset(
                    'assets/images/paper.png',
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 35.h, right: 5.w),
              child: const SizedBox(
                width: 5,
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 35.h, right: 15.w),
              child: DefaultTextStyle(
                style: TextStyle(
                    fontSize: 4.w,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold),
                child: const Text(
                  'التخصص',
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 38.h, right: 15.w),
              child: DefaultTextStyle(
                style: TextStyle(
                    fontSize: 4.w,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
                child: Text(
                  doctor!.specialization!,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 46.h, right: 2.w),
              child: CircleAvatar(
                radius: 15,
                backgroundColor: ColorApp.imageOrDetialsColor,
                foregroundColor: ColorApp.imageOrDetialsColor,
                child: SizedBox(
                  height: 5.w,
                  width: 5.w,
                  child: Image.asset(
                    'assets/images/pencil.png',
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 45.h, right: 5.w),
              child: const SizedBox(
                width: 5,
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 45.h, right: 14.w),
              child: DefaultTextStyle(
                style: TextStyle(
                    fontSize: 4.w,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold),
                child: const Text(
                  'معلومات أخرى',
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 48.h, right: 15.w),
              child: DefaultTextStyle(
                textAlign: TextAlign.end,
                style: TextStyle(
                    fontSize: 4.w,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
                child: Text(
                  doctor!.info!,
                  textAlign: TextAlign.end,
                  // textDirection: TextDirection.rtl,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 55.h, right: 8.w),
              child: DefaultTextStyle(
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 5.w,
                    fontWeight: FontWeight.bold),
                child: const Text(
                  'معلومات التواصل',
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 60.h, right: 3.w),
              child: DefaultTextStyle(
                style: TextStyle(
                    fontSize: 4.w,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold),
                child: const Text(
                  'الدولة التي يعمل بها',
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 64.h, right: 3.w),
              child: DefaultTextStyle(
                style: TextStyle(
                    fontSize: 4.w,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
                child: Text(
                  doctor!.country!,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 60.h, right: 60.w),
              child: DefaultTextStyle(
                style: TextStyle(
                    fontSize: 4.w,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold),
                child: const Text(
                  'المدينة التي يعمل بها',
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 64.h, right: 60.w),
              child: DefaultTextStyle(
                style: TextStyle(
                    fontSize: 4.w,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
                child: Text(
                  doctor!.city!,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 70.h, right: 3.w),
              child: DefaultTextStyle(
                style: TextStyle(
                    fontSize: 4.w,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold),
                child: const Text(
                  'اسم المجمع أو العيادة',
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 74.h, right: 3.w),
              child: DefaultTextStyle(
                style: TextStyle(
                    fontSize: 4.w,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
                child: Text(
                  'مجمع ${doctor!.medicalComplex!}',
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 70.h, right: 60.w),
              child: DefaultTextStyle(
                style: TextStyle(
                    fontSize: 4.w,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold),
                child: const Text(
                  'عنوان المجمع أو العيادة',
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 74.h, right: 60.w),
              child: DefaultTextStyle(
                style: TextStyle(
                    fontSize: 4.w,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
                child: Text(
                  doctor!.adressOfMedicalComplext!,
                  textAlign: TextAlign.end,
                  textDirection: TextDirection.rtl,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Padding(
              padding: EdgeInsets.only(top: 80.h, right: 3.w, left: 3.w),
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: const Color(0xFFe6e6e6)),
                child: MaterialButton(
                  onPressed: () {},
                  child: Text(
                    'انقر هنا للانتقال إلى ال google map',
                    textAlign: TextAlign.justify,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(color: ColorApp.imageOrDetialsColor),
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 90.h, right: 6.w),
              child: DefaultTextStyle(
                style: TextStyle(
                    fontSize: 4.w,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold),
                child: const Text(
                  'رقم التواصل',
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 94.h, right: 4.w),
              child: TextButton(
                onPressed: () async {
                  var number = 'tel://${doctor!.phone!}';

                  await launch(number);
                },
                child: DefaultTextStyle(
                  style: TextStyle(
                      fontSize: 4.w,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                  child: Text(
                    doctor!.phone!,
                  ),
                ),
              ),
            ),
          ),
          const ImageProfileDoctor()
        ],
      ),
    );
  }
}
