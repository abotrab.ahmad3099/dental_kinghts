import 'dart:async';
import 'package:dental_kinghts/src/authentication/authentication_bloc.dart';
import 'package:dental_kinghts/src/constants/storage.dart';
import 'package:dental_kinghts/src/data/repositories/repository.dart';
import 'package:dental_kinghts/src/presentation/pages/home_page_doctor.dart';

import '/src/constants/color_app.dart';
import '/src/presentation/pages/navigator_to_login_home.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class SplachScreen extends StatefulWidget {
  const SplachScreen({Key? key}) : super(key: key);

  @override
  State<SplachScreen> createState() => _SplachScreenState();
}

class _SplachScreenState extends State<SplachScreen> {
  Repository repo = Repository();
  checkIfAuthenticated() async {
    await AuthenticationBloc.getuserdata();
    if (Storage().token != '') Storage().doctor = await repo.my_profile();
    return true;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    var route =
        MaterialPageRoute(builder: (context) => const NavigatorToLoginOrHome());
    var route2 =
        MaterialPageRoute(builder: (context) => const HomePageDoctor());
    Duration duration = const Duration(seconds: 5);
    Timer(duration, () {
      checkIfAuthenticated().then((success) {
        if (Storage().token != '') {
          Navigator.pushAndRemoveUntil(context, route2, (route2) => false);
        } else {
          Navigator.pushAndRemoveUntil(context, route, (route) => false);
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.white,
      child: Stack(
        alignment: Alignment.center,
        // crossAxisAlignment: CrossAxisAlignment.center,
        // mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.5,
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: const BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(
                  'assets/images/logo2.jpeg',
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 40.h),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.w),
              child: LinearProgressIndicator(
                color: ColorApp.blueColor,
              ),
            ),
          )
        ],
      ),
    );
  }
}
