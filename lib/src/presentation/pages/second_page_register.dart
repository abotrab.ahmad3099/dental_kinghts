import 'package:dental_kinghts/src/data/repositories/repository.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../data/models/question.dart';
import '/src/presentation/pages/complete_register.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../constants/color_app.dart';

class SecondPageRegister extends StatefulWidget {
  final List<bool> values;
  final List<String?> selectedQuestions;
  SecondPageRegister(
      {Key? key, required this.values, required this.selectedQuestions})
      : super(key: key);

  @override
  State<SecondPageRegister> createState() => _SecondPageRegisterState();
}

class _SecondPageRegisterState extends State<SecondPageRegister> {
  List<bool> trueValues = [];
  List<bool> falseValues = [];

  @override
  void initState() {
    super.initState();
  }

  Form formMethod() {
    return Form(
      child: Container(
        width: double.infinity,
        decoration: decorationInnerContainer(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 3.h,
            ),
            DefaultTextStyle(
              textAlign: TextAlign.center,
              maxLines: 3,
              style: TextStyle(
                fontSize: 5.w,
                color: Colors.black,
              ),
              child: const Text(
                'يشرٌفنا انضمامك ضمن منصة فرسان الأسنان وحتى تتمكن من الاستمرار في التسجيل نرجوا الإجابة على الأسئلة التالية',
                maxLines: 3,
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            SizedBox(
              height: 3.h,
            ),
            Padding(
              padding: EdgeInsets.only(left: 5.w),
              child: Row(
                children: [
                  const Text('لا'),
                  SizedBox(
                    width: 13.w,
                  ),
                  const Text('نعم'),
                ],
              ),
            ),
            questionsList(),
            SizedBox(
              height: 2.h,
            ),
            Expanded(child: Container()),
            Container(
              width: 35.w,
              height: 7.h,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                gradient: LinearGradient(
                  colors: [ColorApp.primaryColor, ColorApp.blueColor],
                ),
              ),
              child: MaterialButton(
                onPressed: () {
                  if (userAnswers.length != questionsListVar.length) {
                    Fluttertoast.showToast(
                        msg: 'يرجى الإجابة على جميع الأسئلة',
                        toastLength: Toast.LENGTH_LONG);
                  } else {
                    bool isCorrect = true;
                    questionsListVar.forEach((element) {
                      if (element.answer != userAnswers[element.id]) {
                        isCorrect = false;
                      }
                    });
                    if (isCorrect) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => const CompleteRegister()));
                    } else {
                      Fluttertoast.showToast(
                          msg:
                              'عذرا لايمكننا إتمام عملية التسجيل حتى يتم الإجابة بشكل صحيح على كل الأسئلة',
                          toastLength: Toast.LENGTH_LONG);
                    }
                  }
                },
                child: const Text(
                  'التالي',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 3.h,
            ),
          ],
        ),
      ),
    );
  }

  List<String> questions = [];
  List<Question> questionsListVar = [];
  final Repository repo = Repository();
  Map<String, String> userAnswers = {};
  questionsList() {
    return Flexible(
        flex: 3,
        child: FutureBuilder<List<Question>>(
            future: repo.getQuestions(
                'yes_no_question',
                widget.selectedQuestions
                    .where((element) => element != '')
                    .join(',')),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                questionsListVar.clear();
                if (trueValues.isEmpty)
                  trueValues = snapshot.data!.map((e) => false).toList();
                if (falseValues.isEmpty)
                  falseValues = snapshot.data!.map((e) => false).toList();
                return ListView.builder(
                  itemCount: snapshot.data!.length,
                  itemBuilder: (context, index) {
                    questionsListVar.add(snapshot.data![index]);
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Checkbox(
                                  value: falseValues[index],
                                  focusColor: Colors.black,
                                  checkColor: Colors.black,
                                  activeColor:
                                      const Color.fromARGB(255, 224, 223, 223),
                                  splashRadius: 2,
                                  onChanged: (value) {
                                    setState(() {
                                      falseValues[index] = value!;
                                      userAnswers[snapshot.data![index].id!] =
                                          "false";
                                    });
                                  },
                                ),
                                SizedBox(
                                  width: 3.w,
                                ),
                                Checkbox(
                                  value: trueValues[index],
                                  focusColor: Colors.black,
                                  checkColor: Colors.black,
                                  activeColor:
                                      const Color.fromARGB(255, 224, 223, 223),
                                  splashRadius: 2,
                                  onChanged: (value) {
                                    setState(() {
                                      trueValues[index] = value!;
                                      userAnswers[snapshot.data![index].id!] =
                                          "true";
                                    });
                                  },
                                ),
                              ],
                            ),
                            Container(
                              alignment: Alignment.topRight,
                              margin: EdgeInsets.only(right: 3.w),
                              child: Row(
                                textDirection: TextDirection.rtl,
                                children: [
                                  Text('- ${index + 1}'),
                                  SizedBox(
                                    width: 5.w,
                                  ),
                                  Text(snapshot.data![index].text!),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 1.w),
                        const Divider()
                      ],
                    );
                  },
                );
              }
              return const Center(child: CircularProgressIndicator());
            }));
  }

  BoxDecoration decorationInnerContainer() {
    return const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(50),
        topRight: Radius.circular(50),
      ),
      boxShadow: [
        BoxShadow(
          color: Color.fromARGB(255, 0, 0, 0),
          offset: Offset(
            5.0,
            5.0,
          ),
          blurRadius: 20.0,
          spreadRadius: 2.0,
        ), //BoxShadow
        BoxShadow(
          color: Colors.white,
          offset: Offset(0.0, 0.0),
          blurRadius: 0.0,
          spreadRadius: 0.0,
        ), //BoxShadow
      ],
    );
  }

  BoxDecoration decorationOuterContainar() {
    return const BoxDecoration(
      image: DecorationImage(
        fit: BoxFit.fill,
        image: AssetImage('assets/images/background.png'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: decorationOuterContainar(),
          child: Stack(
            alignment: Alignment.topCenter,
            children: [
              Positioned(
                top: 3.h,
                right: 40.w,
                child: SizedBox(
                  height: 15.h,
                  child: Image.asset(
                    'assets/images/logo.png',
                    color: Colors.white60,
                  ),
                ),
              ),
              Positioned(
                right: 90.w,
                child: SizedBox(
                  height: 15.h,
                  child: IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: const Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.h),
                child: formMethod(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
