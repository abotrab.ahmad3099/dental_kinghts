import 'dart:convert';

import 'package:dental_kinghts/src/authentication/authentication_bloc.dart';
import 'package:dental_kinghts/src/constants/server_addresses.dart';
import 'package:dental_kinghts/src/constants/storage.dart';
import 'package:dental_kinghts/src/data/repositories/repository.dart';

import '/src/constants/doctor_api.dart';
import '/src/constants/text_const.dart';
import '/src/local_data.dart';
import '/src/presentation/pages/home_page_doctor.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import '../../constants/color_app.dart';
import '../widgets/lable_data.dart';
import '../widgets/text_form_field_data.dart';
import 'register_page.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController email = TextEditingController();
  final formKey = GlobalKey<FormState>();
  TextEditingController password = TextEditingController();
  Repository repo = Repository();
  bool isLoading = false;
  bool visiablePassword = false;

  Future<bool> _authUser() async {
    // getLoc();
    /*print(
        'Name: ${emailController.text}, Password: ${passwordController.text}');*/
    //var firebasetoken = Storage().firebase_token;
    var input = <String, String>{
      'email': email.text.toString(),
      'password': password.text.toString(),
      // 'firebase_token': firebasetoken
    };
    var url = '${ServerAddresses.serverAddress2}login';

    var response = await http.post(Uri.parse(url), body: input, headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    });
    Map jsonResponse = json.decode(response.body);
    if (response.statusCode != 200) {
      Fluttertoast.showToast(
          msg: jsonResponse['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      isLoading = false;
      setState(() {});
      return false;
    } else {
      AuthenticationBloc.login(
          jsonResponse['token'],
          jsonResponse['name'],
          jsonResponse['mobile'],
          jsonResponse['image'] ?? '',
          jsonResponse['email'],
          '');
      Storage().doctor = await repo.my_profile();
      return true;
    }
  }

  formMethod() {
    return FocusTraversalGroup(
      child: Form(
        key: formKey,
        child: Container(
          width: double.infinity,
          decoration: decorationInnerContainer(),
          child: CustomScrollView(
            slivers: [
              SliverFillRemaining(
                hasScrollBody: false,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 2.h,
                    ),
                    DefaultTextStyle(
                      style: TextStyle(
                        fontSize: 15.w,
                        color: ColorApp.primaryColor,
                      ),
                      child: const Text(
                        '! أهلاً بك',
                      ),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    DefaultTextStyle(
                      style: TextStyle(
                        fontSize: 4.w,
                        color: ColorApp.primaryColor,
                      ),
                      child: const Text('الرجاء تسجيل الدخول إلى حسابك'),
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    const LableTextField(
                      textLable: 'بريدك الالكتروني',
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    TextFormFieldData(
                      controller: email,
                      hintText: 'بريدك الالكتروني',
                      validationText: TextConst.validationEmail,
                      icon: IconButton(
                        onPressed: () {},
                        icon: const Icon(Icons.email_outlined),
                      ),
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    const LableTextField(
                      textLable: 'كلمة المرور',
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    TextFormFieldData(
                      controller: password,
                      hintText: 'كلمة المرور',
                      validationText: TextConst.validationEmail,
                      showPassword: !visiablePassword,
                      icon: IconButton(
                        onPressed: () {
                          setState(() {
                            visiablePassword = !visiablePassword;
                          });
                        },
                        icon: visiablePassword == true
                            ? const Icon(Icons.visibility)
                            : const Icon(Icons.visibility_off),
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 2.w),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: TextButton(
                          onPressed: onPressedForgetPassword,
                          child: Text(
                            'هل نسيت كلمة المرور',
                            style: TextStyle(
                              color: ColorApp.primaryColor,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7.h,
                    ),
                    isLoading
                        ? const CircularProgressIndicator()
                        : Container(
                            width: 40.w,
                            height: 6.h,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              gradient: LinearGradient(
                                colors: [
                                  ColorApp.primaryColor,
                                  ColorApp.blueColor
                                ],
                              ),
                            ),
                            child: MaterialButton(
                              onPressed: onPressedLoginButton,
                              child: const Text(
                                'تسجيل دخول',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                    Expanded(child: Container()),
                    Row(
                      children: [
                        Expanded(
                          child: TextButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => const RegisterPage(),
                                ),
                              );
                            },
                            child: FittedBox(
                              child: Text(
                                'تسجيل حساب جديد',
                                style: TextStyle(
                                  color: ColorApp.blueColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 3.w),
                          child: const FittedBox(
                            fit: BoxFit.cover,
                            child: Text(
                              'لا تملك حساب في تطبيق فرسان الأسنان؟',
                              style: TextStyle(),
                            ),
                          ),
                        ),
                        SizedBox(width: 4.w),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  onPressedLoginButton() async {
    isLoading = true;
    setState(() {});
    bool res = await _authUser();
    if (res) {
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => const HomePageDoctor()));
    }
  }

  onPressedForgetPassword() {}

  BoxDecoration decorationInnerContainer() {
    return const BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(50),
        topRight: Radius.circular(50),
      ),
      boxShadow: [
        BoxShadow(
          color: Color.fromARGB(255, 0, 0, 0),
          offset: Offset(
            5.0,
            5.0,
          ),
          blurRadius: 20.0,
          spreadRadius: 2.0,
        ), //BoxShadow
        BoxShadow(
          color: Colors.white,
          offset: Offset(0.0, 0.0),
          blurRadius: 0.0,
          spreadRadius: 0.0,
        ), //BoxShadow
      ],
    );
  }

  BoxDecoration decorationOuterContainar() {
    return const BoxDecoration(
      image: DecorationImage(
        fit: BoxFit.fill,
        image: AssetImage('assets/images/background.png'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: decorationOuterContainar(),
          child: Stack(
            children: [
              Positioned(
                top: 2.h,
                right: 45.w,
                child: SizedBox(
                  height: 10.h,
                  child: Image.asset(
                    'assets/images/logo.png',
                    color: const Color.fromARGB(255, 178, 178, 178),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 13.h),
                child: formMethod(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
