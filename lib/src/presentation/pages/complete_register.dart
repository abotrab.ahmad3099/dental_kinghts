import 'dart:io';
import 'package:dental_kinghts/src/data/models/country.dart';
import 'package:dental_kinghts/src/presentation/custome_snak_bar.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_picker/image_picker.dart';

import '../../constants/server_addresses.dart';
import '../../constants/static_data.dart';
import '../../data/models/city.dart';
import '../../data/models/doctor_model.dart';
import '../../local_data.dart';
import '/src/constants/color_app.dart';
import '/src/constants/constant_strings.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import '../widgets/lable_data.dart';
import '../widgets/text_form_field_data.dart';
import 'home_page_doctor.dart';

class CompleteRegister extends StatefulWidget {
  const CompleteRegister({Key? key}) : super(key: key);

  @override
  State<CompleteRegister> createState() => _CompleteRegisterState();
}

class _CompleteRegisterState extends State<CompleteRegister> {
  TextEditingController address = TextEditingController();
  TextEditingController addressOfMedicalComplext = TextEditingController();
  List<Cities> citiesList = [];
  Set<String> citiesName = {'الرياض'};
  bool visiablePassword = true;
  List<Country> countriesList = [];
  Set<String> countriesName = {'السعودية'};
  String currentCity = 'الرياض';
  String currentCountry = 'السعودية';
  String? currentCountryWork;
  TextEditingController email = TextEditingController();
  TextEditingController flagToWork = TextEditingController();
  final formKey = GlobalKey<FormState>();
  List<String> gender = ['ذكر', 'أنثى'];
  String genderValue = '';
  File? image;
  TextEditingController medicalComplex = TextEditingController();
  TextEditingController mobile = TextEditingController();
  // List<TextEditingController> textEditingControllers = [];
  TextEditingController name = TextEditingController();

  String nationaliyValue = 'syrian';
  TextEditingController password = TextEditingController();
  bool showThePhoneNumberToEveryBody = false;
  String specializationValue = '';
  bool? thereImage = false;
  String yearsOfExperience = '';

  Cities? _selectedCityObj;
  Country? _selectedCountryObj;

  @override
  void initState() {
    getSpecilizations();
    getCountries();
    specializationValue = ConstantStrings.specialization.keys.toList()[0];
    yearsOfExperience = '0';
    currentCountryWork = 'السعودية';
    currentCity = 'الرياض';
    super.initState();
  }

  void getSpecilizations() async {
    final data = await http
        .get(Uri.parse("${ServerAddresses.serverAddress}specilzations"));
    var responseBody = data.body;
    final parsed = json.decode(data.body).cast<Map<String, dynamic>>();
    for (var element in parsed) {
      ConstantStrings.specialization[element['spec_name']] = element['id'];
    }
    setState(() {});
  }

  Future<List> getCountries() async {
    if (countriesName.length == 1) {
      final data = await http
          .get(Uri.parse("${ServerAddresses.serverAddress}countries"));
      var responseBody = data.body;
      print(responseBody);
      final parsed = json.decode(data.body).cast<Map<String, dynamic>>();

      countriesList =
          parsed.map<Country>((json) => Country.fromJson(json)).toList();

      for (int i = 0; i < countriesList.length; i++) {
        countriesName.add(countriesList[i].name ?? 'اختر البلد');
      }
    }
    _selectedCountryObj =
        countriesList.firstWhere(((element) => element.name == 'السعودية'));
    setState(() {});
    return countriesName.toList();
  }

  Future<Set<String>> getStates(String country_id) async {
    if (citiesName.length == 1) {
      final data = await http.get(Uri.parse(
          "${ServerAddresses.serverAddress}cities?country_id=$country_id"));
      var responseBody = data.body;
      print(responseBody);
      final parsed = json.decode(data.body).cast<Map<String, dynamic>>();

      citiesList = parsed.map<Cities>((json) => Cities.fromJson(json)).toList();

      for (int i = 0; i < citiesList.length; i++) {
        citiesName.add(citiesList[i].name ?? 'المدينة');
      }
    }
    setState(() {});
    return citiesName;
  }

  formMethod() {
    return FocusTraversalGroup(
      child: Form(
        key: formKey,
        child: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  nameEmailPassword(),
                  SizedBox(
                    height: 2.h,
                  ),
                  genderDetected(),
                  SizedBox(
                    height: 2.h,
                  ),
                  dropDownChoices(
                      'الجنسية', ConstantStrings.nationality, nationaliyValue,
                      (String? value) {
                    setState(() {
                      nationaliyValue = value!;
                    });
                  }),
                  SizedBox(
                    height: 2.h,
                  ),
                  numberPhone(),
                  SizedBox(
                    height: 2.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 20.h,
                        padding: EdgeInsets.only(left: 4.w),
                        child: !thereImage!
                            ? Center(
                                child: Image.asset(
                                    'assets/images/person_unavailable.png'))
                            : Center(
                                child: Image.file(
                                  image!,
                                  fit: BoxFit.fill,
                                ),
                              ),
                      ),
                      const Spacer(),
                      uploadDoctorPhotoWidget(),
                      const Spacer(),
                    ],
                  ),
                  SizedBox(
                    height: 3.h,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 18.w),
                    child: dropDownChoices(
                        'التخصص',
                        ConstantStrings.specialization.keys.toList(),
                        specializationValue, (String? value) {
                      setState(() {
                        specializationValue = value!;
                      });
                    }),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 38.w),
                    child: dropDownChoices(
                      'سنوات الخبرة',
                      ConstantStrings().yearOfExperince(),
                      yearsOfExperience,
                      (String? value) {
                        setState(() {
                          yearsOfExperience = value!;
                        });
                      },
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  dropDownChoices(
                    'دولة العمل الحالي',
                    countriesName.toList(),
                    currentCountryWork,
                    (String? newvalue) {
                      Country country = countriesList.firstWhere(
                          (element) => element.name == newvalue.toString());
                      _selectedCountryObj = country;
                      citiesName.clear();
                      if (country.states != null) {
                        citiesList.clear();
                        citiesList.addAll(country.states!);
                        for (var element in country.states!) {
                          citiesName.add(element.name!);
                        }
                      }
                      currentCity = citiesName.first;
                      setState(() {
                        currentCountry = newvalue!;
                      });
                    },
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  dropDownChoices(
                    'المدينة',
                    citiesName.toList(),
                    currentCity,
                    (String? newvalue) {
                      Cities cities = citiesList.firstWhere(
                          (element) => element.name == newvalue.toString());
                      _selectedCityObj = cities;
                      citiesName.clear();

                      currentCity = citiesName.first;
                      setState(() {
                        currentCity = newvalue!;
                      });
                    },
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Column(
                    children: [
                      const LableTextField(
                        textLable: 'مكان العمل الحالي',
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      TextFormFieldData(
                        keyboardType: TextInputType.text,
                        controller: address,
                        hintText: 'مكان العمل الحالي',
                        validationText: 'مكان العمل الحالي',
                        icon: IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.work_outline),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Column(
                    children: [
                      const LableTextField(
                        textLable: 'معلم قريب من مكان العمل',
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      TextFormFieldData(
                        keyboardType: TextInputType.text,
                        controller: addressOfMedicalComplext,
                        hintText: 'معلم قريب من مكان العمل',
                        validationText: 'معلم قريب من مكان العمل',
                        icon: IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.workspaces_outlined),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Column(
                    children: [
                      const LableTextField(
                        textLable: 'اسم المكان',
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      TextFormFieldData(
                        keyboardType: TextInputType.text,
                        controller: medicalComplex,
                        hintText: 'اسم المكان',
                        validationText: 'اسم المكان',
                        icon: IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.workspaces_outlined),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Container(
                    width: 35.w,
                    height: 7.h,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      gradient: LinearGradient(
                        colors: [ColorApp.primaryColor, ColorApp.blueColor],
                      ),
                    ),
                    child: MaterialButton(
                      onPressed: registerProcess,
                      child: const Text(
                        'التالي',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Expanded(child: Container()),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void registerProcess() async {
    // todo register api
    var url = '${ServerAddresses.serverAddress2}register';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    List<http.MultipartFile> newList = <http.MultipartFile>[];

    if (name.text.trim().isEmpty) {
      showCustomSnackBar('enter your first name', context);
    } else if (email.text.trim().isEmpty) {
      showCustomSnackBar('enter_email_address', context);
    } else if (mobile.text.trim().isEmpty) {
      showCustomSnackBar('enter phone number', context);
    } else if (password.text.trim().length < 6) {
      showCustomSnackBar('pleas enter zip sold code', context);
    } else if (_selectedCountryObj == null) {
      showCustomSnackBar('pleas choice your country', context);
    } else if (_selectedCityObj == null) {
      showCustomSnackBar('pleas choice your city', context);
    } else {
      request.fields['name'] = name.text;
      request.fields['email'] = email.text;
      request.fields['password'] = password.text;
      request.fields['mobile'] = mobile.text;
      request.fields['address'] = address.text;
      request.fields['medicalComplex'] = medicalComplex.text;
      request.fields['adressOfMedicalComplext'] = addressOfMedicalComplext.text;

      request.fields['gender'] = genderValue;
      request.fields['yearExperience'] = yearsOfExperience;

      request.fields['country_id'] = _selectedCountryObj!.id!;
      request.fields['city_id'] = _selectedCityObj!.id!;

      String d = ConstantStrings.specialization[specializationValue]!;
      request.fields['specialization_id'] =
          ConstantStrings.specialization[specializationValue]!;
      if (image != null) {
        newList.add(await http.MultipartFile.fromPath('image', image!.path));
        request.files.addAll(newList);
      }

      var res = await request.send();
      var response = await http.Response.fromStream(res);
      debugPrint(response.body);
      if (response.statusCode == 200) {
        LocalData.doctor = Doctor(name: name.text);
        // ignore: use_build_context_synchronously
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (_) => const HomePageDoctor()),
            (route) => false);
      } else {
        // ignore: use_build_context_synchronously
        showCustomSnackBar("Could not make register", context);
      }
    }
  }

  Container uploadDoctorPhotoWidget() {
    return Container(
      height: 5.h,
      decoration: BoxDecoration(
        color: ColorApp.titleColorInRegisterPage,
        borderRadius: BorderRadius.circular(10),
      ),
      child: MaterialButton(
          onPressed: imagePickreMethodToUploadPhoto,
          child: const Text(
            'إدراج صورة شخصية',
            style: TextStyle(color: Colors.black),
          )),
    );
  }

  Column numberPhone() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        const LableTextField(
          textLable: 'رقم التواصل',
        ),
        SizedBox(
          height: 2.h,
        ),
        TextFormFieldData(
          keyboardType: TextInputType.phone,
          controller: mobile,
          hintText: 'رقم التواصل',
          validationText: 'رقم التواصل',
          icon: IconButton(
            onPressed: () {},
            icon: const Icon(Icons.phone_outlined),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.only(right: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const Text('هل تود إظهار الرقم الخاص بك للعامة'),
              Checkbox(
                value: showThePhoneNumberToEveryBody,
                onChanged: (value) {
                  setState(() {
                    showThePhoneNumberToEveryBody = value!;
                  });
                },
                activeColor: ColorApp.primaryColor,
              )
            ],
          ),
        )
      ],
    );
  }

  Column dropDownChoices(
    String title,
    List<String> list,
    valueChoice,
    Function(String? value) onChange,
  ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 32.0),
          child: Text(
            title,
            style: TextStyle(
                fontSize: 5.w, color: ColorApp.titleColorInRegisterPage),
          ),
        ),
        DropdownButtonFormField<String>(
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: const BorderSide(
                color: Colors.white,
                width: 1,
              ),
            ),
            disabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: const BorderSide(
                color: Colors.white,
                width: 1,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: const BorderSide(
                color: Colors.white,
                width: 1,
              ),
            ),
            prefixIcon: Padding(
              padding: const EdgeInsets.only(right: 45.0),
              child: Align(
                alignment: Alignment.centerRight,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      valueChoice,
                      textAlign: TextAlign.right,
                      style: const TextStyle(
                        color: Color.fromARGB(255, 81, 81, 81),
                        fontSize: 17,
                      ),
                    ),
                    const SizedBox(width: 10),
                    const Icon(
                      Icons.keyboard_arrow_down,
                      size: 17,
                    ),
                  ],
                ),
              ),
            ),
          ),
          value: valueChoice,
          icon: const Icon(
            Icons.keyboard_arrow_down,
            color: Colors.white,
          ),
          items: list.map(
            (String item) {
              return DropdownMenuItem(
                alignment: Alignment.topRight,
                value: item,
                child: Text(
                  item,
                  textAlign: TextAlign.right,
                  style: const TextStyle(
                    color: Color.fromARGB(255, 130, 130, 130),
                  ),
                ),
              );
            },
          ).toList(),
          onChanged: onChange,
        ),
      ],
    );
  }

  Column nameEmailPassword() {
    return Column(
      children: [
        const LableTextField(
          textLable: 'الاسم الثلاثي',
        ),
        SizedBox(
          height: 2.h,
        ),
        TextFormFieldData(
          controller: name,
          hintText: 'الاسم الثلاثي',
          validationText: 'الاسم الثلاثي',
          icon: IconButton(
            onPressed: () {},
            icon: const Icon(Icons.person_outline),
          ),
        ),
        SizedBox(
          height: 3.h,
        ),
        const LableTextField(
          textLable: 'بريدك الالكتروني',
        ),
        SizedBox(
          height: 2.h,
        ),
        TextFormFieldData(
          controller: email,
          hintText: 'بريدك الالكتروني',
          validationText: 'بريدك الالكتروني',
          icon: IconButton(
            onPressed: () {},
            icon: const Icon(Icons.email_outlined),
          ),
        ),
        SizedBox(
          height: 3.h,
        ),
        const LableTextField(
          textLable: 'كلمة المرور',
        ),
        SizedBox(
          height: 2.h,
        ),
        TextFormFieldData(
          controller: password,
          hintText: 'كلمة المرور',
          validationText: 'كلمة المرور',
          showPassword: !visiablePassword,
          icon: IconButton(
            onPressed: () {
              setState(() {
                visiablePassword = !visiablePassword;
              });
            },
            icon: visiablePassword == true
                ? const Icon(Icons.visibility)
                : const Icon(Icons.visibility_off),
          ),
        ),
      ],
    );
  }

  Row genderDetected() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        SizedBox(
          width: 10.w,
        ),
        Expanded(
          child: ListTile(
            minLeadingWidth: 2,
            dense: true,
            title: Align(
              alignment: const Alignment(-2, 0),
              child: Radio<String>(
                focusColor: Colors.amber,
                activeColor: Colors.black,
                value: gender[0],
                groupValue: genderValue,
                onChanged: (value) {
                  setState(
                    () {
                      genderValue = value!;
                    },
                  );
                },
              ),
            ),
            leading: const Text('ذكر'),
          ),
        ),
        Expanded(
          child: ListTile(
            minLeadingWidth: 2,
            leading: const Text('أنثى'),
            title: Align(
              alignment: const Alignment(-1.2, 0),
              child: Radio<String>(
                focusColor: Colors.amber,
                activeColor: Colors.black,
                value: gender[1],
                groupValue: genderValue,
                onChanged: (value) {
                  setState(
                    () {
                      genderValue = value!;
                    },
                  );
                },
              ),
            ),
          ),
        ),
        Text(
          ':الجنس',
          style: TextStyle(color: const Color(0xFFB6B3B3), fontSize: 5.w),
        ),
        SizedBox(
          width: 8.w,
        )
      ],
    );
  }

  // onPressedRegisterButton() {}

  Future<File?> compressImage(File file) async {
    final filePath = file.absolute.path;
    final lastIndex = filePath.lastIndexOf(RegExp(r'.png|.jp'));
    final splitted = filePath.substring(0, (lastIndex));
    final outPath = "${splitted}_out${filePath.substring(lastIndex)}";

    if (lastIndex == filePath.lastIndexOf(RegExp(r'.png'))) {
      final compressedImage = await FlutterImageCompress.compressAndGetFile(
          filePath, outPath,
          minWidth: 1000,
          minHeight: 1000,
          quality: 50,
          format: CompressFormat.png);
      return compressedImage;
    } else {
      final compressedImage = await FlutterImageCompress.compressAndGetFile(
        filePath,
        outPath,
        minWidth: 1000,
        minHeight: 1000,
        quality: 50,
      );
      return compressedImage;
    }
  }

  Future pickImage() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      if (image == null) return;
      final imageTemp = File(image.path);
      StaticData.personalData!.image = await compressImage(imageTemp);
      setState(() {
        this.image = imageTemp;
        thereImage = true;
      });
    } on PlatformException catch (e) {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text(
            'warning',
            style: TextStyle(color: Colors.yellow),
          ),
          content: Text('Failed to pick image: $e'),
        ),
      );
    }
  }

  imagePickreMethodToUploadPhoto() async {
    await pickImage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: const BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage('assets/images/background.png'),
            ),
          ),
          child: Stack(
            alignment: Alignment.topCenter,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 5.h),
                child: DefaultTextStyle(
                  style: TextStyle(color: Colors.white, fontSize: 8.w),
                  child: const Text('إنشاء حساب'),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.h),
                child: Container(
                  height: double.infinity,
                  width: double.infinity,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.h),
                child: formMethod(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
