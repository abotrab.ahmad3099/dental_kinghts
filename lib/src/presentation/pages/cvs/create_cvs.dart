import 'package:dental_kinghts/src/data/models/country.dart';

import '../../../constants/server_addresses.dart';
import '../../../data/models/city.dart';
import '/src/presentation/widgets/personal_information.dart';
import '/src/presentation/widgets/view.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import '../../../constants/constant_strings.dart';
import '../../../local_data.dart';
import '../../widgets/drawer_doctor.dart';
import '../../widgets/experince.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class CreateCVs extends StatefulWidget {
  const CreateCVs({Key? key}) : super(key: key);

  @override
  State<CreateCVs> createState() => _CreateCVsState();
}

class _CreateCVsState extends State<CreateCVs> {
  Color backgroundColor = const Color.fromARGB(255, 206, 205, 205);
  String currentCity = '';
  String? currentCountryWork;
  bool extraInformation = false;
  final formKey = GlobalKey<FormState>();
  List<String> gender = ['ذكر', 'أنثى'];
  String genderValue = '';
  List<String> hintTexts = [
    'الاسم الأول',
    'الاسم الأخير (اسم العائلة)',
    'البريد الالكتروني',
    'رقم الهاتف',
    'عنوان الإقامة',
    'المدينة',
    'مكان الميلاد',
    'تاريخ الميلاد',
    'الجنس',
    'الحالة المدنية',
    'linkedIn',
  ];

  String nationaliyValue = 'syrian';
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  String specializationValue = '';
  int state = 0;
  List<TextEditingController> personalData = [];
  List<TextEditingController> experinceData = [];
  bool thereImage = false;
  String yearsOfExperience = '';
  late ScrollController _scrollController;
  List<Country> countriesList = [];
  List<String> countriesName = [];

  List<Cities> citiesList = [];
  List<String> citiesName = [];

  @override
  void initState() {
    LocalData();
    _scrollController = ScrollController();
    personalData = [];
    experinceData = [];
    specializationValue = '';
    // specializationValue = ConstantStrings.specialization[0];
    yearsOfExperience = '0';
    // currentCountryWork = ConstantStrings.countriesName.keys.toList()[0];
    currentCity = 'المدينة';
    super.initState();
  }

  Future<void> getStates(String country_id) async {
    if (citiesName.length == 1) {
      final data = await http.get(Uri.parse(
          ServerAddresses.serverAddress + "cities?country_id=" + country_id));
      var responseBody = data.body;
      print(responseBody);
      final parsed = json.decode(data.body).cast<Map<String, dynamic>>();

      citiesList = parsed.map<Cities>((json) => Cities.fromJson(json)).toList();

      for (int i = 0; i < citiesList.length; i++) {
        citiesName.add(citiesList[i].name ?? 'المدينة');
      }
    }

    // return citiesName;
  }

  Future<List> getCountries() async {
    if (countriesName.length == 1) {
      final data = await http
          .get(Uri.parse("${ServerAddresses.serverAddress}countries"));
      var responseBody = data.body;
      print(responseBody);
      final parsed = json.decode(data.body).cast<Map<String, dynamic>>();

      countriesList =
          parsed.map<Country>((json) => Country.fromJson(json)).toList();

      for (int i = 0; i < countriesList.length; i++) {
        countriesName.add(countriesList[i].name ?? 'اختر البلد');
      }
    }
    currentCountryWork = countriesList
        .firstWhere(((element) => element.name == 'السعودية'))
        .name;
    setState(() {});
    return countriesName.toList();
  }

  @override
  void dispose() {
    _scrollController.dispose(); // dispose the controller
    super.dispose();
  }

  view() {
    return View(
        //background: backgroundColor,
        );
  }

  experinces() {
    return ExperinceWidget(
        backgroundColor: backgroundColor,
        callBackFunction: (value) {
          _scrollController.animateTo(0,
              duration: const Duration(seconds: 1), curve: Curves.linear);
          setState(() {
            state = value;
          });
        });
  }

  personalInformation() {
    return PersonalInformation(
        personalData: personalData,
        hintTexts: hintTexts,
        callbackFunction: (value) {
          _scrollController.animateTo(0,
              duration: const Duration(seconds: 1), curve: Curves.linear);
          setState(() {
            state = value;
          });
        });
  }

  informationInsert() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          child: Container(
            decoration: BoxDecoration(
              color: state == 2
                  ? const Color.fromARGB(255, 185, 185, 185)
                  : const Color(0xFFeeeeee),
              borderRadius:
                  const BorderRadius.only(topLeft: Radius.circular(40)),
              border:
                  Border.all(color: const Color.fromARGB(255, 185, 185, 185)),
            ),
            height: 13.h,
            child: MaterialButton(
              onPressed: () {
                _scrollController.animateTo(0,
                    duration: const Duration(seconds: 3), curve: Curves.linear);
                setState(() {
                  state = 2;
                });
              },
              child: const Text(
                'معاينة',
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        Expanded(
          child: Container(
            decoration: BoxDecoration(
                border:
                    Border.all(color: const Color.fromARGB(255, 185, 185, 185)),
                color: state == 1
                    ? const Color.fromARGB(255, 185, 185, 185)
                    : const Color(0xFFeeeeee)),
            height: 13.h,
            child: MaterialButton(
              onPressed: () {
                _scrollController.animateTo(0,
                    duration: const Duration(seconds: 3), curve: Curves.linear);
                setState(() {
                  state = 1;
                });
              },
              child: const Text(
                'التجارب و الخبرات',
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        Expanded(
          child: Container(
            decoration: BoxDecoration(
                color: state == 0
                    ? const Color.fromARGB(255, 185, 185, 185)
                    : const Color(0xFFeeeeee),
                borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(40),
                ),
                border: Border.all(
                    color: const Color.fromARGB(255, 185, 185, 185))),
            height: 13.h,
            child: MaterialButton(
              focusColor: const Color.fromARGB(255, 185, 185, 185),
              onPressed: () {
                _scrollController.animateTo(0,
                    duration: const Duration(seconds: 3), curve: Curves.linear);
                setState(() {
                  state = 0;
                });
              },
              child: const Center(
                  child: Text(
                'المعلومات الشخصية',
                textAlign: TextAlign.center,
              )),
            ),
          ),
        ),
      ],
    );
  }

  uploadProfilePhotoEvent() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 25.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
              ),
              child: CustomScrollView(
                controller: _scrollController,
                slivers: [
                  SliverFillRemaining(
                    hasScrollBody: false,
                    child: state == 0
                        ? personalInformation()
                        : state == 1
                            ? experinces()
                            : view(),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: 4.5.h,
            child: Text(
              'السير الذاتية',
              style: TextStyle(color: Colors.white, fontSize: 5.w),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.h),
            child: informationInsert(),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
