import 'package:dental_kinghts/src/data/repositories/repository.dart';
import 'package:dental_kinghts/src/presentation/pages/cvs/cv_view.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '/src/constants/storage.dart';
import '/src/data/models/doctor_model.dart';
import '/src/presentation/pages/profile.dart';
import '../../../constants/color_app.dart';
import '../../widgets/drawer_doctor.dart';

class ListOfCVs extends StatefulWidget {
  const ListOfCVs({Key? key}) : super(key: key);

  @override
  State<ListOfCVs> createState() => _ListOfCVsState();
}

class _ListOfCVsState extends State<ListOfCVs> {
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  TextEditingController search = TextEditingController();
  final Repository repository = Repository();
  List<Doctor> doctors = [];
  List<Doctor> suggestions = [];

  @override
  void initState() {
    doctors = doctors;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: CustomScrollView(
                slivers: [
                  SliverFillRemaining(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 2.h,
                        ),
                        Expanded(
                          flex: 50,
                          child: SizedBox(
                            width: 92.w,
                            child: FutureBuilder<List<Doctor>>(
                                future: repository.cvs(),
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    doctors.addAll(snapshot.data!);
                                    suggestions = doctors;
                                    return ListView.separated(
                                        shrinkWrap: true,
                                        itemBuilder: ((context, index) =>
                                            CardDoctorCV(
                                                doctor: snapshot.data![index])),
                                        separatorBuilder: ((context, index) =>
                                            sepratedFunctionListView(
                                                context, index)),
                                        itemCount: snapshot.data!.length);
                                  }
                                  return const Center(
                                    child: CircularProgressIndicator(),
                                  );
                                }),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),

          /////////-----------------------------/////////
          Positioned(
            top: 4.5.h,
            child: Text(
              'السير الذاتية',
              style: TextStyle(color: Colors.white, fontSize: 5.w),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 87.w,
            child: IconButton(
              onPressed: () {
                showSearch(
                    context: context,
                    delegate: MySearchDelegate(doctors, suggestions));
              },
              icon: const Icon(
                Icons.search,
                color: Colors.white,
              ),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  sepratedFunctionListView(context, index) {
    return SizedBox(
      height: 2.h,
    );
  }

  onPressedLikeButton() {}

  onPressedFavoriteButton() {}
}

class MySearchDelegate extends SearchDelegate<Doctor> {
  List<Doctor> doctors = [];
  List<Doctor> suggestions = [];

  MySearchDelegate(this.doctors, this.suggestions);

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(onPressed: () {}, icon: const Icon(Icons.clear));
  }

  @override
  Widget buildResults(BuildContext context) {
    final list = doctors
        .where((doctor) =>
            doctor.name!.toLowerCase().contains(query.toLowerCase()))
        .toList();
    print('search delegertds fdsfmsdljfsdklfjs');
    return ListView.separated(
      itemCount: list.length,
      itemBuilder: (context, index) {
        return CardDoctorCV(
          doctor: list[index],
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 2.h,
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final list = suggestions
        .where((doctor) =>
            doctor.name!.toLowerCase().contains(query.toLowerCase()))
        .toList();
    print('suggestion');
    print(query);
    print(suggestions.length);
    return ListView.separated(
      itemCount: list.length,
      itemBuilder: (context, index) {
        return CardDoctorCV(
          doctor: list[index],
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 2.h,
        );
      },
    );
  }
}

class CardDoctorCV extends StatelessWidget {
  const CardDoctorCV({Key? key, required this.doctor}) : super(key: key);
  final Doctor doctor;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 60.w,
      height: 22.h,
      decoration: BoxDecoration(
          color: const Color(0xFFfbfbfb),
          borderRadius: BorderRadius.circular(20),
          border: Border.all(color: const Color(0xFFdbdbdb)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 1,
              offset: const Offset(0, 2),
            ),
          ]),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 4.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Column(
              children: [
                SizedBox(height: 3.5.h),
                Text(
                  'الدكتور ${doctor.name}',
                  style: TextStyle(fontSize: 5.w),
                ),
                SizedBox(height: 1.h),
                Text(
                  'أخصائي ${doctor.specialization}',
                  style:
                      TextStyle(fontSize: 3.w, color: const Color(0xFF797979)),
                ),
                SizedBox(height: 2.h),
                moreDetails(context, doctor),
              ],
            ),
            const VerticalDivider(
              color: Color(0xFFdbdbdb),
              thickness: 1,
            ),
            CircleAvatar(
              radius: 12.w,
              backgroundColor: ColorApp.primaryColor,
              child: Image.asset('assets/images/doctor.png'),
            ),
          ],
        ),
      ),
    );
  }

  Container moreDetails(context, doctor) {
    return Container(
      width: 40.w,
      height: 6.h,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: const Color(0xFF907785),
      ),
      child: Center(
        child: MaterialButton(
          onPressed: () => goToProfile(context, doctor),
          child: Center(
            child: Text(
              'المزيد من التفاصيل',
              style: TextStyle(color: Colors.white, fontSize: 3.4.w),
            ),
          ),
        ),
      ),
    );
  }

  goToProfile(context, Doctor doctor) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (_) {
        if (doctor.id == Storage().userId) {
          return Profile(doctor: doctor, isUserDoctor: true);
        } else {
          return CvView(doctor: doctor, isUserDoctor: false);
        }
      }),
    );
  }
}
