import 'package:dental_kinghts/src/data/models/cv.dart';
import 'package:dental_kinghts/src/data/models/education.dart';
import 'package:dental_kinghts/src/data/repositories/repository.dart';

import '/src/data/models/doctor_model.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../constants/color_app.dart';
import '../../widgets/details_info_personality.dart';
import '../../widgets/drawer_doctor.dart';
import '../home_page_doctor.dart';

// ignore: must_be_immutable
class CvView extends StatefulWidget {
  CvView({
    Key? key,
    required this.doctor,
    required this.isUserDoctor,
  }) : super(key: key);

  Doctor doctor;
  bool isUserDoctor;

  @override
  State<CvView> createState() => _CvViewState();
}

class _CvViewState extends State<CvView> {
  bool acceptToAll = false;
  bool faceCovering = false;
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  final Repository repository = Repository();
  whereIsWork(staticTitle, content) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 2.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          DefaultTextStyle(
            style: const TextStyle(
                color: Colors.grey, fontWeight: FontWeight.bold),
            child: Text(
              staticTitle,
            ),
          ),
          SizedBox(
            height: 1.h,
          ),
          DefaultTextStyle(
            style: const TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold),
            child: Text(content),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldState,
      drawer: DrawerDoctor(),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/background.png'))),
          ),
          Padding(
            padding: EdgeInsets.only(top: 13.h),
            child: Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: CustomScrollView(
                slivers: [
                  SliverFillRemaining(
                    hasScrollBody: false,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: 2.5.h),
                        CircleAvatar(
                          backgroundColor: ColorApp.primaryColor,
                          radius: 11.w,
                          child: Image.asset('assets/images/doctor.png'),
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 2.w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              widget.isUserDoctor
                                  ? IconButton(
                                      onPressed: () {},
                                      icon: Icon(
                                        Icons.edit,
                                        color: ColorApp.primaryColor,
                                      ),
                                      iconSize: 4.w,
                                    )
                                  : Container(),
                              const Text(
                                'المعلومات الشخصية',
                                style: TextStyle(fontWeight: FontWeight.w600),
                              )
                            ],
                          ),
                        ),
                        const Divider(),
                        SizedBox(height: 1.5.h),
                        Details(
                          title: 'الاسم',
                          content: widget.doctor.name!,
                          icon: Icons.person,
                        ),
                        SizedBox(height: 1.5.h),
                        Details(
                          title: 'الجنسية',
                          content: widget.doctor.nationality!,
                          icon: Icons.access_alarm,
                        ),
                        SizedBox(height: 1.5.h),
                        Details(
                          title: 'التخصص',
                          content: widget.doctor.specialization!,
                          icon: Icons.pages_outlined,
                        ),
                        SizedBox(height: 1.5.h),
                        Details(
                          title: 'معلومات أخرى',
                          content: widget.doctor.info!,
                          icon: Icons.perm_camera_mic_outlined,
                        ),
                        SizedBox(height: 2.h),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 2.w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              widget.isUserDoctor
                                  ? IconButton(
                                      onPressed: () {},
                                      icon: Icon(
                                        Icons.edit,
                                        color: ColorApp.primaryColor,
                                      ),
                                      iconSize: 4.w,
                                    )
                                  : Container(),
                              const Text(
                                'معلومات التواصل',
                                style: TextStyle(fontWeight: FontWeight.w600),
                              )
                            ],
                          ),
                        ),
                        const Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            whereIsWork(
                              'المدينة التي يعمل بها',
                              widget.doctor.city!,
                            ),
                            Expanded(child: Container()),
                            whereIsWork(
                              'الدولة التي يعمل بها',
                              widget.doctor.country!,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 3.w),
                              child: CircleAvatar(
                                backgroundColor: ColorApp.primaryColor,
                                radius: 3.w,
                                child: Icon(
                                  Icons.location_on,
                                  color: Colors.white,
                                  size: 4.w,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            whereIsWork(
                              'عنوان المجمع أو العيادة',
                              widget.doctor.adressOfMedicalComplext!,
                            ),
                            Expanded(child: Container()),
                            whereIsWork(
                              'اسم المجمع أو العيادة',
                              widget.doctor.medicalComplex!,
                            ),
                            SizedBox(width: 12.w),
                          ],
                        ),
                        SizedBox(height: 2.h),
                        GestureDetector(
                          onTap: () => launch('tel://${widget.doctor.phone}'),
                          child: Padding(
                            padding: EdgeInsets.only(right: 1.w),
                            child: Details(
                                title: 'رقم التواصل',
                                content: widget.doctor.phone!,
                                icon: Icons.phone),
                          ),
                        ),
                        SizedBox(height: 2.h),
                        GestureDetector(
                          onTap: () => launch('https://${widget.doctor.email}'),
                          child: Padding(
                            padding: EdgeInsets.only(right: 1.w),
                            child: Details(
                              title: 'البريد الالكتروني',
                              content: widget.doctor.email!,
                              icon: Icons.email,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 3.h,
                        ),
                        FutureBuilder<CV>(
                            future: repository.getCv(widget.doctor.id),
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                return Column(
                                  children: [
                                    Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 2.w),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          widget.isUserDoctor
                                              ? IconButton(
                                                  onPressed: () {},
                                                  icon: Icon(
                                                    Icons.grade,
                                                    color:
                                                        ColorApp.primaryColor,
                                                  ),
                                                  iconSize: 4.w,
                                                )
                                              : Container(),
                                          const Text(
                                            'التعليم',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600),
                                          )
                                        ],
                                      ),
                                    ),
                                    const Divider(),
                                    if (snapshot.data!.educations != null &&
                                        snapshot.data!.educations!.isNotEmpty)
                                      for (Educations edu
                                          in snapshot.data!.educations!)
                                        buildEducation(edu),
                                    Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 2.w),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          widget.isUserDoctor
                                              ? IconButton(
                                                  onPressed: () {},
                                                  icon: Icon(
                                                    Icons.grade,
                                                    color:
                                                        ColorApp.primaryColor,
                                                  ),
                                                  iconSize: 4.w,
                                                )
                                              : Container(),
                                          const Text(
                                            'الخبرات المهنية',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600),
                                          )
                                        ],
                                      ),
                                    ),
                                    const Divider(),
                                    if (snapshot.data!.experiences != null &&
                                        snapshot.data!.experiences!.isNotEmpty)
                                      for (Experiences exp
                                          in snapshot.data!.experiences!)
                                        buildExperience(exp),
                                    Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 2.w),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          widget.isUserDoctor
                                              ? IconButton(
                                                  onPressed: () {},
                                                  icon: Icon(
                                                    Icons.grade,
                                                    color:
                                                        ColorApp.primaryColor,
                                                  ),
                                                  iconSize: 4.w,
                                                )
                                              : Container(),
                                          const Text(
                                            'الدورات التعليمية',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600),
                                          )
                                        ],
                                      ),
                                    ),
                                    const Divider(),
                                    if (snapshot.data!.courses != null &&
                                        snapshot.data!.courses!.isNotEmpty)
                                      for (Courses course
                                          in snapshot.data!.courses!)
                                        buildCourse(course),
                                    Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 2.w),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          widget.isUserDoctor
                                              ? IconButton(
                                                  onPressed: () {},
                                                  icon: Icon(
                                                    Icons.grade,
                                                    color:
                                                        ColorApp.primaryColor,
                                                  ),
                                                  iconSize: 4.w,
                                                )
                                              : Container(),
                                          const Text(
                                            'اللغات',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600),
                                          )
                                        ],
                                      ),
                                    ),
                                    const Divider(),
                                    if (snapshot.data!.languages != null &&
                                        snapshot.data!.languages!.isNotEmpty)
                                      for (Languages lang
                                          in snapshot.data!.languages!)
                                        buildLanguage(lang),
                                  ],
                                );
                              }
                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            })
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 4.5.h,
            left: 35.w,
            child: Text(
              'السيرة الذاتية',
              style: TextStyle(
                  color: ColorApp.titleColorInRegisterPage, fontSize: 5.w),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 1.w,
            child: IconButton(
              onPressed: () {
                scaffoldState.currentState!.openDrawer();
              },
              icon: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          ),
          Positioned(
            top: 3.h,
            left: 90.w,
            child: IconButton(
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (_) => const HomePageDoctor()),
                    (route) => false);
              },
              icon: const Icon(
                Icons.home_outlined,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildEducation(Educations education) {
    return Padding(
      padding: EdgeInsets.only(bottom: 2),
      child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            whereIsWork(
              'الاختصاص',
              education.specName,
            ),
            Expanded(child: Container()),
            whereIsWork(
              'الجامعة',
              education.university,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 3.w),
              child: CircleAvatar(
                backgroundColor: ColorApp.primaryColor,
                radius: 3.w,
                child: Icon(
                  Icons.grade,
                  color: Colors.white,
                  size: 4.w,
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 2.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            whereIsWork(
              'البداية',
              education.startDate,
            ),
            Expanded(child: Container()),
            whereIsWork(
              'الانتهاء',
              education.endDate,
            ),
            SizedBox(width: 12.w),
          ],
        ),
        SizedBox(height: 2.h),
        Row(
          children: [
            SizedBox(width: 65.w),
            whereIsWork(
              'تفاصيل أخرى',
              education.eduDetails,
            ),
            SizedBox(width: 12.w),
          ],
        )
      ]),
    );
  }

  Widget buildExperience(Experiences exp) {
    return Padding(
      padding: EdgeInsets.only(bottom: 2),
      child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            whereIsWork(
              'الشركة',
              exp.companyName,
            ),
            Expanded(child: Container()),
            whereIsWork(
              'العمل',
              exp.jobName,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 3.w),
              child: CircleAvatar(
                backgroundColor: ColorApp.primaryColor,
                radius: 3.w,
                child: Icon(
                  Icons.grade,
                  color: Colors.white,
                  size: 4.w,
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 2.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            whereIsWork(
              'البداية',
              exp.expStartDate,
            ),
            Expanded(child: Container()),
            whereIsWork(
              'الانتهاء',
              exp.expEndDate,
            ),
            SizedBox(width: 12.w),
          ],
        ),
        SizedBox(height: 2.h),
        Row(
          children: [
            SizedBox(width: 65.w),
            whereIsWork(
              'تفاصيل أخرى',
              exp.exprienceDetails,
            ),
            SizedBox(width: 12.w),
          ],
        )
      ]),
    );
  }

  Widget buildCourse(Courses course) {
    return Padding(
      padding: EdgeInsets.only(bottom: 2),
      child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            whereIsWork(
              'تاريخ الدورة',
              course.courseDate,
            ),
            Expanded(child: Container()),
            whereIsWork(
              'اسم الدورة',
              course.courseName,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 3.w),
              child: CircleAvatar(
                backgroundColor: ColorApp.primaryColor,
                radius: 3.w,
                child: Icon(
                  Icons.grade,
                  color: Colors.white,
                  size: 4.w,
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 2.h,
        ),
      ]),
    );
  }

  Widget buildLanguage(Languages lang) {
    return Padding(
      padding: EdgeInsets.only(bottom: 2),
      child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              width: 10.w,
            ),
            whereIsWork(
              'المستوى',
              lang.level,
            ),
            Expanded(child: Container()),
            whereIsWork(
              'اللغة',
              lang.language,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 3.w),
              child: CircleAvatar(
                backgroundColor: ColorApp.primaryColor,
                radius: 3.w,
                child: Icon(
                  Icons.grade,
                  color: Colors.white,
                  size: 4.w,
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 2.h,
        ),
      ]),
    );
  }
}
