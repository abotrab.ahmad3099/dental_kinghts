import 'dart:io';

import 'package:dental_kinghts/src/constants/storage.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';

import '/src/constants/static_data.dart';
import '/src/data/models/personal_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sizer/sizer.dart';
import 'package:intl/intl.dart';
import '../../constants/color_app.dart';
import '../../constants/constant_strings.dart';

// ignore: must_be_immutable
class PersonalInformation extends StatefulWidget {
  PersonalInformation({
    Key? key,
    required this.personalData,
    required this.hintTexts,
    required this.callbackFunction,
  }) : super(key: key);

  Function callbackFunction;
  List<String> hintTexts = [];
  late List<TextEditingController> personalData;

  @override
  State<PersonalInformation> createState() => _PersonalInformationState();
}

class _PersonalInformationState extends State<PersonalInformation> {
  Color backgroundColor = const Color.fromARGB(255, 206, 205, 205);
  late String civilStatusValue;
  DateTime? dateTime;
  late DateTime endDate;
  late bool extraInformation;
  final formKey = GlobalKey<FormState>();
  late String gender;
  File? image;
  File? imagePersonal;
  late String nationality;
  late TimeOfDay selectedTime;
  late DateTime startDate;
  bool thereImage = false;

  @override
  void initState() {
    for (var i = 0; i < 10; i++) {
      widget.personalData.add(TextEditingController());
    }
    extraInformation = false;
    startDate = DateTime.now();
    endDate = DateTime.now();
    selectedTime = TimeOfDay.now();
    gender = 'ذكر';
    civilStatusValue = ConstantStrings.civilStatus[0];
    nationality = 'syrian';
    dateTime = DateTime.now();
    super.initState();
  }

  Future<File?> compressImage(File file) async {
    final filePath = file.absolute.path;
    final lastIndex = filePath.lastIndexOf(RegExp(r'.png|.jp'));
    final splitted = filePath.substring(0, (lastIndex));
    final outPath = "${splitted}_out${filePath.substring(lastIndex)}";

    if (lastIndex == filePath.lastIndexOf(RegExp(r'.png'))) {
      final compressedImage = await FlutterImageCompress.compressAndGetFile(
          filePath, outPath,
          minWidth: 1000,
          minHeight: 1000,
          quality: 50,
          format: CompressFormat.png);
      return compressedImage;
    } else {
      final compressedImage = await FlutterImageCompress.compressAndGetFile(
        filePath,
        outPath,
        minWidth: 1000,
        minHeight: 1000,
        quality: 50,
      );
      return compressedImage;
    }
  }

  Future pickImage() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      if (image == null) return;
      final imageTemp = File(image.path);
      StaticData.personalData!.image = await compressImage(imageTemp);
      setState(() {
        this.image = imageTemp;
        thereImage = true;
      });
    } on PlatformException catch (e) {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text(
            'warning',
            style: TextStyle(color: Colors.yellow),
          ),
          content: Text('Failed to pick image: $e'),
        ),
      );
    }
  }

  // Select for Date
  Future<DateTime> selectDate(BuildContext context) async {
    final selected = await showDatePicker(
      context: context,
      initialDate: startDate,
      firstDate: DateTime(1920),
      lastDate: DateTime(2025),
    );
    if (selected != null && selected != startDate) {
      setState(() {
        startDate = selected;
      });
    }
    return startDate;
  }

  Future selectDateTime(BuildContext context) async {
    final date = await selectDate(context);
    setState(() {
      dateTime = DateTime(
        date.year,
        date.month,
        date.day,
      );
    });
  }

  String getDateTime(dateTime1) {
    return dateTime == null
        ? 'أدخل تاريخ ميلادك'
        : DateFormat('yyyy-MM-dd').format(dateTime1!);
  }

  civilStatus() {
    return SizedBox(
      height: 15.h,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(width: 2.w),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.only(right: 2.w),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'linkedIn',
                    style: TextStyle(
                      fontSize: 4.w,
                      color: ColorApp.titleColorInRegisterPage,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 2.h),
              SizedBox(
                height: 7.h,
                child: Container(
                  width: 40.w,
                  decoration: BoxDecoration(
                    color: backgroundColor,
                    borderRadius: BorderRadius.circular(13),
                  ),
                  child: textFieldInput('linkedIn', widget.personalData[8]),
                ),
              ),
            ],
          ),
          Expanded(
            child: Container(),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.only(right: 2.w),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'الحالة المدنية',
                    style: TextStyle(
                      fontSize: 4.w,
                      color: ColorApp.titleColorInRegisterPage,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 2.h),
              Container(
                width: 40.w,
                decoration: BoxDecoration(
                  color: backgroundColor,
                  borderRadius: BorderRadius.circular(13),
                ),
                child: Center(
                  child: DropdownButton<String>(
                    borderRadius: BorderRadius.circular(13),
                    value: civilStatusValue,
                    icon: const Icon(
                      Icons.keyboard_arrow_down,
                      color: Colors.black,
                    ),
                    items: ConstantStrings.civilStatus.map((String items) {
                      return DropdownMenuItem(
                        value: items,
                        child: Text(
                          items,
                          textAlign: TextAlign.right,
                          style: const TextStyle(color: Colors.black),
                        ),
                      );
                    }).toList(),
                    onChanged: (String? newValue) {
                      setState(() {
                        civilStatusValue = newValue!;
                      });
                    },
                  ),
                ),
              ),
            ],
          ),
          SizedBox(width: 2.w),
        ],
      ),
    );
  }

  genderAndNationality() {
    return SizedBox(
      height: 15.h,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(width: 2.w),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.only(right: 2.w),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'الجنسية',
                    style: TextStyle(
                      fontSize: 4.w,
                      color: ColorApp.titleColorInRegisterPage,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 2.h),
              Container(
                width: 40.w,
                decoration: BoxDecoration(
                  color: backgroundColor,
                  borderRadius: BorderRadius.circular(13),
                ),
                child: Center(
                  child: DropdownButton<String>(
                    borderRadius: BorderRadius.circular(13),
                    value: nationality,
                    icon: const Icon(
                      Icons.keyboard_arrow_down,
                      color: Colors.black,
                    ),
                    items: ConstantStrings.nationality.map((String items) {
                      return DropdownMenuItem(
                        value: items,
                        child: Text(
                          items,
                          textAlign: TextAlign.right,
                          style: const TextStyle(color: Colors.black),
                        ),
                      );
                    }).toList(),
                    onChanged: (String? newValue) {
                      setState(() {
                        nationality = newValue!;
                      });
                    },
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: Container(),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.only(right: 2.w),
                child: Text(
                  'الجنس',
                  style: TextStyle(
                    fontSize: 4.w,
                    color: ColorApp.titleColorInRegisterPage,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 2.h),
                child: Container(
                  width: 40.w,
                  decoration: BoxDecoration(
                      color: backgroundColor,
                      borderRadius: BorderRadius.circular(13)),
                  child: Center(
                    child: DropdownButton<String>(
                      borderRadius: BorderRadius.circular(13),
                      // Initial Value
                      value: gender,

                      // Down Arrow Icon
                      icon: const Icon(
                        Icons.keyboard_arrow_down,
                        color: Colors.black,
                      ),

                      // Array list of items
                      items: ['ذكر', 'أنثى', 'غير ذلك'].map((String items) {
                        return DropdownMenuItem(
                          value: items,
                          child: Text(
                            items,
                            style: const TextStyle(color: Colors.black),
                          ),
                        );
                      }).toList(),

                      onChanged: (String? newValue) {
                        setState(() {
                          gender = newValue!;
                        });
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(width: 2.w),
        ],
      ),
    );
  }

  birthDateDataInput() {
    return SizedBox(
      height: 15.h,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(width: 2.w),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.only(right: 2.w),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'تاريخ الميلاد',
                    style: TextStyle(
                      fontSize: 4.w,
                      color: ColorApp.titleColorInRegisterPage,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 2.h),
              Container(
                height: 8.h,
                width: 40.w,
                decoration: BoxDecoration(
                  color: backgroundColor,
                  borderRadius: BorderRadius.circular(13),
                ),
                child: Container(
                  decoration: BoxDecoration(
                    color: backgroundColor,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: TextButton(
                      child: Text(
                        getDateTime(dateTime),
                        style: const TextStyle(color: Colors.black),
                      ),
                      onPressed: () async {
                        await selectDateTime(context);
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: Container(),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.only(right: 2.w),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'مكان الميلاد',
                    style: TextStyle(
                      fontSize: 4.w,
                      color: ColorApp.titleColorInRegisterPage,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 2.h),
              Container(
                width: 40.w,
                decoration: BoxDecoration(
                  color: backgroundColor,
                  borderRadius: BorderRadius.circular(13),
                ),
                child: textFieldInput('مكان الميلاد', widget.personalData[7]),
              ),
            ],
          ),
          SizedBox(width: 2.w),
        ],
      ),
    );
  }

  textFieldInput(text, controller, {type, String? defaultVal}) {
    if (defaultVal != null) {
      controller.text = defaultVal;
    }
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 3.w),
      child: TextField(
        keyboardType: type ?? TextInputType.text,
        controller: controller,
        onTap: () {
          if (controller.selection ==
              TextSelection.fromPosition(
                  TextPosition(offset: controller.text.length - 1))) {
            setState(() {
              controller.selection = TextSelection.fromPosition(
                  TextPosition(offset: controller.text.length));
            });
          }
        },
        textAlign: TextAlign.right,
        decoration: InputDecoration(
          fillColor: backgroundColor,
          filled: true,
          focusColor: backgroundColor,
          hoverColor: backgroundColor,
          hintText: text,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(
              color: backgroundColor,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(
              color: backgroundColor,
            ),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(
              color: backgroundColor,
            ),
          ),
        ),
      ),
    );
  }

  lableText(text) {
    return Padding(
      padding: EdgeInsets.only(right: 7.w),
      child: Align(
        alignment: Alignment.centerRight,
        child: Text(
          text,
          style: TextStyle(
            fontSize: 5.w,
            color: ColorApp.titleColorInRegisterPage,
          ),
        ),
      ),
    );
  }

  uploadProfilePhotoEvent() async {
    pickImage();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        SizedBox(height: 3.h),
        Padding(
          padding: EdgeInsets.only(right: 3.w),
          child: Text('المعلومات الشخصية', style: TextStyle(fontSize: 5.w)),
        ),
        const Divider(color: Colors.black),
        Padding(
          padding: EdgeInsets.only(right: 6.w),
          child: const Text(
            'المسمى الوظيفي',
            style: TextStyle(color: Color.fromARGB(255, 185, 185, 185)),
          ),
        ),
        SizedBox(height: 1.h),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 5.w),
          child: TextField(
            onTap: () {
              if (widget.personalData[0].selection ==
                  TextSelection.fromPosition(TextPosition(
                      offset: widget.personalData[0].text.length - 1))) {
                setState(() {
                  widget.personalData[0].selection = TextSelection.fromPosition(
                      TextPosition(offset: widget.personalData[0].text.length));
                });
              }
            },
            textAlign: TextAlign.right,
            textAlignVertical: TextAlignVertical.center,
            controller: widget.personalData[0],
            decoration: InputDecoration(
              hintText: 'مثال : طبيب أسنان',
              hintStyle: const TextStyle(
                  fontWeight: FontWeight.w500, color: Colors.black),
              fillColor: backgroundColor,
              filled: true,
              focusColor: backgroundColor,
              hoverColor: backgroundColor,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(12),
                borderSide: BorderSide(
                  color: backgroundColor,
                ),
              ),
              disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(12),
                borderSide: BorderSide(
                  color: backgroundColor,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(12),
                borderSide: BorderSide(color: backgroundColor),
              ),
            ),
          ),
        ),
        SizedBox(height: 1.h),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: 20.h,
              padding: EdgeInsets.only(left: 4.w),
              child: !thereImage
                  ? Center(
                      child:
                          Image.asset('assets/images/person_unavailable.png'))
                  : Center(
                      child: Image.file(
                        image!,
                        fit: BoxFit.fill,
                      ),
                    ),
            ),
            Expanded(child: Container()),
            Padding(
              padding: EdgeInsets.only(right: 3.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  const Text('الصورة الشخصية'),
                  SizedBox(height: 3.h),
                  Container(
                    height: 7.h,
                    width: 50.w,
                    decoration: BoxDecoration(
                      color: const Color(0xFFbdbcbc),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: TextButton(
                      onPressed: uploadProfilePhotoEvent,
                      child: const Text(
                        'ارفع الصورة',
                        style: TextStyle(
                            color: Color(0xFF8c7884),
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
        Form(
          key: formKey,
          child: Column(
            children: [
              lableText(widget.hintTexts[0]),
              SizedBox(height: 1.h),
              textFieldInput(widget.hintTexts[0], widget.personalData[1],
                  defaultVal: Storage().name.split(' ')[0]),
              SizedBox(height: 2.h),
              lableText(widget.hintTexts[1]),
              SizedBox(height: 1.h),
              textFieldInput(widget.hintTexts[1], widget.personalData[2],
                  defaultVal: Storage().name.split(' ')[1]),
              SizedBox(height: 2.h),
              lableText(widget.hintTexts[2]),
              SizedBox(height: 1.h),
              textFieldInput(widget.hintTexts[2], widget.personalData[3],
                  defaultVal: Storage().email),
              SizedBox(height: 2.h),
              lableText(widget.hintTexts[3]),
              SizedBox(height: 1.h),
              textFieldInput(widget.hintTexts[3], widget.personalData[4],
                  defaultVal: Storage().mobile, type: TextInputType.number),
              SizedBox(height: 2.h),
              lableText(widget.hintTexts[4]),
              SizedBox(height: 1.h),
              textFieldInput(widget.hintTexts[4], widget.personalData[5],
                  defaultVal: Storage().address),
              SizedBox(height: 2.h),
              lableText(widget.hintTexts[5]),
              SizedBox(height: 1.h),
              textFieldInput(widget.hintTexts[5], widget.personalData[6]),
              SizedBox(height: 2.h),
              Container(
                width: 94.w,
                height: 8.h,
                decoration: BoxDecoration(
                  color: const Color(0xFF8c7884),
                  borderRadius: BorderRadius.circular(12),
                ),
                child: TextButton(
                    onPressed: () {
                      setState(() {
                        extraInformation = !extraInformation;
                      });
                    },
                    child: const Text(
                      'معلومات إضافية',
                      style: TextStyle(color: Colors.white),
                    )),
              ),
              SizedBox(height: 2.h),
              if (extraInformation)
                Column(
                  children: [
                    birthDateDataInput(),
                    genderAndNationality(),
                    civilStatus(),
                  ],
                )
              else
                Container(),
              const Divider(
                color: Color.fromARGB(255, 185, 185, 185),
              ),
              Container(
                width: 50.w,
                height: 8.h,
                decoration: BoxDecoration(
                  color: const Color(0xFF8c7884),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: TextButton(
                  onPressed: () {
                    setState(() {
                      widget.callbackFunction.call(1);
                      StaticData.personalData = PersonalData();
                      StaticData.personalData!.nameOfEmployee =
                          widget.personalData[0].text;
                      StaticData.personalData!.firstName =
                          widget.personalData[1].text;
                      StaticData.personalData!.lastName =
                          widget.personalData[2].text;
                      StaticData.personalData!.email =
                          widget.personalData[3].text;
                      StaticData.personalData!.phone =
                          widget.personalData[4].text;
                      StaticData.personalData!.address =
                          widget.personalData[5].text;
                      StaticData.personalData!.city =
                          widget.personalData[6].text;
                      StaticData.personalData!.placeBirth =
                          widget.personalData[7].text;
                      StaticData.personalData!.linkedInLink =
                          widget.personalData[8].text;

                      StaticData.personalData!.civilStatus = civilStatusValue;
                      StaticData.personalData!.birthDate =
                          getDateTime(dateTime);
                      StaticData.personalData!.gender = gender;
                      StaticData.personalData!.natinality = nationality;
                    });
                  },
                  child: const Text(
                    'الخطوة التالية',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              )
            ],
          ),
        ),
      ],
    );
  }
}
