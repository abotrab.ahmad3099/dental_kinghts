import 'dart:convert';
import 'dart:io';

import 'package:dental_kinghts/src/constants/color_app.dart';
import 'package:dental_kinghts/src/data/models/spec.dart';
import 'package:dental_kinghts/src/presentation/pages/home_page_doctor.dart';
import 'package:dental_kinghts/src/presentation/widgets/profile_avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:sizer/sizer.dart';

import '../../constants/server_addresses.dart';
import '../../constants/storage.dart';

class CreatePostContainer extends StatefulWidget {
  final List<Specialization> spec;
  const CreatePostContainer({Key? key, required this.spec}) : super(key: key);

  @override
  State<CreatePostContainer> createState() => _CreatePostContainerState();
}

class _CreatePostContainerState extends State<CreatePostContainer>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  List<Asset> images = [];
  List<File> files = [];

  Future<File> getImageFileFromAssets(Asset asset) async {
    final byteData = await asset.getByteData(quality: 50);

    final tempFile =
        File("${(await getTemporaryDirectory()).path}/${asset.name}");
    final file = await tempFile.writeAsBytes(
      byteData.buffer
          .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes),
    );

    return file;
  }

  Future<void> pickImages() async {
    List<Asset> resultList = <Asset>[];

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 6,
        enableCamera: true,
        selectedAssets: images,
        materialOptions: MaterialOptions(
          actionBarTitle: "فرسان الأسنان",
          actionBarColor: ColorApp.primaryColorStr,
        ),
      );
    } on Exception catch (e) {
      print(e);
    }

    for (Asset asset in resultList) {
      File file = await getImageFileFromAssets(asset);
      files.add(file);
    }

    setState(() {
      images = resultList;
    });
  }

  late String currentSpec;
  Specialization _selectedSpec = Specialization();
  @override
  void initState() {
    currentSpec = widget.spec[0].specName!;
    _selectedSpec = widget.spec[0];
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<String> multiImagePostAPI() async {
    Fluttertoast.showToast(
        msg: "جاري إضافة المنشور", toastLength: Toast.LENGTH_LONG);

    var url = ServerAddresses.serverAddress + 'savePost';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    List<MultipartFile> newList = <MultipartFile>[];
    Map<String, String> headers = {'token': Storage().token};
    request.headers.addAll(headers);

    for (int i = 0; i < files.length; i++) {
      newList.add(await http.MultipartFile.fromPath('images[]', files[i].path));
    }

    request.fields['post_description'] = _postDescriptionController.text;
    request.fields['specilization_id'] = _selectedSpec.id!;
    request.files.addAll(newList);
    var res = await request.send();
    var response = await http.Response.fromStream(res);
    // print(response.body);
    if (res.statusCode == 200) {
      try {
        Map jsonResponse = json.decode(response.body);
        return jsonResponse['result'].toString();
      } catch (e) {
        // Fluttertoast.showToast(
        //     msg: "حدث خطا عند الترفيع يرجى التأكد من المدخلات",
        //     toastLength: Toast.LENGTH_LONG);
        // debugPrint("status : $e");
        return "0";
      }
    } else {
      Fluttertoast.showToast(
          msg: "حدث خطا عند الترفيع يرجى التأكد من المدخلات",
          toastLength: Toast.LENGTH_LONG);
      debugPrint("status : $res");
      return "0";
    }
  }

  TextEditingController _postDescriptionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Card(
          margin: EdgeInsets.symmetric(horizontal: 0.0),
          elevation: 0.0,
          shape: null,
          child: Container(
            padding: const EdgeInsets.fromLTRB(12.0, 8.0, 12.0, 0.0),
            color: Colors.white,
            child: Column(
              children: [
                SizedBox(
                  width: 90.w,
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 60.w,
                            child: Align(
                                alignment: Alignment.centerRight,
                                child: DropdownButtonFormField<String>(
                                  elevation: 10,
                                  alignment: Alignment.center,
                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(20),
                                      borderSide: const BorderSide(
                                        color: Colors.white,
                                        width: 1,
                                      ),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(20),
                                      borderSide: const BorderSide(
                                        color: Colors.white,
                                        width: 1,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(20),
                                      borderSide: const BorderSide(
                                        color: Colors.white,
                                        width: 1,
                                      ),
                                    ),
                                  ),
                                  value: currentSpec,
                                  icon: const Icon(Icons.keyboard_arrow_down),
                                  items: widget.spec.map(
                                    (Specialization item) {
                                      return DropdownMenuItem(
                                        value: item.specName,
                                        child: Text(
                                          item.specName!,
                                          textAlign: TextAlign.right,
                                          style: const TextStyle(
                                            color:
                                                Color.fromARGB(255, 81, 81, 81),
                                          ),
                                        ),
                                      );
                                    },
                                  ).toList(),
                                  onChanged: (String? newvalue) async {
                                    Specialization country = widget.spec
                                        .firstWhere((element) =>
                                            element.specName ==
                                            newvalue.toString());
                                    _selectedSpec = country;

                                    setState(() {
                                      currentSpec = newvalue!;
                                    });
                                  },
                                )),
                          ),
                        ],
                      ),
                      SizedBox(
                        width: 1.h,
                      ),
                      DefaultTextStyle(
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          fontSize: 10.sp,
                          color: Colors.black,
                        ),
                        child: const Text(
                          'اختر الاختصاص',
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    ProfileAvatar(imageUrl: 'assets/images/doctor.png'),
                    const SizedBox(width: 8.0),
                    Expanded(
                      child: TextField(
                        controller: _postDescriptionController,
                        enableSuggestions: true,
                        maxLines: null,
                        decoration: InputDecoration.collapsed(
                          hintText: 'اكتب مقالتك العلمية هنا',
                        ),
                      ),
                    )
                  ],
                ),
                const Divider(height: 10.0, thickness: 0.5),
                Container(
                  height: 40.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      TextButton.icon(
                        onPressed: () => print('Live'),
                        icon: const Icon(
                          Icons.videocam,
                          color: Colors.red,
                        ),
                        label: Text('Video'),
                      ),
                      const VerticalDivider(width: 8.0),
                      TextButton.icon(
                        onPressed: pickImages,
                        icon: const Icon(
                          Icons.photo_library,
                          color: Colors.green,
                        ),
                        label: Text('Photo'),
                      ),
                      const VerticalDivider(width: 8.0),
                      TextButton.icon(
                        onPressed: () async {
                          // multiImagePostAPI();
                          var res = await multiImagePostAPI();
                          if (res != 0) {
                            Fluttertoast.showToast(
                                msg: "تم إضافة المنشور بنجاح",
                                toastLength: Toast.LENGTH_LONG,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.green,
                                textColor: Colors.white,
                                fontSize: 16.0);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HomePageDoctor()),
                            );
                          }
                        },
                        icon: const Icon(
                          Icons.save,
                          color: Colors.blue,
                        ),
                        label: Text('نشر'),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(right: 20, left: 30),
          child: GridView.builder(
            scrollDirection: Axis.vertical,
            itemCount: images.length,
            shrinkWrap: true,
            physics: const ScrollPhysics(),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, childAspectRatio: 0.98),
            itemBuilder: (context, index) {
              return Row(
                children: [
                  Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(13),
                      ),
                    ),
                    child: AssetThumb(
                      asset: images[index],
                      width: 40.w.toInt(),
                      height: 40.w.toInt(),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ],
    );
  }
}
