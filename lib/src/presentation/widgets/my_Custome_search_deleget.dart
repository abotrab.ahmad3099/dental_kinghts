import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../data/models/doctor_model.dart';

abstract class SearchResultWidget {
  Widget showResult(dynamic data);
}

class MySearchDelegate extends SearchDelegate<Doctor> {
  List<Doctor> doctors = [];
  List<Doctor> suggestions = [];
  final SearchResultWidget widgetWillShowAsResult;

  MySearchDelegate(this.doctors, this.suggestions, this.widgetWillShowAsResult);

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(onPressed: () {}, icon: const Icon(Icons.clear));
  }

  @override
  Widget buildResults(BuildContext context) {
    final list = doctors
        .where((doctor) =>
            doctor.name!.toLowerCase().contains(query.toLowerCase()))
        .toList();
    print('search delegertds fdsfmsdljfsdklfjs');
    return ListView.separated(
      itemCount: list.length,
      itemBuilder: (context, index) {
        return widgetWillShowAsResult.showResult(doctors[index]);
      },
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 2.h,
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final list = suggestions
        .where((doctor) =>
            doctor.name!.toLowerCase().contains(query.toLowerCase()))
        .toList();
    return ListView.separated(
      itemCount: list.length,
      itemBuilder: (context, index) {
        return widgetWillShowAsResult.showResult(suggestions[index]);
      },
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 2.h,
        );
      },
    );
  }
}
