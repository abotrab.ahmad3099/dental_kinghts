import '/src/constants/static_data.dart';
import '/src/data/models/course.dart';
import '/src/data/models/education.dart';
import '/src/data/models/empolyement_qualifications.dart';
import '/src/data/models/hobbit.dart';
import '/src/data/models/language.dart';
import '/src/data/models/skill.dart';
import '/src/logic/controller_editing.dart';
import '/src/logic/more_experince_logic.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:sizer/sizer.dart';
import '../../constants/color_app.dart';
import '../../data/models/experiences.dart';

// ignore: must_be_immutable
class ExperinceWidget extends StatefulWidget {
  const ExperinceWidget(
      {Key? key, required this.backgroundColor, required this.callBackFunction})
      : super(key: key);

  final Color backgroundColor;
  final Function callBackFunction;

  @override
  State<ExperinceWidget> createState() => _ExperinceWidgetState();
}

class _ExperinceWidgetState extends State<ExperinceWidget> {
  Color backgroundColor = const Color.fromARGB(255, 206, 205, 205);
  final ControllerEditing controllerEditing = Get.put(ControllerEditing());
  late DateTime endDate;
  DateTime? endDateTime;
  TextEditingController introduction = TextEditingController();
  late TimeOfDay selectedTime;
  late DateTime startDate;
  DateTime? startDateTime;
  int value = 1;

  late ScrollController _scrollController;
  var controllerGetx;

  @override
  void dispose() {
    _scrollController.dispose(); // dispose the controller
    super.dispose();
  }

  @override
  void initState() {
    controllerGetx = Get.put(MoreExperinceLogic());
    startDate = DateTime.now();
    endDate = DateTime.now();
    selectedTime = TimeOfDay.now();
    startDateTime = DateTime.now();
    endDateTime = DateTime.now();
    _scrollController = ScrollController();
    super.initState();
  }

  // Select for Date
  Future<DateTime> selectStartDateDate(BuildContext context) async {
    final selected = await showDatePicker(
      context: context,
      initialDate: startDate,
      firstDate: DateTime(1920),
      lastDate: DateTime(2025),
    );
    if (selected != null && selected != startDate) {
      setState(() {
        startDate = selected;
      });
    }
    return startDate;
  }

  Future<DateTime> selectCourseDate(BuildContext context) async {
    final selected = await showDatePicker(
      context: context,
      initialDate: startDate,
      firstDate: DateTime(1920),
      lastDate: DateTime(2025),
    );
    if (selected != null && selected != startDate) {
      controllerEditing.updateTimeCourse(selected);
    }
    return startDate;
  }

  Future<DateTime> selectStartDateLearn(BuildContext context) async {
    final selected = await showDatePicker(
      context: context,
      initialDate: startDate,
      firstDate: DateTime(1920),
      lastDate: DateTime(2025),
    );
    if (selected != null && selected != startDate) {
      controllerEditing.updateStartDateLean(selected);
    }
    return startDate;
  }

  Future<DateTime> selectEndDateLearn(BuildContext context) async {
    final selected = await showDatePicker(
      context: context,
      initialDate: startDate,
      firstDate: DateTime(1920),
      lastDate: DateTime(2025),
    );
    if (selected != null && selected != startDate) {
      controllerEditing.updateEndDateLean(selected);
    }
    return startDate;
  }

  Future<DateTime> selectStartDateEmployeeQulification(
      BuildContext context) async {
    final selected = await showDatePicker(
      context: context,
      initialDate: startDate,
      firstDate: DateTime(1920),
      lastDate: DateTime(2025),
    );
    if (selected != null && selected != startDate) {
      controllerEditing.updateStartDateEmployeeQualificatio(selected);
    }
    return startDate;
  }

  Future<DateTime> selectEndDateEmployeeQulification(
      BuildContext context) async {
    final selected = await showDatePicker(
      context: context,
      initialDate: startDate,
      firstDate: DateTime(1920),
      lastDate: DateTime(2025),
    );
    if (selected != null && selected != startDate) {
      controllerEditing.updateEndDateEmployeeQualificatio(selected);
    }
    return startDate;
  }

  Future selectStartDateTime(BuildContext context) async {
    final date = await selectStartDateDate(context);
    setState(() {
      startDateTime = DateTime(
        date.year,
        date.month,
        date.day,
      );
    });
  }

  String formateShowDate(dateTime1) {
    return dateTime1 == null
        ? 'الرجاء اختيار التاريخ'
        : DateFormat('yyyy-MM-dd').format(dateTime1!);
  }

  Future<DateTime> selectEndDateDate(BuildContext context) async {
    final selected = await showDatePicker(
      context: context,
      initialDate: startDate,
      firstDate: DateTime(1920),
      lastDate: DateTime(2025),
    );
    if (selected != null && selected != startDate) {
      setState(() {
        startDate = selected;
      });
    }
    return startDate;
  }

  Future selectEndDateTime(BuildContext context) async {
    final date = await selectEndDateDate(context);
    setState(() {
      endDateTime = DateTime(
        date.year,
        date.month,
        date.day,
      );
    });
  }

  String getEndDateTime() {
    return endDateTime == null
        ? 'الرجاء اختيار التاريخ'
        : DateFormat('yyyy-MM-dd').format(endDateTime!);
  }

  experinceInCV(
      BuildContext context,
      String firstText,
      String secondText,
      bool ifThereNameCompant,
      String firstButton,
      String secondButton,
      TextEditingController controller1,
      TextEditingController controller2,
      DateTime startDate,
      DateTime endDate,
      {TextEditingController? companyNameController,
      TextEditingController? detialsController,
      Function? buttonAdd,
      Function? buttonEditi}) {
    return SizedBox(
      height: ifThereNameCompant ? 70.h : 52.h,
      child: Column(
        children: [
          firstInput(firstText, secondText, controller1, controller2),
          SizedBox(height: 1.h),
          ifThereNameCompant
              ? nameOfComapany('اسم الشركة أو المؤسسة', companyNameController!)
              : const SizedBox(),
          ifThereNameCompant ? SizedBox(height: 1.h) : const SizedBox(),
          ifThereNameCompant ? SizedBox(height: 1.h) : const SizedBox(),
          dateStartEnd(context, startDate, endDate, ifThereNameCompant),
          SizedBox(height: 1.h),
          moreDetials(detialsController!),
          SizedBox(height: 1.h),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              addMoreExperinces(secondButton, buttonEditi),
              addMoreExperinces(firstButton, buttonAdd),
            ],
          ),
        ],
      ),
    );
  }

  addMoreExperinces(String text, function) {
    return SizedBox(
      height: 4.h,
      child: GetBuilder<MoreExperinceLogic>(
          init: MoreExperinceLogic(),
          builder: (_) {
            return TextButton(
              onPressed: function,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    text,
                    style: TextStyle(color: ColorApp.primaryColor),
                  ),
                  Icon(Icons.add, color: ColorApp.primaryColor)
                ],
              ),
            );
          }),
    );
  }

  moreDetials(TextEditingController detialsController) {
    return SizedBox(
      height: 13.h,
      child: Row(
        children: [
          SizedBox(width: 3.w),
          Expanded(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 2.w),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      'المزيد من التفاصيل',
                      style: TextStyle(
                        fontSize: 4.w,
                        color: ColorApp.titleColorInRegisterPage,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 2.h),
                Expanded(
                  child: TextField(
                    keyboardType: TextInputType.text,
                    controller: detialsController,
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      fillColor: backgroundColor,
                      filled: true,
                      focusColor: backgroundColor,
                      hoverColor: backgroundColor,
                      hintText: 'المزيد من التفاصيل',
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: BorderSide(
                            color: backgroundColor,
                          )),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: BorderSide(
                            color: backgroundColor,
                          )),
                      disabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                        borderSide: BorderSide(
                          color: backgroundColor,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(width: 2.w),
        ],
      ),
    );
  }

  dateStartEnd(BuildContext context, startDate, endDate, ifThereNameCompant) {
    return SizedBox(
      height: 13.h,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(width: 3.w),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(width: 3.w),
                Expanded(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 1.w),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Text(
                            'إلى تاريخ',
                            style: TextStyle(
                              fontSize: 4.w,
                              color: ColorApp.titleColorInRegisterPage,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 2.h),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: backgroundColor,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: TextButton(
                              child: Text(
                                formateShowDate(endDate),
                                style: const TextStyle(color: Colors.black),
                              ),
                              onPressed: () {
                                ifThereNameCompant
                                    ? selectEndDateEmployeeQulification(context)
                                    : selectEndDateLearn(context);
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 5.w),
                Expanded(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 5.w),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Text(
                            'من تاريخ',
                            style: TextStyle(
                              fontSize: 4.w,
                              color: ColorApp.titleColorInRegisterPage,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 2.h),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: backgroundColor,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: TextButton(
                              child: Text(
                                formateShowDate(startDate),
                                style: const TextStyle(color: Colors.black),
                              ),
                              onPressed: () {
                                ifThereNameCompant
                                    ? selectStartDateEmployeeQulification(
                                        context)
                                    : selectStartDateLearn(context);
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 3.w),
              ],
            ),
          ),
        ],
      ),
    );
  }

  nameOfComapany(text, TextEditingController companyNameController) {
    return SizedBox(
      height: 15.h,
      child: Row(
        children: [
          SizedBox(width: 3.w),
          Expanded(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 2.w),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      text,
                      style: TextStyle(
                        fontSize: 4.w,
                        color: ColorApp.titleColorInRegisterPage,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 2.h),
                Expanded(
                  child: TextField(
                    keyboardType: TextInputType.text,
                    controller: companyNameController,
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      fillColor: backgroundColor,
                      filled: true,
                      focusColor: backgroundColor,
                      hoverColor: backgroundColor,
                      hintText: text,
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: BorderSide(
                            color: backgroundColor,
                          )),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: BorderSide(
                            color: backgroundColor,
                          )),
                      disabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                        borderSide: BorderSide(
                          color: backgroundColor,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(width: 3.w),
        ],
      ),
    );
  }

  firstInput(firstText, secondText, TextEditingController controller1,
      TextEditingController controller2) {
    return SizedBox(
      height: 15.h,
      child: Row(
        children: [
          SizedBox(width: 3.w),
          Expanded(
            child: Row(
              children: [
                SizedBox(width: 3.w),
                Expanded(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 2.w),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Text(
                            secondText,
                            style: TextStyle(
                              fontSize: 4.w,
                              color: ColorApp.titleColorInRegisterPage,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 2.h),
                      Expanded(
                        child: TextField(
                          keyboardType: TextInputType.text,
                          controller: controller1,
                          textAlign: TextAlign.right,
                          decoration: InputDecoration(
                            fillColor: backgroundColor,
                            filled: true,
                            focusColor: backgroundColor,
                            hoverColor: backgroundColor,
                            hintText: secondText,
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12),
                                borderSide: BorderSide(
                                  color: backgroundColor,
                                )),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12),
                                borderSide: BorderSide(
                                  color: backgroundColor,
                                )),
                            disabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12),
                              borderSide: BorderSide(
                                color: backgroundColor,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 5.w),
                Expanded(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 2.w),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Text(
                            firstText,
                            style: TextStyle(
                              fontSize: 4.w,
                              color: ColorApp.titleColorInRegisterPage,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 2.h),
                      Expanded(
                        child: TextField(
                          keyboardType: TextInputType.text,
                          controller: controller2,
                          textAlign: TextAlign.right,
                          decoration: InputDecoration(
                            fillColor: backgroundColor,
                            filled: true,
                            focusColor: backgroundColor,
                            hoverColor: backgroundColor,
                            hintText: firstText,
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12),
                                borderSide: BorderSide(
                                  color: backgroundColor,
                                )),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12),
                                borderSide: BorderSide(
                                  color: backgroundColor,
                                )),
                            disabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12),
                              borderSide: BorderSide(
                                color: backgroundColor,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 3.w),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            SizedBox(height: 3.h),
            Padding(
              padding: EdgeInsets.only(right: 3.w),
              child: Text('التجارب والخبرات', style: TextStyle(fontSize: 5.w)),
            ),
            const Divider(color: Colors.black),
            Padding(
              padding: EdgeInsets.only(right: 5.w),
              child: Text(
                'المقدمة',
                style: TextStyle(color: Colors.black, fontSize: 5.w),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.w),
              child: TextField(
                maxLines: 5,
                textAlign: TextAlign.right,
                textAlignVertical: TextAlignVertical.center,
                controller: controllerGetx.introduction,
                decoration: InputDecoration(
                  hintText: '...اكتب هنا',
                  hintStyle: const TextStyle(
                      fontWeight: FontWeight.w500, color: Colors.black),
                  fillColor: widget.backgroundColor,
                  filled: true,
                  focusColor: widget.backgroundColor,
                  hoverColor: widget.backgroundColor,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                    borderSide: BorderSide(
                      color: widget.backgroundColor,
                    ),
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                    borderSide: BorderSide(
                      color: widget.backgroundColor,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                    borderSide: BorderSide(color: widget.backgroundColor),
                  ),
                ),
              ),
            ),
            const Divider(),
            SizedBox(height: 1.h),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GetBuilder<MoreExperinceLogic>(
                    init: MoreExperinceLogic(),
                    builder: (_) {
                      return Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(color: Colors.black)),
                          child: Center(
                            child: DropdownButton<int>(
                              value: _.counterEmp,
                              hint: const Text("0"),
                              icon: const Icon(Icons.keyboard_arrow_down),
                              items: _.empQualifiqation.map((int item) {
                                return DropdownMenuItem<int>(
                                  value: item,
                                  child: Text(item.toString()),
                                );
                              }).toList(),
                              onChanged: (int? newValue) {
                                // print(newValue);
                                if (newValue != 0) {
                                  _.updateValueEmpQualification(newValue);
                                  EmpolyementQualifications
                                      empolyementQualifications = StaticData
                                              .experiences!
                                              .empolyementQualifications![
                                          _.counterEmp - 1];
                                  controllerEditing.updateEmployeeQualification(
                                    empolyementQualifications.nameOfJobs,
                                    empolyementQualifications.countryCity,
                                    empolyementQualifications.nameOfComapny,
                                    empolyementQualifications.moreDetials,
                                    DateTime.parse(
                                        empolyementQualifications.startDate!),
                                    DateTime.parse(
                                        empolyementQualifications.endDate!),
                                  );
                                }
                              },
                            ),
                          ),
                        ),
                      );
                    }),
                const Expanded(child: SizedBox()),
                Padding(
                  padding: EdgeInsets.only(right: 5.w),
                  child: Text(
                    'الخبرات الوظيفية',
                    style: TextStyle(color: Colors.black, fontSize: 5.w),
                  ),
                ),
              ],
            ),
            SizedBox(height: 1.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.w),
              child: Text(
                '.أضف الوظائف والمناصب التي شغلتها في الوصف أضف إنجازاتك والمهام التي كنت تقوم بها',
                textAlign: TextAlign.end,
                style: TextStyle(
                  color: const Color.fromARGB(255, 192, 189, 189),
                  height: 0.2.h,
                ),
                maxLines: 2,
              ),
            ),
            SizedBox(height: 2.h),
            GetBuilder<ControllerEditing>(
              init: ControllerEditing(),
              builder: (_) {
                return experinceInCV(
                  context,
                  'اسم الوظيفة (المنصب)',
                  'المدينة/الدولة',
                  true,
                  'أضف المزيد من الخبرات',
                  'التعديل على خبرة',
                  _.employeeQualif[0],
                  _.employeeQualif[1],
                  _.dateTimeStartEmpQualification,
                  _.dateTimeEndEmpQualification,
                  companyNameController: _.employeeQualif[2],
                  detialsController: _.employeeQualif[3],
                  buttonAdd: () {
                    var emp = EmpolyementQualifications(
                      nameOfJobs: controllerEditing.employeeQualif[0].text,
                      countryCity: controllerEditing.employeeQualif[1].text,
                      nameOfComapny: controllerEditing.employeeQualif[2].text,
                      moreDetials: controllerEditing.employeeQualif[3].text,
                      startDate:
                          formateShowDate(_.dateTimeStartEmpQualification),
                      endDate: formateShowDate(_.dateTimeEndEmpQualification),
                    );

                    StaticData.experiences!.empolyementQualifications!.add(emp);
                    _.education[1].clear();
                    _.education[2].clear();
                    _.updateEmployeeQualification(
                        "", "", "", "", DateTime.now(), DateTime.now());

                    Get.find<MoreExperinceLogic>().updateEmployeeQualifi();
                  },
                  buttonEditi: () {
                    StaticData.experiences!.empolyementQualifications![
                            Get.find<MoreExperinceLogic>().counterEmp - 1] =
                        EmpolyementQualifications(
                      nameOfJobs: controllerEditing.employeeQualif[0].text,
                      countryCity: controllerEditing.employeeQualif[1].text,
                      nameOfComapny: controllerEditing.employeeQualif[2].text,
                      moreDetials: controllerEditing.employeeQualif[3].text,
                      startDate:
                          formateShowDate(_.dateTimeStartEmpQualification),
                      endDate: formateShowDate(_.dateTimeEndEmpQualification),
                    );

                    _.employeeQualif[0].clear();
                    _.employeeQualif[1].clear();
                    _.employeeQualif[2].clear();
                    _.employeeQualif[3].clear();
                    _.updateEmployeeQualification(
                        "", "", "", "", DateTime.now(), DateTime.now());
                  },
                );
              },
            ),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GetBuilder<MoreExperinceLogic>(
                    init: MoreExperinceLogic(),
                    builder: (_) {
                      return Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(color: Colors.black)),
                          child: Center(
                            child: DropdownButton<int>(
                              value: _.counterEducation,
                              hint: const Text("0"),
                              icon: const Icon(Icons.keyboard_arrow_down),
                              items: _.educations.map((int item) {
                                return DropdownMenuItem<int>(
                                  value: item,
                                  child: Text(item.toString()),
                                );
                              }).toList(),
                              onChanged: (int? newValue) {
                                if (newValue != 0) {
                                  _.updateValueEducation(newValue);
                                  Education education = StaticData.experiences!
                                      .educations![_.counterEducation - 1];
                                  controllerEditing.updateEducation(
                                    education.nameOfSpecilization,
                                    education.nameOfUniversity,
                                    education.moreDetials,
                                    DateTime.parse(education.startDate!),
                                    DateTime.parse(education.endDate!),
                                  );
                                }
                              },
                            ),
                          ),
                        ),
                      );
                    }),
                const Expanded(child: SizedBox()),
                Padding(
                  padding: EdgeInsets.only(right: 5.w),
                  child: Text(
                    'التعليم',
                    style: TextStyle(color: Colors.black, fontSize: 5.w),
                  ),
                ),
              ],
            ),
            SizedBox(height: 1.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.w),
              child: Text(
                'أضف مؤهلك التعليمي . مثل شهادة جامعية ، ماستر ،دكتوراه',
                textAlign: TextAlign.end,
                style: TextStyle(
                  color: const Color.fromARGB(255, 192, 189, 189),
                  height: 0.2.h,
                ),
                maxLines: 2,
              ),
            ),
            SizedBox(height: 1.h),
            GetBuilder<ControllerEditing>(
                init: ControllerEditing(),
                builder: (_) {
                  return experinceInCV(
                      context,
                      'التخصص أو الدراسة',
                      'اسم الجامعة',
                      false,
                      'أضف مؤهل',
                      'التعديل على المؤهل',
                      _.education[0],
                      _.education[1],
                      _.dateTimeStartLearn,
                      _.dateTimeEndLearn,
                      detialsController: _.education[2],
                      //button add
                      buttonAdd: () {
                    StaticData.experiences!.educations!.add(
                      Education(
                        nameOfSpecilization: _.education[0].text,
                        nameOfUniversity: _.education[1].text,
                        startDate: formateShowDate(_.dateTimeStartLearn),
                        endDate: formateShowDate(_.dateTimeEndLearn),
                        moreDetials: _.education[2].text,
                      ),
                    );
                    _.education[0].clear();
                    _.education[1].clear();
                    _.education[2].clear();
                    _.updateEducation(
                        "", "", "", DateTime.now(), DateTime.now());

                    Get.find<MoreExperinceLogic>().updateEducation();
                  }, buttonEditi: () {
                    StaticData.experiences!.educations![
                        Get.find<MoreExperinceLogic>().counterEducation -
                            1] = Education(
                      nameOfSpecilization: controllerEditing.education[0].text,
                      nameOfUniversity: controllerEditing.education[1].text,
                      moreDetials: controllerEditing.education[2].text,
                      startDate:
                          formateShowDate(controllerEditing.dateTimeStartLearn),
                      endDate:
                          formateShowDate(controllerEditing.dateTimeEndLearn),
                    );

                    controllerEditing.education[0].clear();
                    controllerEditing.education[1].clear();
                    controllerEditing.education[2].clear();
                    _.updateEducation(
                        "", "", "", DateTime.now(), DateTime.now());
                  });
                }),
            const Divider(),
            Row(
              children: [
                GetBuilder<MoreExperinceLogic>(
                    init: MoreExperinceLogic(),
                    builder: (_) {
                      return Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(color: Colors.black)),
                          child: Center(
                            child: DropdownButton<int>(
                              value: _.counterLang,
                              hint: const Text("0"),
                              icon: const Icon(Icons.keyboard_arrow_down),
                              items: _.lang.map((int item) {
                                return DropdownMenuItem<int>(
                                  value: item,
                                  child: Text(item.toString()),
                                );
                              }).toList(),
                              onChanged: (int? newValue) {
                                if (newValue != 0) {
                                  _.updateValueLang(newValue);
                                  Language language = StaticData.experiences!
                                      .languages![_.counterLang - 1];

                                  controllerEditing.updateLangData(
                                      language.language, language.level);
                                }
                              },
                            ),
                          ),
                        ),
                      );
                    }),
                const Expanded(child: SizedBox()),
                Padding(
                  padding: EdgeInsets.only(right: 5.w),
                  child: Text(
                    'اللغات',
                    style: TextStyle(color: Colors.black, fontSize: 5.w),
                  ),
                ),
              ],
            ),
            SizedBox(height: 1.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.w),
              child: Text(
                'أضف اللغات التي تتقنها',
                textAlign: TextAlign.end,
                style: TextStyle(
                  color: const Color.fromARGB(255, 192, 189, 189),
                  height: 0.2.h,
                ),
              ),
            ),
            SizedBox(height: 1.h),
            SizedBox(
              height: 20.h,
              child: Column(
                children: [
                  firstInput(
                    'اللغة',
                    'المستوى',
                    controllerEditing.lang[0],
                    controllerEditing.lang[1],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      addMoreExperinces('التعديل على لغة', () {
                        StaticData.experiences!.languages![
                            Get.find<MoreExperinceLogic>().counterLang -
                                1] = Language(
                          language: controllerEditing.lang[1].text,
                          level: controllerEditing.lang[0].text,
                        );

                        controllerEditing.lang[0].clear();
                        controllerEditing.lang[1].clear();
                      }),
                      addMoreExperinces('أضف لغة', () {
                        StaticData.experiences!.languages!.add(
                          Language(
                            language: controllerEditing.lang[1].text,
                            level: controllerEditing.lang[0].text,
                          ),
                        );
                        controllerEditing.lang[0].clear();
                        controllerEditing.lang[1].clear();
                        Get.find<MoreExperinceLogic>().updateLang();
                      }),
                    ],
                  ),
                ],
              ),
            ),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GetBuilder<MoreExperinceLogic>(
                    init: MoreExperinceLogic(),
                    builder: (_) {
                      return Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(color: Colors.black)),
                          child: Center(
                            child: DropdownButton<int>(
                              value: _.counterSkills,
                              hint: const Text("0"),
                              icon: const Icon(Icons.keyboard_arrow_down),
                              items: _.skills.map((int item) {
                                return DropdownMenuItem<int>(
                                  value: item,
                                  child: Text(item.toString()),
                                );
                              }).toList(),
                              onChanged: (int? newValue) {
                                if (newValue != 0) {
                                  _.updateValueSkills(newValue);
                                  Skill skill = StaticData.experiences!
                                      .skills![_.counterSkills - 1];

                                  controllerEditing.updateSkillsData(
                                      skill.nameSkill, skill.level);
                                }
                              },
                            ),
                          ),
                        ),
                      );
                    }),
                const Expanded(child: SizedBox()),
                Padding(
                  padding: EdgeInsets.only(right: 5.w),
                  child: Text(
                    'المهارات',
                    style: TextStyle(color: Colors.black, fontSize: 5.w),
                  ),
                ),
              ],
            ),
            SizedBox(height: 1.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.w),
              child: Text(
                'أضف المهارات التي تتقنها',
                textAlign: TextAlign.end,
                style: TextStyle(
                  color: const Color.fromARGB(255, 192, 189, 189),
                  height: 0.2.h,
                ),
                maxLines: 2,
              ),
            ),
            SizedBox(height: 1.5.h),
            SizedBox(
              height: 20.h,
              child: Column(
                children: [
                  firstInput(
                    'المهارة',
                    'المستوى',
                    controllerEditing.skill[0],
                    controllerEditing.skill[1],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      addMoreExperinces('التعديل على مهارة', () {
                        StaticData.experiences!.skills![
                            Get.find<MoreExperinceLogic>().counterSkills -
                                1] = Skill(
                          nameSkill: controllerEditing.skill[1].text,
                          level: controllerEditing.skill[0].text,
                        );

                        controllerEditing.skill[0].clear();
                        controllerEditing.skill[1].clear();
                      }),
                      addMoreExperinces('أضف مهارة', () {
                        StaticData.experiences!.skills!.add(Skill(
                          nameSkill: controllerEditing.skill[1].text,
                          level: controllerEditing.skill[0].text,
                        ));
                        controllerEditing.skill[0].clear();
                        controllerEditing.skill[1].clear();
                        Get.find<MoreExperinceLogic>().updateSkills();
                      }),
                    ],
                  ),
                ],
              ),
            ),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GetBuilder<MoreExperinceLogic>(
                    init: MoreExperinceLogic(),
                    builder: (_) {
                      return Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(color: Colors.black)),
                          child: Center(
                            child: DropdownButton<int>(
                              value: _.counterHobbies,
                              hint: const Text("0"),
                              icon: const Icon(Icons.keyboard_arrow_down),
                              items: _.hobbies.map((int item) {
                                return DropdownMenuItem<int>(
                                  value: item,
                                  child: Text(item.toString()),
                                );
                              }).toList(),
                              onChanged: (int? newValue) {
                                if (newValue != 0) {
                                  _.updateValueHobbies(newValue);
                                  Hobbit hobbit = StaticData.experiences!
                                      .hobbits![_.counterHobbies - 1];

                                  controllerEditing
                                      .updateHobbit(hobbit.nameOfHobbit);
                                }
                              },
                            ),
                          ),
                        ),
                      );
                    }),
                const Expanded(child: SizedBox()),
                Padding(
                  padding: EdgeInsets.only(right: 5.w),
                  child: Text(
                    'الهوايات',
                    style: TextStyle(color: Colors.black, fontSize: 5.w),
                  ),
                ),
              ],
            ),
            SizedBox(height: 1.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.w),
              child: Text(
                'أضف الهوايات التي تفضل ممارستها في وقت المتعة والراحة',
                textAlign: TextAlign.end,
                style: TextStyle(
                  color: const Color.fromARGB(255, 192, 189, 189),
                  height: 0.2.h,
                ),
                maxLines: 2,
              ),
            ),
            SizedBox(height: 1.5.h),
            SizedBox(
              height: 20.h,
              child: Column(
                children: [
                  nameOfComapany(
                    'الهواية',
                    controllerEditing.hobbies[0],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      addMoreExperinces('تعديل على هواية', () {
                        StaticData.experiences!.hobbits![
                            Get.find<MoreExperinceLogic>().counterHobbies -
                                1] = Hobbit(
                          nameOfHobbit: controllerEditing.hobbies[0].text,
                        );

                        controllerEditing.hobbies[0].clear();
                      }),
                      addMoreExperinces('أضف هواية', () {
                        StaticData.experiences!.hobbits!.add(
                          Hobbit(
                            nameOfHobbit: controllerEditing.hobbies[0].text,
                          ),
                        );
                        controllerEditing.hobbies[0].clear();
                        Get.find<MoreExperinceLogic>().updateHobbies();
                      }),
                    ],
                  ),
                ],
              ),
            ),
            const Divider(),
            Padding(
              padding: EdgeInsets.only(right: 5.w),
              child: Row(
                children: [
                  GetBuilder<MoreExperinceLogic>(
                      init: MoreExperinceLogic(),
                      builder: (_) {
                        return Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                border: Border.all(color: Colors.black)),
                            child: Center(
                              child: DropdownButton<int>(
                                value: _.counterCourses,
                                hint: const Text("0"),
                                icon: const Icon(Icons.keyboard_arrow_down),
                                items: _.courses.map((int item) {
                                  return DropdownMenuItem<int>(
                                    value: item,
                                    child: Text(item.toString()),
                                  );
                                }).toList(),
                                onChanged: (int? newValue) {
                                  if (newValue != 0) {
                                    _.updateValueCourse(newValue);
                                    Course course = StaticData.experiences!
                                        .courses![_.counterCourses - 1];

                                    controllerEditing.updateCourseData(
                                        course.nameCourse, course.date);
                                    setState(() {
                                      startDateTime =
                                          DateTime.parse(course.date!);
                                    });
                                  }
                                },
                              ),
                            ),
                          ),
                        );
                      }),
                  const Expanded(child: SizedBox()),
                  Text(
                    'الدورات التدريبية',
                    style: TextStyle(color: Colors.black, fontSize: 5.w),
                  ),
                ],
              ),
            ),
            SizedBox(height: 1.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.w),
              child: Text(
                'أضف الدورات التدريبية التي حصلت عليها',
                textAlign: TextAlign.end,
                style: TextStyle(
                  color: const Color.fromARGB(255, 192, 189, 189),
                  height: 0.2.h,
                ),
                maxLines: 2,
              ),
            ),
            SizedBox(height: 1.5.h),
            SizedBox(
              height: 35.h,
              child: Column(
                children: [
                  SizedBox(
                    height: 15.h,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 4.w),
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              'الدورة التدريبية',
                              style: TextStyle(
                                fontSize: 5.w,
                                color: ColorApp.titleColorInRegisterPage,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 2.h),
                        Expanded(
                          child: SizedBox(
                            height: 10.h,
                            width: 95.w,
                            child: TextField(
                              keyboardType: TextInputType.text,
                              controller: controllerEditing.course[0],
                              textAlign: TextAlign.right,
                              decoration: InputDecoration(
                                fillColor: backgroundColor,
                                filled: true,
                                focusColor: backgroundColor,
                                hoverColor: backgroundColor,
                                hintText: 'الدورة التدريبية',
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12),
                                    borderSide: BorderSide(
                                      color: backgroundColor,
                                    )),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12),
                                    borderSide: BorderSide(
                                      color: backgroundColor,
                                    )),
                                disabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12),
                                  borderSide: BorderSide(
                                    color: backgroundColor,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  // تاريخ
                  SizedBox(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(right: 5.w),
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              'التاريخ',
                              style: TextStyle(
                                fontSize: 4.w,
                                color: ColorApp.titleColorInRegisterPage,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 2.h),
                        GetBuilder<ControllerEditing>(
                            init: ControllerEditing(),
                            builder: (_) {
                              return SizedBox(
                                height: 8.h,
                                width: 95.w,
                                child: Container(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 4.w),
                                  decoration: BoxDecoration(
                                    color: backgroundColor,
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: TextButton(
                                      child: Text(
                                        formateShowDate(_.dateTimeCourse),
                                        style: const TextStyle(
                                            color: Colors.black),
                                      ),
                                      onPressed: () {
                                        selectCourseDate(context);
                                      },
                                    ),
                                  ),
                                ),
                              );
                            }),
                      ],
                    ),
                  ),
                  SizedBox(height: 1.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      addMoreExperinces('تعديل على دورة', () {
                        StaticData.experiences!.courses![
                            Get.find<MoreExperinceLogic>().counterCourses -
                                1] = Course(
                            nameCourse: controllerEditing.course[0].text,
                            date: formateShowDate(
                                Get.find<ControllerEditing>().dateTimeCourse));
                        controllerEditing.updateCourseData(
                            '', DateTime.now().toString());
                        controllerEditing.course[0].clear();
                      }),
                      addMoreExperinces('أضف دورة', () {
                        StaticData.experiences!.courses!.add(
                          Course(
                            nameCourse: controllerEditing.course[0].text,
                            date: formateShowDate(
                                controllerEditing.dateTimeCourse),
                          ),
                        );
                        controllerEditing.updateCourseData(
                            '', DateTime.now().toString());
                        controllerEditing.course[0].clear();

                        Get.find<MoreExperinceLogic>().updateCourse();
                      }),
                    ],
                  ),
                ],
              ),
            ),
            const Divider(),
            Align(
              alignment: Alignment.center,
              child: Container(
                width: 50.w,
                height: 8.h,
                decoration: BoxDecoration(
                  color: const Color(0xFF8c7884),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: TextButton(
                  onPressed: () {
                    //                   StaticData.experiences =  Experiences(
                    //   introduction: '',
                    //   empolyementQualifications: <EmpolyementQualifications>[],
                    //   courses: <Course>[],
                    //   educations: [],
                    //   hobbits: <Hobbit>[],
                    //   languages: <Language>[],
                    //   skills: <Skill>[],
                    // );

                    setState(() {
                      widget.callBackFunction.call(2);
                    });
                  },
                  child: const Text(
                    'الخطوة التالية',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Align(
              alignment: Alignment.center,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: 80.w,
                    height: 7.h,
                    decoration: BoxDecoration(color: ColorApp.primaryColor),
                    child: const Align(
                      alignment: Alignment.center,
                      child: Text(
                        'نصائح خبرات العمل',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  Container(
                    width: 80.w,
                    height: 15.h,
                    decoration: const BoxDecoration(
                        color: Color.fromARGB(255, 206, 205, 205),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20))),
                    child: Align(
                      alignment: Alignment.center,
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 3.w),
                        child: const Text(
                          'القسم الذي تراه غير ضروري اتركه فارغاً ولن يظهر في سيرتك الذاتية\n في قسم خبرات العمل ابدأ بوظيفتك الأخيرة ثم استمر حتى الوظيفة الأولى',
                          textAlign: TextAlign.end,
                          maxLines: 4,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ],
    );
  }
}
