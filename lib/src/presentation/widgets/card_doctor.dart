import 'package:flutter/material.dart';
import '../../constants/color_app.dart';
import '../../data/models/doctor_model.dart';
import '../pages/detials_doctor.dart';
import 'package:sizer/sizer.dart';

class CardDoctor extends StatelessWidget {
  const CardDoctor({
    Key? key,
    required this.nameDoctor,
    required this.medicalComplex,
    required this.doctor,
  }) : super(key: key);

  final Doctor? doctor;
  final String? medicalComplex;
  final String? nameDoctor;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: <Widget>[
        Container(
          height: 27.h,
          width: 90.w,
          margin: EdgeInsets.all(6.w),
          child: Card(
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            color: const Color.fromRGBO(242, 242, 242, 300),
            child: Container(),
          ),
        ),
        Container(
          padding: EdgeInsets.only(top: 8.h),
          child: Column(
            children: [
              Text(
                'الدكتور ${nameDoctor!}',
                style: TextStyle(fontSize: 5.w),
              ),
              SizedBox(
                height: 2.h,
              ),
              Text(
                'مجمع ${medicalComplex!}',
                style: TextStyle(fontSize: 5.w),
              ),
              SizedBox(
                height: 3.h,
              ),
              Container(
                width: 30.w,
                decoration: BoxDecoration(
                    color: ColorApp.imageOrDetialsColor,
                    borderRadius: BorderRadius.circular(20)),
                child: MaterialButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => DetialsDoctor(doctor: doctor),
                      ),
                    );
                  },
                  child: const Text(
                    'التفاصيل',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          width: 50,
          height: 50,
          decoration: const ShapeDecoration(
            shape: CircleBorder(),
            color: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.all(1),
            child: DecoratedBox(
              position: DecorationPosition.background,
              decoration: ShapeDecoration(
                color: ColorApp.imageOrDetialsColor,
                shape: const CircleBorder(),
                image: const DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(
                    'assets/images/doctor.png',
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
