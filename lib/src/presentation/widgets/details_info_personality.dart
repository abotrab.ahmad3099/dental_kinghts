import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import '../../constants/color_app.dart';

class Details extends StatelessWidget {
  const Details({
    Key? key,
    required this.title,
    required this.content,
    required this.icon,
  }) : super(key: key);

  final String content;
  final IconData icon;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 2.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  title,
                  textAlign: TextAlign.start,
                  textDirection: TextDirection.rtl,
                  style: TextStyle(color: ColorApp.titleColorInRegisterPage),
                ),
                Text(
                  content,
                  textAlign: TextAlign.start,
                  textDirection: TextDirection.rtl,
                ),
              ],
            ),
          ),
          SizedBox(width: 2.w),
          CircleAvatar(
            backgroundColor: ColorApp.primaryColor,
            radius: 3.w,
            child: Center(
              child: Icon(
                icon,
                color: Colors.white,
                size: 4.w,
              ),
            ),
          )
        ],
      ),
    );
  }
}
