import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../constants/color_app.dart';

class ButtonsInApp extends StatelessWidget {
  const ButtonsInApp({
    Key? key,
    required this.onPressed,
    required this.textButton,
  }) : super(key: key);

  final void Function() onPressed;
  final String textButton;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40.w,
      height: 6.h,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        gradient: LinearGradient(
          colors: [ColorApp.primaryColor, ColorApp.blueColor],
        ),
      ),
      child: MaterialButton(
        onPressed: onPressed,
        child: Text(textButton, style: const TextStyle(color: Colors.white)),
      ),
    );
  }
}
