import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../constants/color_app.dart';

class ImageProfileDoctor extends StatelessWidget {
  const ImageProfileDoctor({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 7.5.h),
      child: Container(
        width: 20.w,
        height: 10.h,
        decoration: const ShapeDecoration(
          shape: CircleBorder(),
          color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.all(1),
          child: DecoratedBox(
            position: DecorationPosition.background,
            decoration: ShapeDecoration(
              color: ColorApp.imageOrDetialsColor,
              shape: const CircleBorder(),
              image: const DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(
                  'assets/images/doctor.png',
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
