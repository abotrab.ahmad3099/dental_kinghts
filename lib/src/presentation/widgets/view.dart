import 'package:dental_kinghts/src/data/models/education.dart';
import 'package:dental_kinghts/src/data/models/language.dart';
import 'package:dental_kinghts/src/logic/more_experince_logic.dart';
import 'package:dental_kinghts/src/presentation/pages/home_page_doctor.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter_cache_manager/file.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../constants/server_addresses.dart';
import '../../constants/storage.dart';
import '../../data/models/personal_data.dart';
import '/src/constants/color_app.dart';
import '/src/constants/doctor_api.dart';
import '/src/constants/static_data.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:http/http.dart' as http;

import 'details_info_personality.dart';
import 'drawer_doctor.dart';

// // ignore: must_be_immutable
// class View extends StatefulWidget {
//   View({
//     Key? key,
//     required this.background,
//   }) : super(key: key);

//   Color background;

//   @override
//   State<View> createState() => _ViewState();
// }

// class _ViewState extends State<View> {
//   var heightBetweenCard = 2.h;
//   bool isLoading = false;
//   TextStyle styleSubTitle =
//       TextStyle(fontSize: 4.w, fontWeight: FontWeight.w600);

//   TextStyle styleTitle = TextStyle(fontSize: 5.w, fontWeight: FontWeight.w600);
//   var valueOfPadding = 3.w;

//   Row twiceData(firstTitle, secondTitle, firstData, secondData) {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.end,
//       children: [
//         Column(
//           crossAxisAlignment: CrossAxisAlignment.end,
//           children: [
//             Text(
//               firstTitle,
//               textAlign: TextAlign.end,
//               style: styleSubTitle,
//             ),
//             SizedBox(height: 1.h),
//             Text(
//               firstData,
//               textAlign: TextAlign.end,
//             ),
//           ],
//         ),
//         SizedBox(
//           width: 25.w,
//         ),
//         Column(
//           crossAxisAlignment: CrossAxisAlignment.end,
//           children: [
//             Text(
//               secondTitle,
//               style: styleSubTitle,
//               textAlign: TextAlign.end,
//             ),
//             SizedBox(height: 1.h),
//             Text(
//               secondData,
//               textAlign: TextAlign.end,
//             ),
//           ],
//         ),
//       ],
//     );
//   }

//   bool personalInformationIsFilled() {
//     return StaticData.personalData!.firstName != null &&
//         StaticData.personalData!.lastName != null &&
//         StaticData.personalData!.gender != null &&
//         StaticData.personalData!.nameOfEmployee != null &&
//         StaticData.personalData!.natinality != null &&
//         StaticData.personalData!.email != null &&
//         StaticData.personalData!.phone != null &&
//         StaticData.personalData!.placeBirth != null &&
//         StaticData.personalData!.address != null &&
//         StaticData.personalData!.birthDate != null &&
//         StaticData.personalData!.civilStatus != null &&
//         StaticData.personalData!.city != null &&
//         StaticData.personalData!.linkedInLink != null &&
//         StaticData.personalData!.country != null;
//   }

//   GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
//   @override
//   Widget build(BuildContext context) {
//     Get.lazyPut(() => MoreExperinceLogic());
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.end,
//       children: [
//         SizedBox(height: heightBetweenCard),
//         Padding(
//           padding: EdgeInsets.only(right: valueOfPadding),
//           child: CircleAvatar(
//             radius: 8.w,
//             backgroundColor: ColorApp.primaryColor,
//             child: Image.asset(
//               'assets/images/doctor.png',
//               fit: BoxFit.fill,
//             ),
//           ),
//         ),
//         SizedBox(height: heightBetweenCard),
//         Padding(
//           padding: EdgeInsets.only(right: valueOfPadding),
//           child: Text(
//             'المقدمة',
//             style: styleTitle,
//             textAlign: TextAlign.end,
//             textDirection: TextDirection.rtl,
//           ),
//         ),
//         SizedBox(height: heightBetweenCard),
//         Padding(
//           padding: EdgeInsets.symmetric(horizontal: 4.w),
//           child: Text(
//             Get.find<MoreExperinceLogic>().introduction.text,
//             maxLines: 4,
//             textAlign: TextAlign.start,
//             textDirection: TextDirection.rtl,
//             style: TextStyle(height: 0.5.w),
//           ),
//         ),
//         !personalInformationIsFilled()
//             ? const Center(
//                 child: Text('لا يوجد بيانات خاصة'),
//               )
//             : PersonalInformationData(),
//         SizedBox(height: heightBetweenCard),
//         !experinceInformationIsFilled()
//             ? const Center(
//                 child: Text('لا يوجد بيانات خاصة'),
//               )
//             : StaticData.experiences!.empolyementQualifications!.isNotEmpty
//                 ? Padding(
//                     padding: EdgeInsets.only(right: valueOfPadding),
//                     child: Text(
//                       'الخبرات الوظيفية',
//                       style: styleTitle,
//                     ),
//                   )
//                 : const SizedBox.shrink(),
//         StaticData.experiences!.empolyementQualifications!.isNotEmpty
//             ? SizedBox(height: heightBetweenCard)
//             : const SizedBox.shrink(),
//         StaticData.experiences!.empolyementQualifications!.isNotEmpty
//             ? equalificationEmployee()
//             : const SizedBox.shrink(),
//         StaticData.experiences!.empolyementQualifications!.isNotEmpty
//             ? SizedBox(height: heightBetweenCard)
//             : const SizedBox.shrink(),
//         StaticData.experiences!.educations!.isNotEmpty
//             ? Padding(
//                 padding: EdgeInsets.only(right: valueOfPadding),
//                 child: Text(
//                   'الشهادات العلمية',
//                   style: styleTitle,
//                 ),
//               )
//             : const SizedBox.shrink(),
//         StaticData.experiences!.educations!.isNotEmpty
//             ? SizedBox(height: heightBetweenCard)
//             : const SizedBox.shrink(),
//         StaticData.experiences!.educations!.isNotEmpty
//             ? educationView()
//             : const SizedBox.shrink(),
//         StaticData.experiences!.educations!.isNotEmpty
//             ? SizedBox(height: heightBetweenCard)
//             : const SizedBox.shrink(),
//         StaticData.experiences!.languages!.isNotEmpty
//             ? Padding(
//                 padding: EdgeInsets.only(right: valueOfPadding),
//                 child: Text(
//                   'اللغات',
//                   style: styleTitle,
//                 ),
//               )
//             : const SizedBox.shrink(),
//         StaticData.experiences!.languages!.isNotEmpty
//             ? SizedBox(height: heightBetweenCard)
//             : const SizedBox.shrink(),
//         StaticData.experiences!.languages!.isNotEmpty
//             ? languageView()
//             : const SizedBox.shrink(),
//         StaticData.experiences!.languages!.isNotEmpty
//             ? SizedBox(height: heightBetweenCard)
//             : const SizedBox.shrink(),
//         StaticData.experiences!.skills!.isNotEmpty
//             ? Padding(
//                 padding: EdgeInsets.only(right: valueOfPadding),
//                 child: Text(
//                   'المهارات',
//                   style: styleTitle,
//                 ),
//               )
//             : const SizedBox.shrink(),
//         StaticData.experiences!.skills!.isNotEmpty
//             ? SizedBox(height: heightBetweenCard)
//             : const SizedBox.shrink(),
//         StaticData.experiences!.skills!.isNotEmpty
//             ? Card(
//                 elevation: 7,
//                 shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(20)),
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.end,
//                   children: [
//                     SizedBox(
//                       height: 40.h,
//                       child: ListView.builder(
//                         itemCount: StaticData.experiences!.skills!.length,
//                         itemBuilder: (context, index) {
//                           return Padding(
//                             padding: EdgeInsets.only(right: 4.w),
//                             child: Column(
//                               crossAxisAlignment: CrossAxisAlignment.center,
//                               children: [
//                                 SizedBox(height: 1.h),
//                                 Column(
//                                   crossAxisAlignment: CrossAxisAlignment.center,
//                                   children: [
//                                     twiceData(
//                                       'المستوى',
//                                       'المهارة',
//                                       StaticData
//                                           .experiences!.skills![index].level,
//                                       StaticData.experiences!.skills![index]
//                                           .nameSkill,
//                                     ),
//                                     SizedBox(height: 1.h),
//                                   ],
//                                 ),
//                                 const Divider(),
//                               ],
//                             ),
//                           );
//                         },
//                       ),
//                     ),
//                   ],
//                 ),
//               )
//             : const SizedBox.shrink(),
//         StaticData.experiences!.skills!.isNotEmpty
//             ? SizedBox(height: heightBetweenCard)
//             : const SizedBox.shrink(),
//         StaticData.experiences!.hobbits!.isNotEmpty
//             ? Padding(
//                 padding: EdgeInsets.only(right: valueOfPadding),
//                 child: Text(
//                   'الهوايات',
//                   style: styleTitle,
//                 ),
//               )
//             : const SizedBox.shrink(),
//         StaticData.experiences!.hobbits!.isNotEmpty
//             ? SizedBox(height: heightBetweenCard)
//             : const SizedBox.shrink(),
//         StaticData.experiences!.skills!.isNotEmpty

//             : const SizedBox.shrink(),
//         StaticData.experiences!.courses!.isNotEmpty
//             ? Padding(
//                 padding: EdgeInsets.only(right: valueOfPadding),
//                 child: Text(
//                   'الدورات التدريبية',
//                   style: styleTitle,
//                 ),
//               )
//             : const SizedBox.shrink(),
//         StaticData.experiences!.courses!.isNotEmpty
//             ? SizedBox(height: heightBetweenCard)
//             : const SizedBox.shrink(),
//         StaticData.experiences!.courses!.isNotEmpty
//             ? Card(
//                 elevation: 7,
//                 shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(20)),
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.end,
//                   children: [
//                     SizedBox(
//                       height: 40.h,
//                       child: ListView.builder(
//                         itemCount: StaticData.experiences!.courses!.length,
//                         itemBuilder: (context, index) {
//                           return Padding(
//                             padding: EdgeInsets.only(right: 4.w),
//                             child: Column(
//                               crossAxisAlignment: CrossAxisAlignment.end,
//                               children: [
//                                 SizedBox(height: 1.h),
//                                 Column(
//                                   crossAxisAlignment: CrossAxisAlignment.end,
//                                   children: [
//                                     Row(
//                                       mainAxisAlignment: MainAxisAlignment.end,
//                                       children: [
//                                         Column(
//                                           children: [
//                                             Text(
//                                               'التاريخ',
//                                               style: styleSubTitle,
//                                             ),
//                                             SizedBox(
//                                               height: 1.h,
//                                             ),
//                                             Text(StaticData.experiences!
//                                                 .courses![index].date!),
//                                           ],
//                                         ),
//                                         SizedBox(
//                                           width: 25.w,
//                                         ),
//                                         Column(
//                                           children: [
//                                             Text(
//                                               'اسم الدورة',
//                                               style: styleSubTitle,
//                                             ),
//                                             SizedBox(
//                                               height: 1.h,
//                                             ),
//                                             Text(
//                                               StaticData.experiences!
//                                                   .courses![index].nameCourse!,
//                                               style: styleSubTitle,
//                                             ),
//                                           ],
//                                         ),
//                                       ],
//                                     ),
//                                     SizedBox(height: 1.h),
//                                   ],
//                                 ),
//                                 const Divider(),
//                               ],
//                             ),
//                           );
//                         },
//                       ),
//                     ),
//                   ],
//                 ),
//               )
//             : const SizedBox.shrink(),
//         SizedBox(
//           height: 3.h,
//         ),

//       ],
//     );
//   }
// }

// // ignore: must_be_immutable
class PersonalInformationData extends StatelessWidget {
  PersonalInformationData({
    Key? key,
  }) : super(key: key);

  var heightBetweenCard = 2.h;
  TextStyle styleSubTitle = TextStyle(
    fontSize: 4.w,
  );

  TextStyle styleTitle = TextStyle(fontSize: 5.w, fontWeight: FontWeight.w600);
  var valueOfPadding = 3.w;

  twiceData(firstTitle, secondTitle, firstData, secondData) {
    return SizedBox(
      width: 70.w,
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  firstTitle,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 4.w),
                  textAlign: TextAlign.end,
                ),
                SizedBox(height: 1.h),
                Text(
                  firstData,
                  textAlign: TextAlign.end,
                ),
              ],
            ),
          ),
          SizedBox(width: 10.w),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  secondTitle,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 4.w),
                  textAlign: TextAlign.end,
                ),
                SizedBox(height: 1.h),
                Text(
                  secondData,
                  textAlign: TextAlign.end,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        RichText(
          text: TextSpan(
            style: DefaultTextStyle.of(context).style,
            children: <TextSpan>[
              TextSpan(
                  text: StaticData.personalData!.firstName ?? "",
                  style: styleSubTitle),
              TextSpan(
                text: " : الاسم",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 4.w),
              ),
            ],
          ),
        ),
        SizedBox(height: 2.h),
        twiceData(
          'مكان الميلاد',
          'تاريخ الميلاد',
          StaticData.personalData!.city ?? "",
          StaticData.personalData!.birthDate ?? "",
        ),
        SizedBox(height: 2.h),
        twiceData(
          'الجنس',
          'الجنسية',
          StaticData.personalData!.gender ?? "",
          StaticData.personalData!.natinality ?? "",
        ),
        SizedBox(height: 1.h),
        twiceData(
          '',
          'الحالة المدنية',
          "",
          StaticData.personalData!.civilStatus ?? "",
        ),
        RichText(
          text: TextSpan(
            style: DefaultTextStyle.of(context).style,
            children: <TextSpan>[
              TextSpan(
                  text: StaticData.personalData!.city ?? "",
                  style: styleSubTitle),
              TextSpan(
                text: ' :  عنوان الإقامة',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 4.w),
              ),
            ],
          ),
        ),
        Padding(
          padding:
              EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.5),
          child: const Divider(),
        ),
        const SizedBox(
          height: 10,
        ),
        // Text('معلومات التواصل',
        //     style: TextStyle(
        //       fontSize: 14,
        //       fontWeight: FontWeight.w500,
        //       color: Colors.grey[600],
        //     )),
        // const SizedBox(
        //   height: 10,
        // ),F∏
        RichText(
          text: TextSpan(
            style: DefaultTextStyle.of(context).style,
            children: <TextSpan>[
              TextSpan(
                  text: StaticData.personalData!.email ?? "",
                  style: styleSubTitle),
              TextSpan(
                text: " : البريد الالكتروني",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 4.w),
              ),
            ],
          ),
        ),
        SizedBox(height: 2.h),
        RichText(
          text: TextSpan(
            style: DefaultTextStyle.of(context).style,
            children: <TextSpan>[
              TextSpan(
                  text: StaticData.personalData!.phone ?? "",
                  style: styleSubTitle),
              TextSpan(
                text: " : رقم الهاتف",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 4.w),
              ),
            ],
          ),
        ),
        SizedBox(height: 2.h),
      ],
    );
  }
}

class View extends StatefulWidget {
  const View({Key? key}) : super(key: key);

  @override
  State<View> createState() => _ViewState();
}

class _ViewState extends State<View> {
  bool isLoading = false;
  TextStyle styleSubTitle = TextStyle(
    fontSize: 4.w,
  );

  twiceData(firstTitle, secondTitle, firstData, secondData) {
    return SizedBox(
      width: 70.w,
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  firstTitle,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 4.w),
                  textAlign: TextAlign.end,
                ),
                SizedBox(height: 1.h),
                Text(
                  firstData,
                  textAlign: TextAlign.end,
                ),
              ],
            ),
          ),
          SizedBox(width: 10.w),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  secondTitle,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 4.w),
                  textAlign: TextAlign.end,
                ),
                SizedBox(height: 1.h),
                Text(
                  secondData,
                  textAlign: TextAlign.end,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  equalificationEmployee(equalificationEmployee) {
    return Padding(
      padding: EdgeInsets.only(right: 4.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          SizedBox(height: 1.h),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              twiceData(
                'المدينة',
                'اسم الوظيفة',
                equalificationEmployee.nameOfJobs,
                equalificationEmployee.countryCity,
              ),
              SizedBox(height: 1.5.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(equalificationEmployee.nameOfComapny!),
                  SizedBox(
                    width: 4.w,
                  ),
                  Text(
                    'اسم الشركة',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 4.w),
                  ),
                ],
              ),
              SizedBox(height: 1.5.h),
              twiceData(
                'إلى تاريخ',
                'من تاريخ',
                equalificationEmployee.endDate,
                equalificationEmployee.startDate,
              ),
              SizedBox(height: 1.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    equalificationEmployee.moreDetials!,
                  ),
                  SizedBox(
                    width: 4.w,
                  ),
                  Text(
                    'المزيد',
                    style: styleSubTitle,
                  ),
                ],
              ),
              SizedBox(height: 1.h),
            ],
          ),
          const Divider(),
        ],
      ),
    );
  }

  languageView(language) {
    return Padding(
      padding: EdgeInsets.only(right: 4.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          SizedBox(height: 1.h),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              twiceData(
                'المستوى',
                'اللغة',
                language.level,
                language.language,
              ),
              SizedBox(height: 1.h),
            ],
          ),
          const Divider(),
        ],
      ),
    );
  }

  skills() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        SizedBox(
          child: ListView.builder(
            itemCount: StaticData.experiences!.skills!.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: EdgeInsets.only(right: 4.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    SizedBox(height: 1.h),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        twiceData(
                          'المستوى',
                          'المهارة',
                          StaticData.experiences!.skills![index].level,
                          StaticData.experiences!.skills![index].nameSkill,
                        ),
                        SizedBox(height: 1.h),
                      ],
                    ),
                    const Divider(),
                  ],
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  educationView() {
    return Column(
      children: [
        SizedBox(
          height: 40.h,
          child: ListView.builder(
            itemCount: StaticData.experiences!.educations!.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: EdgeInsets.only(right: 4.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 1.h),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        twiceData(
                          'اسم التخصص',
                          'اسم الجامعة',
                          StaticData.experiences!.educations![index]
                              .nameOfSpecilization,
                          StaticData.experiences!.educations![index]
                              .nameOfUniversity!,
                        ),
                        SizedBox(height: 1.5.h),
                        twiceData(
                          'إلى تاريخ',
                          'من تاريخ',
                          StaticData.experiences!.educations![index].endDate!,
                          StaticData.experiences!.educations![index].startDate!,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              StaticData
                                  .experiences!.educations![index].moreDetials!,
                            ),
                            SizedBox(
                              width: 4.w,
                            ),
                            Text(
                              'المزيد',
                              style: styleSubTitle,
                            ),
                          ],
                        ),
                        SizedBox(height: 1.h),
                      ],
                    ),
                    const Divider(),
                  ],
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  hobbits(hobbit) {
    return Padding(
      padding: EdgeInsets.only(left: 50.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          SizedBox(height: 1.h),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    hobbit.nameOfHobbit!,
                    textAlign: TextAlign.end,
                  ),
                  Expanded(
                      child: SizedBox(
                    width: 50.w,
                  )),
                  Text(
                    'الهواية',
                    textAlign: TextAlign.end,
                    style: styleSubTitle,
                  ),
                  SizedBox(
                    width: 2.w,
                  )
                ],
              ),
              SizedBox(height: 1.h),
            ],
          ),
          const Divider(),
        ],
      ),
    );
  }

  bool personalInformationIsFilled() {
    bool c = StaticData.personalData!.firstName != null &&
        StaticData.personalData!.lastName != null &&
        StaticData.personalData!.gender != null &&
        StaticData.personalData!.nameOfEmployee != null &&
        StaticData.personalData!.natinality != null &&
        StaticData.personalData!.email != null &&
        StaticData.personalData!.phone != null &&
        StaticData.personalData!.placeBirth != null &&
        StaticData.personalData!.address != null &&
        StaticData.personalData!.birthDate != null &&
        StaticData.personalData!.civilStatus != null &&
        StaticData.personalData!.city != null &&
        StaticData.personalData!.linkedInLink != null;
    return c;
  }

  bool experinceInformationIsFilled() {
    return StaticData.experiences!.empolyementQualifications!.isNotEmpty &&
        StaticData.experiences!.educations!.isNotEmpty &&
        StaticData.experiences!.skills!.isNotEmpty &&
        StaticData.experiences!.hobbits!.isNotEmpty &&
        StaticData.experiences!.courses!.isNotEmpty &&
        StaticData.experiences!.languages!.isNotEmpty;
  }

  addCV() async {
    var url = '${ServerAddresses.serverAddress}cv';
    var request = http.MultipartRequest('POST', Uri.parse(url));
    List<http.MultipartFile> newList = <http.MultipartFile>[];
    Map<String, String> headers = {'token': Storage().token};
    request.fields['nameOfEmployee'] = StaticData.personalData!.nameOfEmployee!;
    StaticData.personalData;
    StaticData.experiences;
    // StaticData.introduction;
    request.fields['firstName'] = StaticData.personalData!.firstName!;
    request.fields['lastName'] = StaticData.personalData!.lastName!;
    request.fields['phone'] = StaticData.personalData!.phone!;
    request.fields['address'] = StaticData.personalData!.address!;
    request.fields['city'] = StaticData.personalData!.city!;
    request.fields['civilStatus'] = StaticData.personalData!.civilStatus!;
    // request.fields['country'] = StaticData.personalData!.country!;
    request.fields['gender'] = StaticData.personalData!.gender!;
    request.fields['linkedInLink'] = StaticData.personalData!.linkedInLink!;
    request.fields['natinality'] = StaticData.personalData!.natinality!;
    request.fields['phone'] = StaticData.personalData!.phone!;
    request.fields['placeBirth'] = StaticData.personalData!.placeBirth!;

    if (StaticData.experiences!.empolyementQualifications!.isNotEmpty) {
      for (var element in StaticData.experiences!.empolyementQualifications!) {
        request.fields['nameOfJobs[]'] = element.nameOfJobs!;
        request.fields['nameOfCompany[]'] = element.nameOfComapny!;
        request.fields['moreDetialsEQ[]'] = element.moreDetials!;
        request.fields['countryCity[]'] = element.countryCity!;
        request.fields['startDateEQ[]'] = element.startDate!;
        request.fields['endDateEQ[]'] = element.endDate!;
      }
    }

    if (StaticData.experiences!.educations!.isNotEmpty) {
      for (var element in StaticData.experiences!.educations!) {
        request.fields['nameOfSpecilization[]'] = element.nameOfSpecilization!;
        request.fields['nameOfUniversity[]'] = element.nameOfUniversity!;
        request.fields['moreDetialsED[]'] = element.moreDetials!;
        request.fields['startDateED[]'] = element.startDate!;
        request.fields['endDateED[]'] = element.endDate!;
      }
    }

    if (StaticData.experiences!.languages!.isNotEmpty) {
      for (var element in StaticData.experiences!.languages!) {
        request.fields['language[]'] = element.language!;
        request.fields['level[]'] = element.level!;
      }
    }

    if (StaticData.experiences!.hobbits!.isNotEmpty) {
      for (var element in StaticData.experiences!.hobbits!) {
        request.fields['nameOfHobbit[]'] = element.nameOfHobbit!;
      }
    }

    if (StaticData.experiences!.courses!.isNotEmpty) {
      for (var element in StaticData.experiences!.courses!) {
        request.fields['nameCourse[]'] = element.nameCourse!;
        request.fields['courseDate[]'] = element.date!;
      }
    }

    var res = await request.send();
    isLoading = true;
    setState(() {});
    var response = await http.Response.fromStream(res);
    isLoading = false;
    setState(() {});
    if (response.statusCode == 200) {
      Fluttertoast.showToast(
          msg: "تم إنشاء السيرة الذاتية بنجاح", toastLength: Toast.LENGTH_LONG);
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (_) => const HomePageDoctor()),
          (route) => false);

      StaticData.experiences!.courses!.clear();
      StaticData.experiences!.educations!.clear();
      StaticData.experiences!.hobbits!.clear();
      StaticData.experiences!.empolyementQualifications!.clear();
      StaticData.experiences!.skills!.clear();
      StaticData.experiences!.introduction = '';
      StaticData.personalData!.address = "";
      StaticData.personalData!.city = "";
      StaticData.personalData!.civilStatus = "";
      StaticData.personalData!.birthDate = "";
      StaticData.personalData!.country = "";
      StaticData.personalData!.email = "";
      StaticData.personalData!.firstName = "";
      StaticData.personalData!.lastName = "";
      StaticData.personalData!.gender = "";
      StaticData.personalData!.image = null;
      StaticData.personalData!.placeBirth = "";
      StaticData.personalData!.phone = "";
    }

    debugPrint(response.body);
  }

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => MoreExperinceLogic());
    return Scaffold(
      body: ListView(
        children: <Widget>[
          !personalInformationIsFilled() && !experinceInformationIsFilled()
              ? const Center(child: Text('لا يوجد بيانات'))
              : const SizedBox.shrink(),
          if (Get.find<MoreExperinceLogic>().introduction.text != "")
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                const SubAddress(
                  title: 'التوصيف',
                  pathIcon: 'assets/images/bio.png',
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 25.0),
                  child: Text(
                    Get.find<MoreExperinceLogic>().introduction.text,
                  ),
                ),
                SizedBox(
                  height: 1.h,
                ),
                // const DottedLine(
                //   dashColor: Colors.grey,
                // ),
              ],
            )
          else
            const SizedBox.shrink(),
          SizedBox(
            height: 1.h,
          ),
          personalInformationIsFilled()
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const SubAddress(
                      title: 'المعلومات الشخصية',
                      pathIcon: 'assets/images/doctor.png',
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 25.0),
                      child: PersonalInformationData(),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    // const DottedLine(
                    //   dashColor: Colors.grey,
                    // ),
                  ],
                )
              : const SizedBox.shrink(),
          SizedBox(
            height: 2.h,
          ),
          StaticData.experiences!.empolyementQualifications!.isNotEmpty
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    const SubAddress(
                      title: 'التجارب والخبرات',
                      pathIcon: 'assets/images/skills.png',
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: StaticData
                          .experiences!.empolyementQualifications!
                          .map<Widget>((equalification) =>
                              equalificationEmployee(equalification))
                          .toList(),
                    ),
                  ],
                )
              : const SizedBox.shrink(),
          StaticData.experiences!.educations!.isNotEmpty
              ? Column(
                  children: <Widget>[
                    const SubAddress(
                      title: 'التعليم',
                      pathIcon: 'assets/images/education.png',
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: StaticData.experiences!.educations!
                          .map<Widget>(
                              (education) => educationWidget(education))
                          .toList(),
                    ),
                    educationView(),
                  ],
                )
              : const SizedBox.shrink(),
          StaticData.experiences!.languages!.isNotEmpty
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    const SubAddress(
                      title: 'اللغات',
                      pathIcon: 'assets/images/lang.png',
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: StaticData.experiences!.languages!
                          .map<Widget>((language) => languageView(language))
                          .toList(),
                    ),
                  ],
                )
              : const SizedBox.shrink(),
          StaticData.experiences!.hobbits!.isNotEmpty
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    const SubAddress(
                      title: 'الهوايات',
                      pathIcon: 'assets/images/skills.png',
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: StaticData.experiences!.hobbits!
                          .map<Widget>((hobbit) => hobbits(hobbit))
                          .toList(),
                    ),
                  ],
                )
              : const SizedBox.shrink(),
          personalInformationIsFilled() || experinceInformationIsFilled()
              ? isLoading
                  ? const Center(
                      child: CircularProgressIndicator(),
                    )
                  : Align(
                      alignment: Alignment.center,
                      child: Container(
                        width: 50.w,
                        height: 8.h,
                        decoration: BoxDecoration(
                          color: const Color(0xFF8c7884),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: TextButton(
                          onPressed: addCV,
                          child: const Text(
                            'إضافة',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    )
              : SizedBox.shrink(),
        ],
      ),
    );
  }

  Padding educationWidget(Education education) {
    return Padding(
      padding: EdgeInsets.only(right: 4.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 1.h),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              twiceData(
                'اسم التخصص',
                'اسم الجامعة',
                education.nameOfSpecilization,
                education.nameOfUniversity!,
              ),
              SizedBox(height: 1.5.h),
              twiceData(
                'إلى تاريخ',
                'من تاريخ',
                education.endDate!,
                education.startDate!,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    education.moreDetials!,
                  ),
                  SizedBox(
                    width: 4.w,
                  ),
                  Text(
                    'المزيد',
                    style: styleSubTitle,
                  ),
                ],
              ),
              SizedBox(height: 1.h),
            ],
          ),
          const Divider(),
        ],
      ),
    );
  }
}

class SubAddress extends StatelessWidget {
  const SubAddress({
    Key? key,
    this.title,
    this.pathIcon,
  }) : super(key: key);
  final String? title;
  final String? pathIcon;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      title!,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey[600],
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                  Image.asset(
                    pathIcon!,
                    height: 25,
                    width: 25,
                  ),
                ],
              ),
            ),
            const Divider(),
          ],
        ),
      ),
    );
  }
}
