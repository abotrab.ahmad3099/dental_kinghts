import 'package:dental_kinghts/src/authentication/authentication_bloc.dart';
import 'package:dental_kinghts/src/constants/storage.dart';
import 'package:dental_kinghts/src/local_data.dart';
import 'package:dental_kinghts/src/presentation/pages/education.dart';
import 'package:dental_kinghts/src/presentation/pages/jobs.dart';
import 'package:dental_kinghts/src/presentation/pages/login_page.dart';

import '../pages/lab/dental_lab.dart';
import '/src/constants/color_app.dart';
import '/src/presentation/pages/albums.dart';
import '/src/presentation/pages/contact_us.dart';

import '/src/presentation/pages/home_page_doctor.dart';
import '/src/presentation/pages/marketing.dart';
import '/src/presentation/pages/profile.dart';
import '/src/presentation/pages/terms_conditions.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../data/models/doctor_model.dart';

// ignore: must_be_immutable
class DrawerDoctor extends StatelessWidget {
  DrawerDoctor({
    Key? key,
  }) : super(key: key);

  List<String> listTitleItem = [
    "ملفي الشخصي",
    "الصفحة الرئيسية",
    "ألبوماتي",
    "التسوق",
    "معامل الأسنان",
    "الوظائف",
    "التعليم",
    "الشروط والأحكام",
    "تواصل معنا",
    "تسجيل الخروج",
  ];
  List<Function> functionsForEachItems = [
    //go to profile
    (context, {Doctor? doctorA}) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) => Profile(
                    doctor: Storage().doctor!,
                    isUserDoctor: true,
                  )));
    },
    //go to home page doctor
    (context, Doctor doctorA) {
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => const HomePageDoctor()));
    },
    //go to album
    (context, Doctor doctorA) {
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => const Albums()));
    },
    //go to marketing section
    (context, Doctor doctorA) {
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => const Marketing()));
    },
    //go to dental lab
    (context, Doctor doctorA) {
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => const DentalLab()));
    },
    (context, Doctor doctorA) {
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => const Education()));
    },
    //go to terms and conditions
    (context, Doctor doctorA) {
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => const TermsConditions()));
    },
    //go to contact us
    (context, Doctor doctorA) {
      Navigator.push(context, MaterialPageRoute(builder: (_) => Contactus()));
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Drawer(
      //backgroundColor: Colors.white,
      child: ListView(
        children: [
          drawerHeader(),
          const Divider(
            thickness: 7,
          ),
          // for (var i = 0; i < listTitleItem.length; i++)
          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => Profile(
                                  doctor: Storage().doctor!,
                                  isUserDoctor: true,
                                )));
                  },
                  child: Text(
                    listTitleItem[0],
                    style: TextStyle(
                        fontSize: 5.w,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                const Divider(),
              ],
            ),
          ),

          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => const HomePageDoctor()));
                  },
                  child: Text(
                    listTitleItem[1],
                    style: TextStyle(
                        fontSize: 5.w,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                const Divider(),
              ],
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => const Albums()));
                  },
                  child: Text(
                    listTitleItem[2],
                    style: TextStyle(
                        fontSize: 5.w,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                const Divider(),
              ],
            ),
          ),

          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => const Marketing()));
                  },
                  child: Text(
                    listTitleItem[3],
                    style: TextStyle(
                        fontSize: 5.w,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                const Divider(),
              ],
            ),
          ),

          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => const DentalLab()));
                  },
                  child: Text(
                    listTitleItem[4],
                    style: TextStyle(
                        fontSize: 5.w,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                const Divider(),
              ],
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => const Jobs()));
                  },
                  child: Text(
                    listTitleItem[5],
                    style: TextStyle(
                        fontSize: 5.w,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                const Divider(),
              ],
            ),
          ),

          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => const Education()));
                  },
                  child: Text(
                    listTitleItem[6],
                    style: TextStyle(
                        fontSize: 5.w,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                const Divider(),
              ],
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => const TermsConditions()));
                  },
                  child: Text(
                    listTitleItem[7],
                    style: TextStyle(
                        fontSize: 5.w,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                const Divider(),
              ],
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => Contactus()));
                  },
                  child: Text(
                    listTitleItem[8],
                    style: TextStyle(
                        fontSize: 5.w,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                const Divider(),
              ],
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    AuthenticationBloc.logout();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => LoginPage()));
                  },
                  child: Text(
                    listTitleItem[9],
                    style: TextStyle(
                        fontSize: 5.w,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                const Divider(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Align listTitleInDrawer(BuildContext context, int index, {Doctor? doctor}) {
    return Align(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextButton(
            onPressed: functionsForEachItems[index](context, doctor),
            child: Text(
              listTitleItem[index],
              style: TextStyle(
                  fontSize: 5.w,
                  fontWeight: FontWeight.w500,
                  color: Colors.black),
            ),
          ),
          const Divider(),
        ],
      ),
    );
  }

  DrawerHeader drawerHeader() {
    return DrawerHeader(
      margin: EdgeInsets.only(top: 4.5.w),
      child: Center(
        child: Column(
          children: [
            SizedBox(
              height: 3.h,
            ),
            CircleAvatar(
              radius: 7.w,
              backgroundColor: ColorApp.primaryColor,
              child: Image.asset(
                'assets/images/doctor.png',
                fit: BoxFit.contain,
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Text(
              Storage().name,
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 6.w),
            ),
          ],
        ),
      ),
    );
  }
}
