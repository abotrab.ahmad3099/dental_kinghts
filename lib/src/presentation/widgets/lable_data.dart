import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../constants/color_app.dart';

class LableTextField extends StatelessWidget {
  const LableTextField({
    Key? key,
    required this.textLable,
  }) : super(key: key);

  final String textLable;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 6.w),
      child: Align(
        alignment: Alignment.centerRight,
        child: Text(
          textLable,
          style: TextStyle(
            fontSize: 5.w,
            color: ColorApp.titleColorInRegisterPage,
          ),
        ),
      ),
    );
  }
}
