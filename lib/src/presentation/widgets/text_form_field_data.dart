import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

// ignore: must_be_immutable
class TextFormFieldData extends StatelessWidget {
  TextFormFieldData({
    Key? key,
    required this.validationText,
    required this.icon,
    required this.hintText,
    required this.controller,
    this.showPassword = false,
    this.keyboardType,
  }) : super(key: key);

  TextEditingController controller;
  final String hintText;
  final Widget icon;

  // ignore: prefer_typing_uninitialized_variables
  var keyboardType;

  bool showPassword = false;
  final String validationText;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 3.w),
      child: TextFormField(
        keyboardType: keyboardType ?? TextInputType.text,
        controller: controller,
        textAlign: TextAlign.right,
        // autovalidateMode: AutovalidateMode.always,
        decoration: InputDecoration(
          hintText: hintText,
          suffixIcon: icon,
          focusColor: Colors.black,
        ),
        obscureText: hintText == 'email' ? false : showPassword,

        validator: (value) {
          if (value == null) {
            return validationText;
          }
          return null;
        },
        onSaved: (value) {},
      ),
    );
  }
}
