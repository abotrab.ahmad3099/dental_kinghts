
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class DropDownDental extends StatefulWidget {
  const DropDownDental({
    Key? key,
  }) : super(key: key);

  // ignore: prefer_typing_uninitialized_variables

  @override
  State<DropDownDental> createState() => _DropDownDentalState();
}

class _DropDownDentalState extends State<DropDownDental> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    // getAllCategory();
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButton(
      // value: selectValue,
      icon: const Icon(Icons.keyboard_arrow_down),
      items: [].map((item) {
        return DropdownMenuItem(
          value: item,
          child: Text(item),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          // selectValue = newValue;
        });
      },
    );
  }
}
