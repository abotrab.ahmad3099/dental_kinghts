// ignore: must_be_immutable
import 'package:favorite_button/favorite_button.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../constants/color_app.dart';
import '../../data/models/doctor_model.dart';

// ignore: must_be_immutable
class CardDoctorHomePage extends StatelessWidget {
  CardDoctorHomePage({
    Key? key,
    required this.doctor,
  }) : super(key: key);

  Doctor doctor;

  onPressedLikeButton() {}

  onPressedFavoriteButton() {}

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: 1.w,
        ),
        Container(
          width: 89.w,
          height: 32.h,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: const Color(0xFFe6e6e6),
          ),
          child: Stack(
            alignment: Alignment.topRight,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 1.h, right: 2.w),
                child: CircleAvatar(
                  backgroundColor: ColorApp.primaryColor,
                  radius: 17,
                  child: Image.asset(
                    'assets/images/doctor.png',
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 1.h, right: 15.w),
                child: Text(
                  doctor.name!,
                  style: const TextStyle(fontWeight: FontWeight.w600),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 4.h, right: 15.w),
                child: Text(
                  doctor.specialization!,
                  style: TextStyle(fontSize: 2.5.w),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 8.h, right: 3.w),
                child: Text(
                  'نبذة عن الطبيب',
                  style: TextStyle(fontSize: 3.w, fontWeight: FontWeight.w700),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 11.h, right: 3.5.w, left: 1.w),
                child: Text(
                  doctor.info!,
                  textAlign: TextAlign.start,
                  textDirection: TextDirection.rtl,
                  // maxLines: 3,
                  style: TextStyle(
                    fontSize: 3.w,
                  ),
                ),
              ),

              ///   here image about doctor if there is

              Padding(
                padding: EdgeInsets.only(
                  top: 16.h,
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(
                      width: 20.w,
                      height: 10.h,
                      child: Image.asset('assets/images/Unknown.png'),
                    ),
                    SizedBox(
                      width: 20.w,
                      height: 10.h,
                      child: Image.asset('assets/images/doctor.png'),
                    )
                  ],
                ),
              ),

              ///   like and comment on doctor
              Padding(
                padding: EdgeInsets.only(
                  top: 25.h,
                ),
                child: const Divider(),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 25.h,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 1.h),
                      child: IconButton(
                        iconSize: 8.w,
                        onPressed: onPressedLikeButton,
                        icon: const Icon(Icons.message_outlined),
                      ),
                    ),
                    SizedBox(width: 3.w),
                    Padding(
                      padding: EdgeInsets.only(right: 3.w, top: 1.h),
                      child: FavoriteButton(
                        iconSize: 12.w,
                        // isFavorite: true,
                        valueChanged: (isFavorite) {},
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          width: 1.w,
        )
      ],
    );
  }
}
