import 'package:bloc/bloc.dart';
import 'package:dental_kinghts/src/bloc/posts_event.dart';
import 'package:dental_kinghts/src/bloc/posts_state.dart';
import 'package:dental_kinghts/src/data/models/post.dart';
import 'package:dental_kinghts/src/data/repositories/repository.dart';
import 'package:equatable/equatable.dart';

class PostsBloc extends Bloc<PostsEvent, PostsState> {
  final Repository repository = Repository();
  int page = 0;
  bool isFetching = false;
  String SpecId;
  List<Post> posts = [];
  PostsBloc({required this.SpecId}) : super(PostsInitialState()) {
    on<PostsEvent>((event, emit) async {
      if (event is PostsChangeSpecEvent) {
        SpecId = event.specId;
        page = 0;
        emit(PostsInitialState());
      }
      if (state is PostsInitialState) {
        emit(PostsLoadingState(message: 'Loading Beers'));
        posts = await repository.fetchPosts(SpecId, page);
        page++;
        emit(PostsSuccessState(posts: posts));
      } else {
        emit(state);
      }
      // TODO: implement event handler
    });
  }
}
