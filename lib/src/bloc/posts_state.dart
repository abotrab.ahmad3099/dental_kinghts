import '../data/models/post.dart';

abstract class PostsState {
  const PostsState();
}

class PostsInitialState extends PostsState {
  const PostsInitialState();
}

class PostsLoadingState extends PostsState {
  final String message;

  const PostsLoadingState({
    required this.message,
  });
}

class PostsSuccessState extends PostsState {
  final List<Post> posts;

  const PostsSuccessState({
    required this.posts,
  });
}

class PostsErrorState extends PostsState {
  final String error;

  const PostsErrorState({
    required this.error,
  });
}
