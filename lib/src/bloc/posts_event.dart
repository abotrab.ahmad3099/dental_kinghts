import 'package:equatable/equatable.dart';

abstract class PostsEvent extends Equatable {
  const PostsEvent();

  @override
  List<Object> get props => [];
}

class PostsLoadEvent extends PostsEvent {
  @override
  String toString() => 'Offers is Loaded';
}

class PostsFetchEvent extends PostsEvent {
  const PostsFetchEvent();
}

class PostsChangeSpecEvent extends PostsEvent {
  final String specId;
  const PostsChangeSpecEvent(this.specId);
}
