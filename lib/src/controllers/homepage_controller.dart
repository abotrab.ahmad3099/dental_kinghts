import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:http/http.dart';

import '../data/models/post.dart';
import '../data/repositories/repository.dart';

class HomePageController extends GetxController
    with StateMixin<List<Post>>, ScrollMixin {
  String? specId = "1";
  final Repository repository = Repository();
  int page = 0;
  List<Post> posts = [];
  bool getFirstData = false;
  bool lastPage = false;
  @override
  void onInit() {
    page = 0;
    getPosts();
    getFirstData = true;
    super.onInit();
    change(posts, status: RxStatus.loading());
  }

  changeSpecId(String? specId2) {
    specId = specId2;
    page = 0;
    posts.clear();
    change(posts, status: RxStatus.loading());
    getPosts();
  }

  getPosts() async {
    if (page == 0) {
      posts = await repository.fetchPosts(specId!, page);
    } else {
      List<Post> temp_posts = await repository.fetchPosts(specId!, page);
      if (temp_posts.isEmpty) {
        lastPage = true;
      } else
        posts.addAll(temp_posts);
    }
    page++;
    change(posts, status: RxStatus.success());
  }

  @override
  Future<void> onEndScroll() async {
    change(posts, status: RxStatus.loadingMore());
    if (!lastPage) getPosts();
  }

  @override
  Future<void> onTopScroll() async {
    print('onTopScroll');
  }
}
