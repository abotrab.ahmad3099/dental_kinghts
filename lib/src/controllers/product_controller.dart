import 'package:dental_kinghts/src/data/models/factory.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class ProductController extends GetxController {
  List<Products> products = [];

  @override
  void onInit() {
    products = [];
    super.onInit();
  }

  addNewProduct(value) {
    products.add(value);
    update();
  }

  deleteProduct(index) {
    products.removeAt(index);
    update();
  }
}
